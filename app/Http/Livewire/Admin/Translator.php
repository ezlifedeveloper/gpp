<?php

namespace App\Http\Livewire\Admin;

use App\Models\Translator as TranslatorModel;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Translator extends Component
{
    use WithPagination, WithFileUploads;

    public $title = 'Translator';

    public $editData = 0;
    public $openForm = 0;

    public $dataId;
    public $inputName, $inputCode, $inputContact;

    public $searchTerm;

    public function render()
    {
        $searchData = $this->searchTerm;
        return view('livewire.admin.translator', [
            'data' => TranslatorModel::withTrashed()
                ->where('name', 'like', '%' . $searchData . '%')
                ->orWhere('code', 'like', '%' . $searchData . '%')
                ->orWhere('contact', 'like', '%' . $searchData . '%')
                ->paginate(15)
        ])->layout('layouts.admin', ['title' => $this->title]);
    }

    private function resetValue()
    {
        $this->reset(['inputName', 'inputCode', 'inputContact', 'dataId']);
        $this->reset(['editData']);
        $this->openForm = false;
    }

    public function storeData()
    {
        if ($this->dataId) {
            $data = TranslatorModel::where('id', $this->dataId)
                ->update([
                    'name' => $this->inputName,
                    'code' => $this->inputCode,
                    'contact' => $this->inputContact,
                    'flag' => env('APP_URL') . '/flag/' . $this->inputCode . '.png'
                ]);
        } else {
            $data = TranslatorModel::create([
                'name' => $this->inputName,
                'code' => $this->inputCode,
                'contact' => $this->inputContact,
                'flag' => env('APP_URL') . '/flag/' . $this->inputCode . '.png',
                'created_by' => Auth::id()
            ]);
        }

        $this->resetValue();
        $this->alert('success', 'Data berhasil diperbarui');
    }

    public function restoreData($id)
    {
        $file = TranslatorModel::where('id', $id)->restore();

        $this->resetValue();
        $this->alert('success', 'Data berhasil direstore');
    }

    public function deleteData($id)
    {
        $file = TranslatorModel::withTrashed()->where('id', $id)->first();
        if ($file->trashed()) {
            $file->forceDelete();
        } else {
            $file->delete();
        }

        $this->resetValue();
        $this->alert('success', 'Data berhasil dihapus');
    }

    public function toggleForm($state = false)
    {
        $this->openForm = $state;
    }

    public function toggleEditData($id = null, $state = false)
    {
        $this->editData = $state;
        $this->dataId = $id;

        if ($this->editData && $id != null) {
            $this->openForm = true;
            $data = TranslatorModel::where('id', $id)->first();
            $this->dataId = $data->id;
            $this->inputName = $data->name;
            $this->inputCode = $data->code;
            $this->inputContact = $data->contact;
        } else {
            $this->resetValue();
        }
    }
}
