<?php

namespace App\Http\Livewire\Admin;

use App\Models\FinanceAccount as ModelsFinanceAccount;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class FinanceAccount extends Component
{
    use WithPagination;
    public $title = 'Finance Account';

    public $editData = 0;
    public $openForm = 0;

    public $dataId;
    public $inputName;

    public $searchTerm;

    public function render()
    {
        $searchData = $this->searchTerm;
        return view('livewire.admin.finance-account', [
            'data' => ModelsFinanceAccount::where('name', 'like', '%' . $searchData . '%')
                ->paginate(15)
        ])->layout('layouts.admin', ['title' => $this->title]);
    }

    private function resetValue()
    {
        $this->reset(['inputName', 'dataId']);
        $this->reset(['editData']);
        $this->openForm = false;
    }

    public function storeData()
    {
        if ($this->dataId) {
            $data = ModelsFinanceAccount::where('id', $this->dataId)
                ->update(['name' => $this->inputName]);
        } else {
            $data = ModelsFinanceAccount::create([
                'name' => $this->inputName,
                'created_by' => Auth::id()
            ]);
        }

        $this->resetValue();
        $this->alert('success', 'Data berhasil diperbarui');
    }

    public function restoreData($id)
    {
        $file = ModelsFinanceAccount::where('id', $id)->restore();

        $this->resetValue();
        $this->alert('success', 'Data berhasil direstore');
    }

    public function deleteData($id)
    {
        $file = ModelsFinanceAccount::where('id', $id)->first();
        $file->delete();

        $this->resetValue();
        $this->alert('success', 'Data berhasil dihapus');
    }

    public function toggleForm($state = false)
    {
        $this->openForm = $state;
    }

    public function toggleEditData($id = null, $state = false)
    {
        $this->editData = $state;
        $this->dataId = $id;

        if ($this->editData && $id != null) {
            $this->openForm = true;
            $data = ModelsFinanceAccount::where('id', $id)->first();
            $this->dataId = $data->id;
            $this->inputName = $data->name;
        } else {
            $this->resetValue();
        }
    }
}
