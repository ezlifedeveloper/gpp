<?php

namespace App\Http\Livewire\Admin;

use App\Models\FinanceCategory as ModelsFinanceCategory;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class FinanceCategory extends Component
{
    use WithPagination;
    public $title = 'Finance Category';

    public $editData = 0;
    public $openForm = 0;

    public $dataId;
    public $inputName, $inputType;

    public $searchTerm;

    public function render()
    {
        $searchData = $this->searchTerm;
        return view('livewire.admin.finance-category', [
            'data' => ModelsFinanceCategory::where('name', 'like', '%' . $searchData . '%')
                ->orWhere('flow', 'like', '%' . $searchData . '%')
                ->orderBy('flow')
                ->orderBy('name')
                ->paginate(15)
        ])->layout('layouts.admin', ['title' => $this->title]);
    }

    private function resetValue()
    {
        $this->reset(['inputName', 'inputType', 'dataId']);
        $this->reset(['editData']);
        $this->openForm = false;
    }

    public function storeData()
    {
        if ($this->dataId) {
            $data = ModelsFinanceCategory::where('id', $this->dataId)
                ->update([
                    'name' => $this->inputName,
                    'flow' => $this->inputType,
                ]);
        } else {
            $data = ModelsFinanceCategory::create([
                'name' => $this->inputName,
                'flow' => $this->inputType,
                'created_by' => Auth::id()
            ]);
        }

        $this->resetValue();
        $this->alert('success', 'Data berhasil diperbarui');
    }

    public function restoreData($id)
    {
        $file = ModelsFinanceCategory::where('id', $id)->restore();

        $this->resetValue();
        $this->alert('success', 'Data berhasil direstore');
    }

    public function deleteData($id)
    {
        $file = ModelsFinanceCategory::where('id', $id)->first();
        $file->delete();

        $this->resetValue();
        $this->alert('success', 'Data berhasil dihapus');
    }

    public function toggleForm($state = false)
    {
        $this->openForm = $state;
    }

    public function toggleEditData($id = null, $state = false)
    {
        $this->editData = $state;
        $this->dataId = $id;

        if ($this->editData && $id != null) {
            $this->openForm = true;
            $data = ModelsFinanceCategory::where('id', $id)->first();
            $this->dataId = $data->id;
            $this->inputName = $data->name;
            $this->inputType = $data->flow;
        } else {
            $this->resetValue();
        }
    }
}
