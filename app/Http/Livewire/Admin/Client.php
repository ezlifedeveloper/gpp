<?php

namespace App\Http\Livewire\Admin;

use App\Helpers\Helper;
use App\Models\Client as ClientModel;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Client extends Component
{
    use WithPagination, WithFileUploads;

    public $title = 'Client';

    public $editData = 0;
    public $openForm = 0;

    public $dataId;
    public $inputName, $inputShort;
    public $photo;

    public $searchTerm;

    public function render()
    {
        $searchData = $this->searchTerm;
        return view('livewire.admin.client', [
            'data' => ClientModel::withTrashed()
                ->where('name', 'like', '%' . $searchData . '%')
                ->orWhere('short', 'like', '%' . $searchData . '%')
                ->paginate(15)
        ])->layout('layouts.admin', ['title' => $this->title]);
    }

    private function resetValue()
    {
        $this->reset(['inputName', 'inputShort', 'dataId']);
        $this->reset(['editData']);
        $this->openForm = false;
    }

    public function storeData()
    {
        if ($this->dataId) {
            $data = ClientModel::where('id', $this->dataId)
                ->update(['name' => $this->inputName, 'short' => $this->inputShort]);
        } else {
            $data = ClientModel::create([
                'name' => $this->inputName,
                'short' => $this->inputShort,
                'created_by' => Auth::id()
            ]);
        }

        $id = $this->dataId ?? $data->id;

        if ($this->photo && !is_string($this->photo)) {
            $ext = '.' . $this->photo->getClientOriginalExtension();
            $filename = Helper::cleanStr(bcrypt($this->inputName . '-photo')) . $ext;
            $this->photo->storeAs('client', $filename);
            ClientModel::where('id', $id)->update(['photo' => env('APP_URL') . '/file/client/' . $filename]);
        }

        $this->resetValue();
        $this->alert('success', 'Data berhasil diperbarui');
    }

    public function restoreData($id)
    {
        $file = ClientModel::where('id', $id)->restore();

        $this->resetValue();
        $this->alert('success', 'Data berhasil direstore');
    }

    public function deleteData($id)
    {
        $file = ClientModel::withTrashed()->where('id', $id)->first();
        if ($file->trashed()) {
            $file->forceDelete();
        } else {
            $file->delete();
        }

        $this->resetValue();
        $this->alert('success', 'Data berhasil dihapus');
    }

    public function toggleForm($state = false)
    {
        $this->openForm = $state;
    }

    public function toggleEditData($id = null, $state = false)
    {
        $this->editData = $state;
        $this->dataId = $id;

        if ($this->editData && $id != null) {
            $this->openForm = true;
            $data = ClientModel::where('id', $id)->first();
            $this->dataId = $data->id;
            $this->inputName = $data->name;
            $this->inputShort = $data->short;
        } else {
            $this->resetValue();
        }
    }
}
