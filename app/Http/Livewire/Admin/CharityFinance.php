<?php

namespace App\Http\Livewire\Admin;

use App\Helpers\Helper;
use App\Models\Charity;
use App\Models\CharityFinance as CharityFinanceModel;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class CharityFinance extends Component
{
    use WithPagination;

    public $title = 'Charity Finance';

    public $editData = 0;
    public $openForm = 0;

    public $dataId;
    public $inputCharity, $inputUser, $inputValue, $inputDate, $inputDetail;

    public $searchTerm;

    public function render()
    {
        $searchData = $this->searchTerm;
        $id = Auth::user()->id;
        return view('livewire.admin.charity-finance', [
            'data' => CharityFinanceModel::when(Helper::isAllowed('superuser'), function ($query) {
                return $query->withTrashed();
            })->when(Helper::isAllowed('charity partner'), function ($query) use ($id, $searchData) {
                return $query->where('user_id', $id)
                    ->where('detail', 'like', '%' . $searchData . '%');
            })->when(!Helper::isAllowed('charity partner'), function ($query) use ($searchData) {
                return $query->where('detail', 'like', '%' . $searchData . '%')
                    ->orWhereHas('charity', function ($searchById) use ($searchData) {
                        $searchById->where('name', 'like', '%' . $searchData . '%');
                    });
            })->orderBy('date', 'desc')->orderBy('created_at', 'desc')->paginate(10),
            'user' => User::orderBy('name', 'asc')->get(),
            'charity' => Charity::orderBy('date', 'desc')->get(),
            'balance' => CharityFinanceModel::sum('value')
        ])->layout('layouts.admin', ['title' => $this->title]);
    }

    private function resetValue()
    {
        $this->reset(['inputCharity', 'inputDetail', 'inputUser', 'inputDate', 'inputValue', 'dataId']);
        $this->reset(['editData']);
        $this->openForm = false;
    }

    public function storeData()
    {
        $last = $this->dataId
            ? CharityFinanceModel::where('id', '<>', $this->dataId)->latest()->first()
            : CharityFinanceModel::latest()->first();

        $data = CharityFinanceModel::updateOrCreate(
            ['id' => $this->dataId],
            [
                'charity_id' => $this->inputCharity,
                'user_id' => $this->inputUser,
                'value' => $this->inputValue,
                'date' => $this->inputDate,
                'detail' => $this->inputDetail,
                'balance' => ($last->balance ?? 0) + $this->inputValue,
                'created_by' => Auth::id()
            ]
        );
        $this->resetValue();

        $this->alert('success', 'Data berhasil diperbarui');
    }

    public function restoreData($id)
    {
        $file = CharityFinanceModel::where('id', $id)->restore();
        $this->resetValue();

        $this->alert('success', 'Data berhasil direstore');
    }

    public function deleteData($id)
    {
        $file = CharityFinanceModel::withTrashed()->where('id', $id)->first();
        if ($file->trashed()) {
            $file->forceDelete();
        } else {
            $file->delete();
        }
        $this->resetValue();

        $this->alert('success', 'Data berhasil dihapus');
    }

    public function toggleForm($state = false)
    {
        $this->openForm = $state;
    }

    public function toggleEditData($id = null, $state = false)
    {
        $this->editData = $state;
        $this->dataId = $id;

        if ($this->editData && $id != null) {
            $this->openForm = true;
            $data = CharityFinanceModel::where('id', $id)->first();
            $this->dataId = $data->id;
            $this->inputCharity = $data->charity_id;
            $this->inputUser = $data->user_id;
            $this->inputDetail = $data->detail;
            $this->inputValue = $data->value;
            $this->inputDate = date('Y-m-d', strtotime($data->date));
        } else {
            $this->resetValue();
        }
    }
}
