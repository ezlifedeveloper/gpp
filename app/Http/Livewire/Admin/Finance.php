<?php

namespace App\Http\Livewire\Admin;

use App\Helpers\Helper;
use App\Models\Charity;
use App\Models\Finance as ModelsFinance;
use App\Models\FinanceAccount;
use App\Models\FinanceCategory;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Finance extends Component
{
    use WithPagination;

    public $title = 'Finance';

    public $editData = 0;
    public $openForm = 0;

    public $dataId;
    public $inputDate, $inputNote, $inputAmount, $inputAccount, $inputCategory;

    public $searchTerm;

    public function render()
    {
        $recap['income'] = 0;
        $recap['expense'] = 0;
        $recap['debt'] = 0;
        $recap['receivable'] = 0;

        $data = ModelsFinance::all();
        foreach($data as $d) {
            if($d->flow == 'Income') $recap['income'] += $d->amount;
            if($d->flow == 'Expense') $recap['expense'] += $d->amount;
            if($d->flow == 'Borrow'){
                $recap['debt'] += $d->amount;
            }
            if($d->flow == 'Borrow Return'){
                $recap['debt'] -= $d->amount;
            }
            if($d->flow == 'Lend'){
                $recap['receivable'] += $d->amount;
            }
            if($d->flow == 'Lend Return'){
                $recap['receivable'] -= $d->amount;
            }
        }
        $recap['balance'] = $recap['income']-$recap['expense']+$recap['debt']-$recap['receivable'];
                
        $searchData = $this->searchTerm;
        return view('livewire.admin.finance', [
            'data' => ModelsFinance::where('note', 'like', '%' . $searchData . '%')
                ->orWhere('flow', 'like', '%' . $searchData . '%')
                ->orWhere('category', 'like', '%' . $searchData . '%')
                ->orWhere('account_name', 'like', '%' . $searchData . '%')
                ->orderBy('date', 'desc')
                ->orderBy('id', 'desc')
                ->paginate(15),
            'category' => FinanceCategory::orderBy('flow', 'asc')->orderBy('name', 'asc')->get(),
            'account' => FinanceAccount::orderBy('name')->get(),
            'recap' => $recap
        ])->layout('layouts.admin', ['title' => $this->title]);
    }

    private function resetValue()
    {
        $this->reset(['inputDate', 'inputCategory', 'inputNote', 'inputAccount', 'inputAmount', 'dataId']);
        $this->reset(['editData']);
        $this->openForm = false;
    }

    public function storeData()
    {
        $category = FinanceCategory::where('id', $this->inputCategory)->first();
        $account = FinanceAccount::where('id', $this->inputAccount)->first();

        $data = ModelsFinance::updateOrCreate(
            ['id' => $this->dataId],
            [
                'date' => $this->inputDate,
                'note' => $this->inputNote,
                'amount' => $this->inputAmount,
                'account_id' => $this->inputAccount,
                'account_name' => $account->name,
                'category_id' => $this->inputCategory,
                'flow' => $category->flow,
                'category' => $category->name,
                'created_by' => Auth::id()
            ]
        );
        $this->resetValue();

        $this->alert('success', 'Data berhasil diperbarui');
    }

    public function restoreData($id)
    {
        $file = ModelsFinance::where('id', $id)->restore();
        $this->resetValue();

        $this->alert('success', 'Data berhasil direstore');
    }

    public function deleteData($id)
    {
        $file = ModelsFinance::withTrashed()->where('id', $id)->first();
        if ($file->trashed()) {
            $file->forceDelete();
        } else {
            $file->delete();
        }
        $this->resetValue();

        $this->alert('success', 'Data berhasil dihapus');
    }

    public function toggleForm($state = false)
    {
        $this->openForm = $state;
    }

    public function toggleEditData($id = null, $state = false)
    {
        $this->editData = $state;
        $this->dataId = $id;

        if ($this->editData && $id != null) {
            $this->openForm = true;
            $data = ModelsFinance::where('id', $id)->first();
            $this->dataId = $data->id;
            $this->inputDate = date('Y-m-d', strtotime($data->date));
            $this->inputNote = $data->note;
            $this->inputCategory = $data->category_id;
            $this->inputAmount = $data->amount;
            $this->inputAccount = $data->account_id;
        } else {
            $this->resetValue();
        }
    }
}
