<?php

namespace App\Http\Livewire\Admin;

use App\Helpers\Helper;
use App\Models\Mail as MailModel;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Mail extends Component
{
    use WithPagination, WithFileUploads;

    public $title = 'Mail';

    public $editData = 0;
    public $openForm = 0;

    public $dataId;
    public $inputSubject, $inputNo, $inputRecipient, $inputSender, $inputType, $inputStatus, $inputDate, $inputValue;
    public $file;

    public $searchTerm;

    public function render()
    {
        $searchData = $this->searchTerm;
        $allMail = MailModel::all();
        return view('livewire.admin.mail', [
            'data' => MailModel::withTrashed()
                ->where('subject', 'like', '%' . $searchData . '%')
                ->orWhere('no', 'like', '%' . $searchData . '%')
                ->orWhere('recipient', 'like', '%' . $searchData . '%')
                ->orWhere('sender', 'like', '%' . $searchData . '%')
                ->orderBy('date', 'desc')
                ->orderBy('created_at', 'desc')
                ->paginate(15),
            'in' => $allMail->where('type', 'Letter')->where('status', 'In')->count(),
            'out' => $allMail->where('type', 'Letter')->where('status', 'Out')->count(),
            'invoice' => $allMail->where('type', 'Invoice')->count(),
            'receipt' => $allMail->where('type', 'Receipt')->count()
        ])->layout('layouts.admin', ['title' => $this->title]);
    }

    private function resetValue()
    {
        $this->reset(['inputSubject', 'inputNo', 'inputRecipient', 'inputSender', 'inputType', 'inputStatus', 'inputDate', 'inputValue', 'dataId']);
        $this->reset(['editData']);
        $this->openForm = false;
    }

    public function changeFormState()
    {
        if ($this->inputStatus == 'In') {
            $this->inputRecipient = 'CV. Grasia Prima Perfekta';
            $this->inputSender = null;
        } else {
            if (!$this->dataId && $this->inputDate) $this->inputNo = Helper::generateMailNumber($this->inputType, $this->inputDate);
            $this->inputRecipient = null;
            $this->inputSender = 'CV. Grasia Prima Perfekta';
        }
        if ($this->inputType == 'Invoice') {
        } else {
            $this->inputValue = 0;
        }
    }

    public function storeData()
    {
        if ($this->dataId) {
            $data = MailModel::where('id', $this->dataId)
                ->update(['subject' => $this->inputSubject, 'no' => $this->inputNo]);
        } else {
            $data = MailModel::create([
                'subject' => $this->inputSubject,
                'recipient' => $this->inputRecipient,
                'sender' => $this->inputSender,
                'type' => $this->inputType,
                'status' => $this->inputStatus,
                'date' => $this->inputDate,
                'value' => $this->inputValue,
                'no' => $this->inputNo,
                'created_by' => Auth::id()
            ]);
        }

        $id = $this->dataId ?? $data->id;

        if ($this->file && !is_string($this->file)) {
            $ext = '.' . $this->file->getClientOriginalExtension();
            $filename = Helper::cleanStr(bcrypt($this->inputNo . '-file')) . $ext;
            $this->file->storeAs('mailing', $filename);
            MailModel::where('id', $id)->update(['file' => env('APP_URL') . '/file/mailing/' . $filename]);
        }

        $this->resetValue();
        $this->alert('success', 'Data berhasil diperbarui');
    }

    public function restoreData($id)
    {
        $file = MailModel::where('id', $id)->restore();

        $this->resetValue();
        $this->alert('success', 'Data berhasil direstore');
    }

    public function deleteData($id)
    {
        $file = MailModel::withTrashed()->where('id', $id)->first();
        if ($file->trashed()) {
            $file->forceDelete();
        } else {
            $file->delete();
        }

        $this->resetValue();
        $this->alert('success', 'Data berhasil dihapus');
    }

    public function toggleForm($state = false)
    {
        $this->openForm = $state;
    }

    public function toggleEditData($id = null, $state = false)
    {
        $this->editData = $state;
        $this->dataId = $id;

        if ($this->editData && $id != null) {
            $this->openForm = true;
            $data = MailModel::where('id', $id)->first();
            $this->dataId = $data->id;
            $this->inputSubject = $data->subject;
            $this->inputRecipient = $data->recipient;
            $this->inputSender = $data->sender;
            $this->inputType = $data->type;
            $this->inputStatus = $data->status;
            $this->inputDate = date('Y-m-d', strtotime($data->date));
            $this->inputValue = $data->value;
            $this->inputNo = $data->no;
        } else {
            $this->resetValue();
        }
    }
}
