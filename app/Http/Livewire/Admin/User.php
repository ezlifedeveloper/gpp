<?php

namespace App\Http\Livewire\Admin;

use App\Models\User as UserModel;
use Livewire\Component;
use Livewire\WithPagination;

class User extends Component
{
    use WithPagination;
    public $title = 'User';

    public $editData = 0;
    public $openForm = 0;

    public $dataId;
    public $inputName, $inputUsername, $inputRole, $inputPassword;

    public $searchTerm;

    public function render()
    {
        $searchData = $this->searchTerm;
        return view('livewire.admin.user', [
            'data' => UserModel::where('name', 'like', '%' . $searchData . '%')
                ->orWhere('username', 'like', '%' . $searchData . '%')
                ->paginate(15)
        ])->layout('layouts.admin', ['title' => $this->title]);
    }

    private function resetValue()
    {
        $this->reset(['inputUsername', 'inputName', 'inputRole', 'inputPassword', 'dataId']);
        $this->reset(['editData']);
        $this->openForm = false;
    }

    public function storeData()
    {
        if ($this->dataId) {
            if ($this->inputPassword) {
                $data = UserModel::where('id', $this->dataId)
                    ->update(
                        [
                            'username' => $this->inputUsername,
                            'name' => $this->inputName,
                            'password' => bcrypt($this->inputPassword),
                            'role' => $this->inputRole,
                        ]
                    );
            } else {
                $data = UserModel::where('id', $this->dataId)
                    ->update(
                        [
                            'username' => $this->inputUsername,
                            'name' => $this->inputName,
                            'role' => $this->inputRole,
                        ]
                    );
            }
        } else {
            $data = UserModel::create(
                [
                    'username' => $this->inputUsername,
                    'name' => $this->inputName,
                    'password' => bcrypt($this->inputPassword),
                    'role' => $this->inputRole,
                ]
            );
        }

        $this->resetValue();
        $this->alert('success', 'Data berhasil diperbarui');
    }

    public function restoreData($id)
    {
        $file = UserModel::where('id', $id)->restore();

        $this->resetValue();
        $this->alert('success', 'Data berhasil direstore');
    }

    public function deleteData($id)
    {
        $file = UserModel::withTrashed()->where('id', $id)->first();
        if ($file->trashed()) {
            $file->forceDelete();
        } else {
            $file->delete();
        }

        $this->resetValue();
        $this->alert('success', 'Data berhasil dihapus');
    }

    public function toggleForm($state = false)
    {
        $this->openForm = $state;
    }

    public function toggleEditData($id = null, $state = false)
    {
        $this->editData = $state;
        $this->dataId = $id;

        if ($this->editData && $id != null) {
            $this->openForm = true;
            $data = UserModel::where('id', $id)->first();
            $this->dataId = $data->id;
            $this->inputUsername = $data->username;
            $this->inputName = $data->name;
            $this->inputRole = $data->role;
        } else {
            $this->resetValue();
        }
    }
}
