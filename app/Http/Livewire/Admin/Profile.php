<?php

namespace App\Http\Livewire\Admin;

use App\Helpers\Helper;
use App\Models\CharityFinance;
use App\Models\CharityParticipant;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class Profile extends Component
{
    use WithFileUploads;

    public $title = 'Profile';

    public $editProfile = 0;

    public $inputName, $inputUsername, $inputDesc;
    public $photo, $cover;

    public function mount()
    {
        $user = Auth::user();
        $this->inputName = $user->name;
        $this->inputUsername = $user->username;
        $this->inputDesc = $user->desc;
        $this->photo = $user->photo;
        $this->cover = $user->cover;
    }

    public function render()
    {
        $user = Auth::user();

        if ($user->role != 'charity partner') {
            $donated = CharityFinance::where('user_id', Auth::id())->where('value', '>', 0)->sum('value');
        } else {
            $donated = -1 * CharityFinance::where('user_id', Auth::id())->where('value', '<', 0)->sum('value');
        }
        // dd($user->created_at->lessThan(Carbon::create(2022, 1, 1, 0, 0, 0)) ? '1 Jan 2019' : $user->created_at->format('d M Y'));
        return view('livewire.admin.profile', [
            'joined' => $user->created_at->lessThan(Carbon::create(2022, 1, 1, 0, 0, 0)) ? '1 Jan 2019' : $user->created_at->format('d M Y'),
            'donated' => $donated,
            'participated' => CharityParticipant::where('user_id', Auth::id())->count()
        ])->layout('layouts.admin', ['title' => $this->title]);
    }

    public function updateProfile()
    {
        $this->validate([
            'inputName' => 'required|string',
            'inputUsername' => 'required|string',
        ]);

        User::where('id', Auth::id())->update([
            'name' => $this->inputName,
            'username' => $this->inputUsername,
            'desc' => $this->inputDesc,
        ]);

        if ($this->cover && !is_string($this->cover)) {
            $ext = '.' . $this->cover->getClientOriginalExtension();
            $filename = Helper::cleanStr(bcrypt($this->inputUsername . '-cover')) . $ext;
            $this->cover->storeAs('user', $filename);
            User::where('id', Auth::id())->update(['cover' => env('APP_URL') . '/file/user/' . $filename]);
        }

        if ($this->photo && !is_string($this->photo)) {
            $ext = '.' . $this->photo->getClientOriginalExtension();
            $filename = Helper::cleanStr(bcrypt($this->inputUsername . '-photo')) . $ext;
            $this->photo->storeAs('user', $filename);
            User::where('id', Auth::id())->update(['photo' => env('APP_URL') . '/file/user/' . $filename]);
        }

        $user = User::where('id', Auth::id())->first();
        Auth::setUser($user);
        $this->alert('success', 'Data berhasil diperbarui');
        $this->toggleEditProfile();
    }

    public function toggleEditProfile()
    {
        $this->editProfile = !$this->editProfile;
    }
}
