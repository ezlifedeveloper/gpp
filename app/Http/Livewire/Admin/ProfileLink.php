<?php

namespace App\Http\Livewire\Admin;

use App\Models\ProfileLink as ModelsProfileLink;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ProfileLink extends Component
{
    public $title = 'MyLink';

    public $openModal = false;
    public $selectedData;
    public $dataId;

    public $inputUrl, $inputTitle, $inputTextColor, $inputBackgroundColor;

    public function mount()
    {
        $this->resetValue();
    }

    public function render()
    {
        return view('livewire.admin.profile-link')
            ->layout('layouts.admin', ['title' => $this->title]);
    }

    public function save()
    {
        if($this->dataId) {
            $data = ModelsProfileLink::find($this->dataId);
        } else {
            $data = new ModelsProfileLink();
        }

        $data->url = $this->inputUrl;
        $data->title = $this->inputTitle;
        $data->textcolor = $this->inputTextColor;
        $data->bgcolor = $this->inputBackgroundColor;
        $data->created_by = Auth::id();

        $data->save();

        $this->resetValue();
        
        $this->alert('success', 'Data berhasil diperbarui');
    }

    public function editLink($id)
    {
        $this->dataId = $id;
        $this->selectedData = ModelsProfileLink::find($id);
        $this->inputUrl = $this->selectedData->url;
        $this->inputTitle = $this->selectedData->title;
        $this->inputTextColor = $this->selectedData->textcolor;
        $this->inputBackgroundColor = $this->selectedData->bgcolor;
        $this->openModal = true;
    }

    public function deleteLink($id)
    {
        $data = ModelsProfileLink::find($id);
        $data->delete();
        $this->alert('success', 'Data berhasil dihapus');
    }

    public function toggleModal($state, $id = null)
    {
        $this->openModal = $state;
        
        if($id){
            $this->dataId = $id;
            $this->selectedData = ModelsProfileLink::find($id);
            $this->inputUrl = $this->selectedData->url;
            $this->inputTitle = $this->selectedData->title;
            $this->inputTextColor = $this->selectedData->textcolor;
            $this->inputBackgroundColor = $this->selectedData->bgcolor;
        } else {
            $this->dataId = null;
            $this->selectedData = null;
        }
    }

    public function resetValue()
    {
        $this->inputUrl = null;
        $this->inputTitle = null;
        $this->inputTextColor = null;
        $this->inputBackgroundColor = null;
        $this->openModal = false;
        $this->dataId = null;
        $this->selectedData = null;
    }
}
