<?php

namespace App\Http\Livewire\Admin;

use App\Helpers\Helper;
use App\Models\Charity as CharityModel;
use App\Models\CharityParticipant;
use App\Models\CharityPhoto;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class Charity extends Component
{
    use WithFileUploads;

    public $title = 'Charity';
    public $editCharity = 0;
    public $addPhoto = 0;
    public $addParticipant = 0;
    public $previewImage = 0;

    public $inputName, $inputDetail, $inputTarget, $inputDate, $inputReport;
    public $inputUser;
    public $photos = [];
    public $dataId = null;
    public $openedImage;

    public $limitPerPage = 3;

    protected $listeners = [
        'load-more' => 'loadMore'
    ];

    public function loadMore()
    {
        $this->limitPerPage = $this->limitPerPage + 2;
    }

    public function render()
    {
        $data = Helper::isAllowed('superuser')
            ? CharityModel::with(['participants', 'photos'])->withTrashed()->orderBy('date', 'desc')->paginate($this->limitPerPage)
            : CharityModel::with(['participants', 'photos'])->orderBy('date', 'desc')->paginate($this->limitPerPage);
        $this->emit('charityStore');

        return view('livewire.admin.charity', [
            'data' => $data,
            'countData' => CharityModel::count(),
            'user' => User::orderBy('name', 'asc')->get(),
        ])
            ->layout('layouts.admin', ['title' => $this->title]);
    }

    private function resetValue()
    {
        $this->reset(['inputName', 'inputDetail', 'inputTarget', 'inputDate', 'inputReport', 'dataId', 'inputUser', 'openedImage']);
        $this->reset(['editCharity', 'addPhoto', 'addParticipant', 'previewImage', 'photos']);
    }

    public function storeCharity()
    {
        $data = CharityModel::updateOrCreate(
            ['id' => $this->dataId],
            [
                'name' => $this->inputName,
                'detail' => $this->inputDetail,
                'slug' => strtolower(Helper::cleanstr(preg_replace('/\s+/', '-', $this->inputName))),
                'target' => $this->inputTarget,
                'date' => $this->inputDate,
                'report' => $this->inputReport,
                'created_by' => Auth::id()
            ]
        );

        $this->resetValue();
        $this->alert('success', 'Data berhasil diperbarui');
    }

    public function uploadPhoto()
    {
        if ($this->dataId) {
            $i = 0;
            foreach ($this->photos as $photo) {
                $ext = '.' . $photo->getClientOriginalExtension();
                $fileName = Helper::cleanstr(bcrypt(time() . $i++ . $this->dataId)) . $ext;
                $photo->storeAs('charity', $fileName);

                CharityPhoto::create([
                    'charity_id' => $this->dataId,
                    'name' => $fileName,
                    'created_by' => Auth::id(),
                    'photo' => env('APP_URL') . '/file/charity/' . $fileName,
                ]);
            }
            $this->resetValue();
            $this->alert('success', 'Foto berhasil diupload');
        } else $this->alert('warning', 'Foto gagal diupload');
    }

    public function addParticipant()
    {
        $data = CharityParticipant::create([
            'charity_id' => $this->dataId,
            'user_id' => $this->inputUser,
            'created_by' => Auth::id()
        ]);

        $this->resetValue();
        $this->alert('success', 'Partisipan berhasil ditambah');
    }

    public function restoreCharity($id)
    {
        $file = CharityModel::where('id', $id)->restore();

        $this->resetValue();
        $this->alert('success', 'Event berhasil direstore');
    }

    public function deleteCharity($id)
    {
        $file = CharityModel::withTrashed()->where('id', $id)->first();
        if ($file->trashed()) {
            $file->forceDelete();
        } else {
            $file->delete();
        }

        $this->resetValue();
        $this->alert('success', 'Event berhasil dihapus');
    }

    public function deleteImage($id)
    {
        $file = CharityPhoto::where('id', $id)->first();
        if ($file->trashed()) {
            $file->forceDelete();
        } else {
            $file->delete();
        }

        $this->resetValue();
        $this->alert('success', 'Gambar berhasil dihapus');
    }

    public function previewPhoto($image)
    {
        $this->previewImage = true;
        $this->openedImage = $image;
    }

    public function closePreviewPhoto()
    {
        $this->previewImage = false;
        $this->openedImage = null;
        $this->resetValue();
    }

    public function toggleEditCharity($id = null)
    {
        $this->editCharity = !$this->editCharity;
        if ($this->editCharity && $id != null) {
            $data = CharityModel::where('id', $id)->first();
            $this->dataId = $data->id;
            $this->inputName = $data->name;
            $this->inputDetail = $data->detail;
            $this->inputTarget = $data->target;
            $this->inputDate = date('Y-m-d', strtotime($data->date));
            $this->inputReport = $data->report;
        } else {
            $this->resetValue();
        }
    }

    public function toggleAddPhoto($id = null)
    {
        $this->addPhoto = !$this->addPhoto;
        $this->dataId = $id;

        if (!$this->dataId) $this->resetValue();
    }

    public function toggleAddParticipant($id = null)
    {
        $this->addParticipant = !$this->addParticipant;
        $this->dataId = $id;

        if (!$this->dataId) $this->resetValue();
    }
}
