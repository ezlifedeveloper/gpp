<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class Project extends Component
{
    public $title = 'Project';

    public function render()
    {
        return view('livewire.admin.project')
            ->layout('layouts.admin', ['title' => $this->title]);
    }
}
