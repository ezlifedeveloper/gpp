<?php

namespace App\Http\Livewire\Frontend;

use App\Models\Charity;
use App\Models\Client;
use App\Models\Portfolio;
use App\Models\Translator;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Home extends Component
{
    public function render()
    {
        $description = __('dictionary.home_seo_description');
        $title = 'Home';

        return view('livewire.frontend.home', [
            'client' => Client::all(),
            'portfolio' => Portfolio::whereHas('type', function ($q) {
                $q->whereIn('type', [1, 2, 3]);
            })->get(),
            'translator' => Translator::all(),
            'charity' => Charity::with(['cover'])->whereHas('cover')->get()
        ])->layout('layouts.frontend', [
            'description' => $description,
            'title' => $title,
            'type' => 'Portfolio'
        ]);
    }
}
