<?php

namespace App\Http\Livewire\Frontend;

use App\Models\Charity;
use App\Models\CharityFinance;
use Livewire\Component;

class CharityDetail extends Component
{
    public $slug;
    public $selectedColor;

    public $openedImage;
    public $previewImage = 0;

    public function mount($slug)
    {
        $this->slug = $slug;
    }

    public function render()
    {
        $color = ['F16450', 'FBB500', '51A6CA', '6AC789'];
        $data = Charity::where('slug', $this->slug)->first();
        $finance = CharityFinance::where('charity_id', $data->id)->where('value', '<', 0)->get();

        $this->selectedColor = $color[array_rand($color)];

        return view('livewire.frontend.charity-detail', [
            'data' => $data,
            'finance' => $finance,
        ])->layout('layouts.frontend', [
            'description' => $data->detail,
            'title' => $data->name,
            'type' => 'Charity',
            'imgasset' => $data->cover->photo ?? null
        ]);
    }

    public function previewPhoto($image)
    {
        $this->previewImage = true;
        $this->openedImage = $image;
    }

    public function closePreviewPhoto()
    {
        $this->previewImage = false;
        $this->openedImage = null;
    }
}
