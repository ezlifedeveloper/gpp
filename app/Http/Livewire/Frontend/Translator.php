<?php

namespace App\Http\Livewire\Frontend;

use App\Helpers\Helper;
use App\Models\LogCalculatorTranslator;
use App\Models\Translator as TranslatorModel;
use Livewire\Component;

class Translator extends Component
{
    public $qty = '-';
    public $price = 15000;
    public $days = 3;
    public $messageUrl = '-';

    public $inputQty, $inputLanguageFrom, $inputLanguageTo, $inputOrderPackage, $inputOrderType, $inputLanguage;

    public function mount()
    {
        $this->qty = __('dictionary.translation_pricing_proof_read_unit');
        $this->inputQty = 1;
        $this->inputLanguageFrom = 'id';
        $this->inputLanguageTo = 'en';
        $this->inputLanguage = 'en';
        $this->inputOrderPackage = __('dictionary.regular');
        $this->inputOrderType = __('dictionary.translation_pricing_document_normal_title');

        $this->price = __('dictionary.currency') . (bcadd(preg_replace("/[^0-9.]/", "", __('dictionary.translation_pricing_document_normal_price')), '0', 3) * 1.1);
        $this->days = trans_choice('dictionary.day_count', 1, ['value' => 1]);
        // $this->calculatePrice();
    }

    public function render()
    {
        $description = __('dictionary.translation_seo_description');
        $title = 'Translator';

        return view('livewire.frontend.translator', [
            'translator' => TranslatorModel::orderBy('name')->get(),
            'title' => $title
        ])->layout('layouts.frontend', [
            'description' => $description,
            'title' => $title,
            'type' => 'Translator',
            'imgasset' => asset('Translator.png')
        ]);
    }

    public function setValue($type, $package)
    {
        $this->inputOrderPackage = $package;
        $this->inputOrderType = $type;
        $this->calculatePrice();
        // dd($this);
    }

    public function calculatePrice()
    {
        $totalPrice = 1;

        if ($this->inputOrderType == __('dictionary.translation_pricing_application_title')) {
            $this->qty = __('dictionary.translation_pricing_application_unit');
            $totalPrice = bcadd(preg_replace("/[^0-9.]/", "", __('dictionary.translation_pricing_application_price')), '0', 3);
            $totalDay = ceil($this->inputQty / 300);
        } else if ($this->inputOrderType == __('dictionary.translation_pricing_proof_read_title')) {
            $this->qty = __('dictionary.translation_pricing_proof_read_unit');
            $totalPrice = bcadd(preg_replace("/[^0-9.]/", "", __('dictionary.translation_pricing_proof_read_price')), '0', 3);
            $totalDay = ceil($this->inputQty / 3);
        } else {
            $this->qty = __('dictionary.translation_pricing_document_normal_unit');
            $totalPrice = bcadd(preg_replace("/[^0-9.]/", "", __('dictionary.translation_pricing_document_normal_price')), '0', 3);
            $totalDay = ceil($this->inputQty / 3);
        }

        if (!$this->inputOrderType == __('dictionary.translation_pricing_proof_read_title')) {
            if ($this->inputLanguageTo == $this->inputLanguageFrom) {
                $this->price = __('dictionary.currency') . 0;
                $this->days = 0;
                return;
            } else if (($this->inputLanguageTo == 'en' && $this->inputLanguageFrom == 'id') || ($this->inputLanguageFrom == 'en' && $this->inputLanguageTo == 'id')) {
                $totalPrice *= 1;
            } else if (($this->inputLanguageTo == 'ko' && $this->inputLanguageFrom == 'id') || ($this->inputLanguageFrom == 'ko' && $this->inputLanguageTo == 'id')) {
                $totalPrice *= 2;
            } else if (($this->inputLanguageTo == 'en' && $this->inputLanguageFrom != 'id') || ($this->inputLanguageFrom == 'en' && $this->inputLanguageTo != 'id')) {
                $totalPrice *= 2;
            } else if ($this->inputLanguageTo == 'id' || $this->inputLanguageFrom == 'id') {
                $totalPrice *= 3;
                $totalDay *= 3;
            } else {
                $totalPrice *= 4;
                $totalDay *= 3;
            }
        }

        if ($this->inputOrderPackage == __('dictionary.express')) {
            $totalPrice *= 2;
            $totalDay = ceil($totalDay / 2);
        }
        $totalPrice *= $this->inputQty;

        $this->price = __('dictionary.currency') . ($totalPrice * 1.1);
        $this->days = trans_choice('dictionary.day_count', $totalDay, ['value' => $totalDay]);

        LogCalculatorTranslator::create([
            'type' => $this->inputOrderType,
            'package' => $this->inputOrderPackage,
            'language' => $this->inputOrderType == __('dictionary.translation_pricing_proof_read_title') ? $this->inputLanguage : $this->inputLanguageFrom . ' to ' . $this->inputLanguageTo,
            'qty' => $this->inputQty,
        ]);

        $this->sendWhatsappMessage();
    }

    public function sendWhatsappMessage()
    {
        if ($this->inputOrderType == __('dictionary.translation_pricing_proof_read_title')) {
            $message = __('dictionary.greeting') . ', ' . __('dictionary.interested_translating') . ' ' . $this->inputQty . ' ' .
                $this->qty . ' ' . __('dictionary.of') . ' ' . $this->inputOrderType . ' ' . __('dictionary.from') . ' ' . $this->inputLanguage .
                '. ' . __('dictionary.looking_for') . ' ' . __('dictionary.package', ['value' => $this->inputOrderPackage]) . ' ' .
                __('dictionary.and') . ' ' . __('dictionary.finish_within') . ' ' . $this->price . ' ' . $this->days;
        } else {
            $message = __('dictionary.greeting') . ', ' . __('dictionary.interested_translating') . ' ' . $this->inputQty . ' ' .
                $this->qty . ' ' . __('dictionary.of') . ' ' . $this->inputOrderType . ' ' . __('dictionary.from') . ' ' . $this->inputLanguageFrom .
                ' ' . __('dictionary.to') . ' '  . $this->inputLanguageTo . '. ' . __('dictionary.looking_for') . ' ' .
                __('dictionary.package', ['value' => $this->inputOrderPackage]) . ' ' . __('dictionary.and') .
                ' ' . __('dictionary.finish_within') . ' ' . $this->price . ' ' . $this->days;
        }

        $this->messageUrl = 'https://api.whatsapp.com/send?phone=6281221120660&text=' . urlencode($message);
    }
}
