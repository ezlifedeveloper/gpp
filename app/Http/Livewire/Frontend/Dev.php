<?php

namespace App\Http\Livewire\Frontend;

use App\Models\Client;
use App\Models\Portfolio;
use Livewire\Component;

class Dev extends Component
{
    public function render()
    {
        $description = __('dictionary.dev_seo_description');
        $title = 'IT Solutions';

        // dd(Portfolio::all());
        return view('livewire.frontend.dev', [
            'portfolio' => Portfolio::with('cover')
                ->whereHas('type', function ($q) {
                    $q->whereIn('type', [1, 2, 3]);
                })->orderByDesc('start')->get(),
            'title' => $title,
            'client' => Client::all(),
        ])->layout('layouts.frontend', [
            'description' => $description,
            'title' => $title,
            'type' => 'IT Project',
            'imgasset' => asset('IT.png')
        ]);
    }
}
