<?php

namespace App\Http\Livewire\Frontend;

use App\Models\Charity;
use App\Models\Portfolio;
use App\Models\Translator;
use Livewire\Component;

class About extends Component
{
    public function render()
    {
        $description = __('dictionary.about_seo_description');
        $title = 'About Us';

        return view('livewire.frontend.about', [
            'title' => $title,
            'project' => Portfolio::count(),
            'charity' => Charity::count(),
            'translation' => Translator::count()
        ])->layout('layouts.frontend', [
            'description' => $description,
            'title' => $title,
            'type' => 'Company Profile'
        ]);
    }
}
