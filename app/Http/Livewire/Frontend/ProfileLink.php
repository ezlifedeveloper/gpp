<?php

namespace App\Http\Livewire\Frontend;

use App\Models\User;
use Livewire\Component;

class ProfileLink extends Component
{
    public $slug;

    public function mount($slug)
    {
        $this->slug = $slug;
    }

    public function render()
    {
        $user = User::where('username', $this->slug)->first();
        if(!$user) {
            return abort(404);
        }

        return view('livewire.frontend.profile-link', [
            'user' => $user
        ]);
    }
}
