<?php

namespace App\Http\Livewire\Frontend;

use App\Models\Charity as CharityModel;
use App\Models\CharityFinance;
use App\Models\User;
use Livewire\Component;
use stdClass;

class Charity extends Component
{
    public $limitPerPage = 5;

    public $openedImage;
    public $previewImage = 0;
    public $imageSize = [48, 64, 56, 40, 72];

    protected $listeners = [
        'load-more' => 'loadMore'
    ];

    public function loadMore()
    {
        $this->limitPerPage = $this->limitPerPage + 5;
    }

    public function render()
    {
        $description = __('dictionary.charity_seo_description');
        $title = 'Charity';
        $charUser = User::whereIn('role', ['charity', 'superuser'])->inRandomOrder()->get();

        // top: 29% -> +-10 ; left: 2% -> +-15;
        $tot = $charUser->count();
        $row = floor($tot / 7);
        $i = 0;
        $top = 5;
        $left = -5;
        foreach ($charUser as $c) {
            if ($i % 7 == 0 && $i > 0) {
                $top += (100 / ($row + 1));
                $left = -10;
            }
            $left += 14;
            $dd = new stdClass;
            $dd->size = $this->imageSize[array_rand($this->imageSize)];
            $dd->style = 'top: ' . rand($top - 4, $top + 4) . '%; left: ' . rand($left - 3, $left + 3) . '%;';
            $dd->image = $c->photo;
            $dd->name = $c->name;
            $dd->username = $c->username;

            $participant[] = $dd;
            $i++;
        }

        // dd($participant);

        return view('livewire.frontend.charity', [
            'charity' => CharityModel::orderBy('date', 'desc')->paginate($this->limitPerPage),
            'participant' => $participant,
            'partner' => User::where('role', 'charity partner')->inRandomOrder()->get(),
            'finance' => CharityFinance::orderBy('date', 'desc')->take(5)->get(),
            'funded' => CharityFinance::where('value', '>', 0)->sum('value'),
        ])->layout('layouts.frontend', [
            'description' => $description,
            'title' => $title,
            'type' => 'Charity',
            'imgasset' => asset('charity.png')
        ]);
    }

    public function previewPhoto($image)
    {
        $this->previewImage = true;
        $this->openedImage = $image;
    }

    public function closePreviewPhoto()
    {
        $this->previewImage = false;
        $this->openedImage = null;
    }
}
