<?php

namespace App\Http\Livewire\Auth;

use App\Helpers\Helper;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Login extends Component
{
    /** @var string */
    public $username = '';

    /** @var string */
    public $password = '';

    /** @var bool */
    public $remember = false;

    protected $rules = [
        'username' => ['required'],
        'password' => ['required'],
    ];

    public function mount()
    {
        if (Auth::check()) return Auth::user()->role == 'superuser' ? redirect()->intended(route('admin.dashboard')) : redirect()->intended(route('admin.profile'));
    }

    public function authenticate()
    {
        $this->validate();
        if (!Auth::attempt(['username' => $this->username, 'password' => $this->password], $this->remember)) {
            $this->addError('username', trans('auth.failed'));

            Helper::recordUserLogin(null, $this->username, 'failed');
            return;
        }

        Helper::recordUserLogin(Auth::id(), $this->username, 'ok');
        return Auth::user()->role == 'superuser' ? redirect()->intended(route('admin.dashboard')) : redirect()->intended(route('admin.profile'));
    }

    public function render()
    {
        return view('livewire.auth.login')->extends('layouts.auth');
    }
}
