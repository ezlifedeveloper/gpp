<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ProfileLinkController extends Controller
{
    public function index($slug)
    {
        $user = User::where('username', $slug)->first();
        if(!$user) {
            return abort(404);
        }
        
        return view('profile-link', [
            'user' => $user
        ]);
    }
}
