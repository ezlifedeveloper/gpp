<?php

namespace App\Helpers;

use App\Models\Bible;
use App\Models\Kepengurusan;
use App\Models\LogLogin;
use App\Models\Mail;
use App\Models\Mapping;
use App\Models\UserLog;
use Illuminate\Support\Facades\Auth;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class Helper
{
    public static function formatRupiah($money)
    {
        return 'Rp ' . number_format($money, 2, ',', '.');
    }

    public static function formatRupiahShortened($money)
    {
        $sat = '';
        if ($money > 1000000000) {
            $sat = __('dictionary.currency_billion');
            $money /= 1000000000;
        } else if ($money > 1000000) {
            $sat = __('dictionary.currency_million');
            $money /= 1000000;
        } else if ($money > 1000) {
            $sat = __('dictionary.currency_thousand');
            $money /= 1000;
        }

        return 'Rp ' . floor($money) . ' ' . $sat;
    }

    public static function generateMailNumber($type, $date)
    {
        // dd(substr($date, 0, 7));
        $lastNumber = Mail::where('type', $type)
            ->whereRaw('SUBSTR(date, 1, 7)=' . "'" . substr($date, 0, 7) . "'")
            ->get();
        // dd($lastNumber);
        $number = count($lastNumber) + 1;

        if ($type == 'Invoice') {
            return $number . '/INV-GPP/' . SELF::numberToRomanRepresentation(substr($date, 5, 2)) . '/' . substr($date, 0, 4);
        } else if ($type == 'Letter') {
            return $number . '/GPP/' . SELF::numberToRomanRepresentation(substr($date, 5, 2)) . '/' . substr($date, 0, 4);
        } else if($type == 'Receipt') {
            return $number . '/RCPT-GPP/' . SELF::numberToRomanRepresentation(substr($date, 5, 2)) . '/' . substr($date, 0, 4);
        } else {
            return sprintf('%04u', $number);
        }
    }

    public static function isAllowed($role)
    {
        return Auth::user()->role == $role;
    }

    public static function getNameInitial($string)
    {
        $acronym = "";
        foreach (explode(' ', $string) as $word)
            $acronym .= mb_substr($word, 0, 1, 'utf-8');

        return strtoupper(substr($acronym, 0, 2));
    }

    public static function recordUserLogin($id, $username, $status)
    {
        LogLogin::create([
            'user_id' => $id,
            'status' => $status,
            'username' => $username,
        ]);
    }

    // public static function recordUserLog($id, $status, $ip = null)
    // {
    //     LogLogin::create([
    //         'user_id' => $id,
    //         'status' => $status,
    //         'ip_address' => $ip,
    //     ]);
    // }

    public static function uploadFile($file, $fileName, $folder)
    {
        $size = $file->getSize();
        $ext = '.' . $file->getClientOriginalExtension();

        $fixedFileName = preg_replace("/[^a-zA-Z0-9.]/", "", $fileName);

        $destinationPath = public_path() . $folder;
        $uploadedname = time() . $fixedFileName . $ext;

        $file->move($destinationPath, $uploadedname);
        $destinationPath = env('APP_URL') . $folder;

        return $destinationPath . $uploadedname;
    }

    public static function cleanStr($string)
    {
        // Replaces all spaces with hyphens.
        $string = str_replace(' ', '-', $string);

        // Removes special chars.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        // Replaces multiple hyphens with single one.
        $string = preg_replace('/-+/', '-', $string);

        return $string;
    }

    public static function numberToRomanRepresentation($number)
    {
        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if ($number >= $int) {
                    $number -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }
        return $returnValue;
    }
}
