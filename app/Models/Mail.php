<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mail extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at', 'date'];
    protected $fillable = ['no', 'sender', 'recipient', 'subject', 'type', 'status', 'date', 'value', 'file', 'created_by'];
}
