<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    use HasFactory;

    protected $connection = 'ezlife';
    protected $with = ['cover', 'type'];

    public function cover()
    {
        return $this->hasOne(FilePortfolio::class, 'portfolio')->latest();;
    }

    public function type()
    {
        return $this->hasMany(PortfolioType::class, 'portfolio');
    }
}
