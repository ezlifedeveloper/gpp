<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Finance extends Model
{
    use HasFactory;

    protected $fillable = [
        'date',
        'note',

        'category_id',
        'flow',
        'category',
        
        'amount',

        'account_id',
        'account_name',

        'created_by',
    ];
    
    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
