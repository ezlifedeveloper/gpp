<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CharityFinance extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['date', 'deleted_at'];
    protected $fillable = ['charity_id', 'detail', 'user_id', 'date', 'value', 'balance', 'created_by'];
    protected $with = ['user', 'charity'];

    public function charity()
    {
        return $this->belongsTo(Charity::class, 'charity_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
