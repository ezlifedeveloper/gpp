<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogCalculatorTranslator extends Model
{
    use HasFactory;

    protected $fillable = ['package', 'type', 'language', 'qty'];
}
