<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-full bg-gray-50 snippet-html js-focus-visible"
    data-js-focus-visible style="scroll-behavior: smooth;">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ isset($title) ? __($title).' - '.env('APP_NAME') : env('APP_NAME') }}</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ url(asset('favicon.ico')) }}">

    <!-- SEO Tag -->
    <meta name="description" content="{{ __($description) }}">
    <meta name="author" content="Grasia Prima Perfekta">
    <meta property="og:description" content="{{ __($description) }}" />
    <meta property="og:url" content="https://grasiapp.id" />
    <meta property="og:image" content="{{ $imgasset ?? asset('logo_bg.jpg') }}" />
    <meta property="og:title" content="{{ $title ? __($title).' - '.env('APP_NAME') : env('APP_NAME') }}" />
    <meta property="og:type" content="{{ $type ?? 'company' }}" />
    <meta property="og:locale:alternate" content="in_ID" />

    {{-- Facebook Domain Verificator --}}
    <meta name="facebook-domain-verification" content="9vhfbfqypx230s36vrubfmujnh5zcp" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-M1FB3JCHH2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-M1FB3JCHH2');
    </script>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@1,700&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://rsms.me/inter/inter.css">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ url(mix('css/app.css')) }}">
    <link rel="stylesheet" href="{{ asset('css/lightslider.css') }}">
    {{--
    <link rel="stylesheet" href="{{ asset('css/style.css') }}"> --}}
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    @livewireStyles

    <!-- Alpine Plugins -->
    <script defer src="https://unpkg.com/@alpinejs/intersect@3.x.x/dist/cdn.min.js"></script>

    <!-- Alpine Core -->
    <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="{{ asset('js/lightslider.js') }}" defer></script>
</head>

<body class="antialiased tracking-tight text-gray-900 bg-white font-inter dark:bg-gray-900 dark:text-gray-100" x-data>

    <!-- This example requires Tailwind CSS v2.0+ -->
    <div class="overflow-hidden bg-white">
        <x-frontend.navbar :title="$title"></x-frontend.navbar>
        <main>
            {{ $slot }}
        </main>
        <x-frontend.footer></x-frontend.footer>
    </div>

    @livewireScripts

    <!-- Scripts -->
    <script>
        AOS.init();
    </script>
</body>


</html>
