<!DOCTYPE html>
<html lang="en" class="h-full bg-gray-50 snippet-html js-focus-visible" data-js-focus-visible>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $title }} - User Page</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ url(asset('favicon.ico')) }}">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://rsms.me/inter/inter.css">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ url(mix('css/app.css')) }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" />
    
    @livewireStyles

    <!-- Scripts -->
    <script src="{{ url(mix('js/app.js')) }}" defer></script>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body class="h-full font-sans leading-normal tracking-normal bg-gray-800">
    <div class="min-h-full bg-gray-50">
        <x-admin.sidebar :title="$title"></x-admin.sidebar>

        <div class="flex flex-col flex-1 md:pl-64">
            <main class="flex-1">
                <div class="py-6">
                    <div class="px-4 mb-4 sm:px-6 md:pr-8">
                        <h1 class="text-2xl font-semibold text-gray-900">{{ $title }}</h1>
                    </div>
                    <div class="h-full px-4 sm:px-6 md:pr-8">
                        {{ $slot }}
                    </div>
                </div>
            </main>
        </div>
    </div>

    @livewireScripts
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <x-livewire-alert::scripts />
</body>

</html>
