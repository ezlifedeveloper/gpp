@extends('layouts.frontend')
@section('content')
<div>
    <x-frontend.hero></x-frontend.hero>
    <x-frontend.home-dev></x-frontend.home-dev>
    <x-frontend.home-translator></x-frontend.home-translator>
    <x-frontend.home-client></x-frontend.home-client>
</div>

@endsection
