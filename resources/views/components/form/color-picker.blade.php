@props(['title' => 'Warna Text', 'model' => 'inputColor', 'required' => true, 'placeholder' => '#FF0000'])
<div class="sm:col-span-1 mb-4">
    <label for="color" class="block text-sm font-medium text-gray-700">
        {{ $title }}
    </label>
    <div class="mt-1">
        <input type="color" name="color" id="color" wire:model='{{ $model }}' required
            class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
    </div>
</div>
