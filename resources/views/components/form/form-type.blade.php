<div class="sm:col-span-1 w-content mb-4">
    <label class="block text-sm font-medium text-gray-700">
        Jenis Pertanyaan
    </label>
    <div class="relative mt-1">
        <select wire:model="inputType" required
            class="block w-full px-3 py-2 mt-1 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            <option value="-">Pilih Jenis Pertanyaan</option>
            <option value="text">Text</option>
            <option value="checkbox">Checkbox</option>
            <option value="select">Select</option>
            <option value="radio">Radio</option>
        </select>
    </div>
</div>
