@props(['title'])
<div class="sm:col-span-1 mb-4">
    <label class="block text-sm font-medium text-gray-700">
        {{$title}}
    </label>
    <div class="mt-1">
        {{ $slot }}
    </div>
</div>
