@props(['title', 'model', 'required', 'multiple', 'disabled' => false, 'accept' => 'image/*,video/*,audio/*,application/pdf'])
<div class="sm:col-span-1 mb-4">
    <label class="block text-sm font-medium text-gray-700">
        {{$title}}
    </label>
    <div class="relative mt-1">
        <input type="file" wire:model.defer='{{ $model }}' {{ $required ? 'required' : '' }} {{ $multiple ? 'multiple' : '' }}
            {{ $disabled ? 'disabled' : '' }} accept="{{ $accept }}" 
            class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
        <div wire:loading wire:target="{{ $model }}">Uploading...</div>
    </div>
</div>
