@props(['title', 'model', 'row' => 3, 'required', 'disabled' => false])
<div class="sm:col-span-1 mb-4">
    <label class="block text-sm font-medium text-gray-700">
        {{ $title }}
    </label>
    <div class="mt-1">
        <textarea wire:model='{{ $model }}' {{ $required ? 'required' : '' }} {{ $disabled ? 'disabled' : '' }} rows="{{ $row }}"
            class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
        </textarea>
    </div>
</div>
