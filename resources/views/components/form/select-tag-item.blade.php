@props(['tag'])

<div class="sm:col-span-1 mb-4">
    <label id="listbox-label" class="block text-sm font-medium text-gray-700" @click="$refs.button.focus()">
        Tag
    </label>
    <div class="relative mt-1">
        <select id="tag" name="tag" wire:model="inputTag"
            class="block w-full px-3 py-2 mt-1 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">

            <option>---- Pilih Tag ----</option>
            @foreach ($tag as $d)
            <option value={{ $d->id }}>
                <div class="flex">
                    <div class="w-12" style="background-color: {{$d->color}}"></div>
                    <div >{{ $d->name }}</div>    
                </div>
            </option>
            @endforeach
        </select>
    </div>
</div>
