@props(['title', 'model', 'required', 'type', 'disabled' => false])
<div class="sm:col-span-1 mb-4">
    <label class="block text-sm font-medium text-gray-700">
        {{ $title }}
    </label>
    <div class="mt-1">
        @if($type=='date')
        <input type="text" id="myDatepicker" wire:model='{{ $model }}' {{ $required ? 'required' : '' }} {{ $disabled
            ? 'disabled' : '' }}
            class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
        @else
        <input type="text" id="myDateTimepicker" wire:model='{{ $model }}' {{ $required ? 'required' : '' }} {{
            $disabled ? 'disabled' : '' }}
            class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
        @endif

    </div>
</div>

@once
<script>
    flatpickr('#myDateTimepicker', {
        enableTime: true,
        dateFormat: "Y-m-d H:i",
        time_24hr: true
    })
    flatpickr('#myDatepicker', {
        enableTime: false,
        dateFormat: "Y-m-d"
    })
</script>
@endonce
