@props(['data'])
<div class="sm:col-span-3 flex-1 max-w-4xl">
    <div class="relative mt-1">
        <div class="w-full ">
            <div x-data="selectConfigs({{collect($data)}})" class="flex flex-col items-center relative">
                <div class="w-full">
                    <div @click.away="close()" class="bg-white flex roundedbg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500">
                        <input 
                               x-model="filter"
                               x-transition:leave="transition ease-in duration-100"
                               x-transition:leave-start="opacity-100"
                               x-transition:leave-end="opacity-0"
                               @mousedown="open()"
                               @keydown.enter.stop.prevent="selectOption()"
                               @keydown.arrow-up.prevent="focusPrevOption()"
                               @keydown.arrow-down.prevent="focusNextOption()"
                               class="px-3 py-1.5 appearance-none outline-none w-full text-gray-800">
                        <div class="text-gray-300 w-8 pl-1 flex items-center ">
                            <button @click="toggle()" class="cursor-pointer w-6 h-6 text-gray-600 outline-none focus:outline-none">
                                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                    <polyline x-show="!isOpen()" points="16 10 12 14 8 10" style="stroke-width: 1.3"></polyline>
                                    <polyline x-show="isOpen()" points="16 15 12 10 8 15" style="stroke-width: 1.3"></polyline>
                                </svg>
                              
                            </button>
                        </div>
                    </div>
                </div>
                <div x-show="isOpen()" class="absolute shadow bg-white top-100 z-40 w-full lef-0 rounded max-h-select overflow-y-auto svelte-5uyqqj">
                    <div class="flex flex-col w-full">
                      <template x-for="(option, index) in filteredOptions()" :key="index">
                        <div @click="onOptionClick(index)" :class="classOption(option.id, index)" :aria-selected="focusedOptionIndex === index">
                            <div class="flex w-full items-center p-2 pl-2 border-transparent border-l-2 relative hover:border-teal-100">
                                <div class="w-full items-center flex">
                                    <div class="mx-2 -mt-1"><span x-text="option.kode_satker + ' - ' + option.deskripsi"></span>
                                        <div class="text-xs truncate w-full normal-case font-normal -mt-1 text-gray-500" x-text="option.email"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </template>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
      function selectConfigs(data) {
        const initSelected = data.find(option => option.kode_satker.toLowerCase().indexOf(@this.satker) > -1)
          return {
              filter: initSelected.kode_satker + ' - ' + initSelected.deskripsi,
              show: false,
              selected: initSelected,
              focusedOptionIndex: null,
              options: data,
              close() { 
                this.show = false;
                this.filter = this.selectedName();
                this.focusedOptionIndex = this.selected ? this.focusedOptionIndex : null;
              },
              open() { 
                this.show = true; 
                this.filter = '';
              },
              toggle() { 
                if (this.show) {
                  this.close();
                }
                else {
                  this.open()
                }
              },
              isOpen() { return this.show === true },
              selectedName() { return this.selected ? this.selected.kode_satker + ' - ' + this.selected.deskripsi : this.filter; },
              classOption(id, index) {
                const isSelected = this.selected ? (id == this.selected.id) : false;
                const isFocused = (index == this.focusedOptionIndex);
                return {
                  'cursor-pointer w-full border-gray-100 border-b hover:bg-blue-50': true,
                  'bg-blue-100': isSelected,
                  'bg-blue-50': isFocused
                };
              },
              filteredOptions() {
                return this.options
                  ? this.options.filter(option => {
                      return (option.kode_satker.toLowerCase().indexOf(this.filter) > -1) 
                        || (option.deskripsi.toLowerCase().indexOf(this.filter) > -1)
                  })
                 : {}
              },
              onOptionClick(index) {
                this.focusedOptionIndex = index;
                this.selectOption();
              },
              selectOption() {
                if (!this.isOpen()) {
                  return;
                }
                this.focusedOptionIndex = this.focusedOptionIndex ?? 0;
                const selected = this.filteredOptions()[this.focusedOptionIndex]
                if (this.selected && this.selected.id == selected.id) {
                  // this.filter = '';
                  // this.selected = null;
                }
                else {
                  this.selected = selected;
                  this.filter = this.selectedName();
                  @this.setSatker(selected.kode_satker);
                }
                this.close();
              },
              focusPrevOption() {
                if (!this.isOpen()) {
                  return;
                }
                const optionsNum = Object.keys(this.filteredOptions()).length - 1;
                if (this.focusedOptionIndex > 0 && this.focusedOptionIndex <= optionsNum) {
                  this.focusedOptionIndex--;
                }
                else if (this.focusedOptionIndex == 0) {
                  this.focusedOptionIndex = optionsNum;
                }
              },
              focusNextOption() {
                const optionsNum = Object.keys(this.filteredOptions()).length - 1;
                if (!this.isOpen()) {
                  this.open();
                }
                if (this.focusedOptionIndex == null || this.focusedOptionIndex == optionsNum) {
                  this.focusedOptionIndex = 0;
                }
                else if (this.focusedOptionIndex >= 0 && this.focusedOptionIndex < optionsNum) {
                  this.focusedOptionIndex++;
                }
              }
          }
         
      }
  </script>
</div>