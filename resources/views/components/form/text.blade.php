@props(['title' => null, 'model', 'required', 'disabled' => false, 'defer' => true])
<div class="sm:col-span-1 @if($title) mb-4 @endif">
    @if($title)
    <label class="block text-sm font-medium text-gray-700">
        {{ $title }}
    </label>
    @endif
    <div class="mt-1">
        <input type="text" @if($defer) wire:model.defer='{{ $model }}' @else wire:model='{{ $model }}' @endif {{ $required ? 'required' : '' }} {{ $disabled ? 'disabled' : '' }}
            class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
    </div>
</div>
