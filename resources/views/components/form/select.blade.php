@props(['title', 'model', 'required', 'disabled' => false, 'useHeader' => true])
<div class="sm:col-span-1 w-content mb-4">
    @if($useHeader)
    <label class="block text-sm font-medium text-gray-700">
        {{$title}}
    </label>
    @endif
    <div class="relative mt-1">
        <select wire:model="{{$model}}" {{ $required ? 'required' : '' }} {{ $disabled ? 'disabled' : '' }}
            class="block w-full px-3 py-2 mt-1 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            {{ $slot }}
        </select>
    </div>
</div>
