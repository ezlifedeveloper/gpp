@props(['title'])
<div class="sm:col-span-1">
    <label class="block text-sm font-bold text-black">
        {{$title}}
    </label>
    <div class="mt-1">
        {{ $slot }}
    </div>
</div>
