@props(['model', 'label', 'required', 'disabled' => false, 'useHeader' => true])

<div class="w-full flex">
    <input class="appearance-none h-4 w-4 border border-gray-300 rounded-sm bg-white 
        checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 
        align-top bg-no-repeat bg-center bg-contain float-left mr-2" 
        type="checkbox" value="" id="{{ 'checkbox'.$model }}" 
        wire:model='{{ $model }}' {{ $required ? 'required' : '' }} {{ $disabled ? 'disabled' : ''}}>

    <label class="form-check-label inline-block text-gray-800" for="{{ 'checkbox'.$model }}">
        {{ $label }}
    </label>
</div>