@props(['user'])

<div>
    <label id="listbox-label" class="block text-sm font-medium text-gray-700">
        Select User
    </label>
    <div class="relative mt-1">
        <select id="user" name="user" wire:model="inputUser"
            class="block w-full px-3 py-2 mt-1 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">

            <option value="">---- Select User ----</option>
            @foreach ($user as $u)
            <option value={{$u->id}}>{{$u->name}}</option>
            @endforeach
        </select>
    </div>
</div>
