<div class="grid grid-cols-1 mt-6 gap-y-6 gap-x-4 md:grid-cols-2 xl:grid-cols-4">
    <div>
        <label for="name" class="block text-sm font-medium text-gray-700">
            Name
        </label>
        <div class="mt-1">
            <input type="text" name="name" id="name" wire:model='inputName'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="Nama Orang Baru">
        </div>
    </div>
    <div>
        <label for="username" class="block text-sm font-medium text-gray-700">
            Username
        </label>
        <div class="mt-1">
            <input type="text" name="username" id="username" wire:model='inputUsername'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="usernameorangbaru">
        </div>
    </div>
    <div>
        <label for="password" class="block text-sm font-medium text-gray-700">
            Password
        </label>
        <div class="mt-1">
            <input type="password" name="password" id="password" wire:model='inputPassword'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
        </div>
    </div>

    <div>
        <label id="listbox-label" class="block text-sm font-medium text-gray-700">
            Role
        </label>
        <div class="relative mt-1">
            <select id="role" name="role" wire:model="inputRole"
                class="block w-full px-3 py-2 mt-1 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">

                <option value="null">---- Select Role ----</option>
                <option>superuser</option>
                <option>charity</option>
                <option>charity partner</option>
            </select>
        </div>
    </div>

</div>
