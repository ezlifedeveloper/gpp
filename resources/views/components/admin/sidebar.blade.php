@props(['title'])

<div x-data="{ open: false }" @keydown.window.escape="open = false">
    <div x-show="open" class="fixed inset-0 z-40 flex md:hidden"
        x-description="Off-canvas menu for mobile, show/hide based on off-canvas menu state." x-ref="dialog"
        aria-modal="true" style="display: none;">

        <div x-show="open" x-transition:enter="transition-opacity ease-linear duration-300"
            x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100"
            x-transition:leave="transition-opacity ease-linear duration-300" x-transition:leave-start="opacity-100"
            x-transition:leave-end="opacity-0"
            x-description="Off-canvas menu overlay, show/hide based on off-canvas menu state."
            class="fixed inset-0 bg-gray-600 bg-opacity-75" @click="open = false" aria-hidden="true"
            style="display: none;">
        </div>

        <div x-show="open" x-transition:enter="transition ease-in-out duration-300 transform"
            x-transition:enter-start="-translate-x-full" x-transition:enter-end="translate-x-0"
            x-transition:leave="transition ease-in-out duration-300 transform" x-transition:leave-start="translate-x-0"
            x-transition:leave-end="-translate-x-full"
            x-description="Off-canvas menu, show/hide based on off-canvas menu state."
            class="relative flex flex-col flex-1 w-full max-w-xs bg-white" style="display: none;">

            <div x-show="open" x-transition:enter="ease-in-out duration-300" x-transition:enter-start="opacity-0"
                x-transition:enter-end="opacity-100" x-transition:leave="ease-in-out duration-300"
                x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0"
                x-description="Close button, show/hide based on off-canvas menu state."
                class="absolute top-0 right-0 pt-2 -mr-12" style="display: none;">
                <button type="button"
                    class="flex items-center justify-center w-10 h-10 ml-1 rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
                    @click="open = false">
                    <span class="sr-only">Close sidebar</span>
                    <svg class="w-6 h-6 text-gray-900" x-description="Heroicon name: outline/x"
                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor"
                        aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12">
                        </path>
                    </svg>
                </button>
            </div>

            <div class="flex-1 h-0 pt-5 pb-4 overflow-y-auto">
                <div class="flex items-center flex-shrink-0 px-4">
                    <a href="{{ route('home') }}">
                        <img class="w-auto h-8" src="{{ asset('logo.png') }}" alt="{{ env('APP_NAME') }}">
                    </a>
                </div>
                <nav class="px-2 mt-5">
                    @if(in_array(Auth::user()->role, ['superuser']))
                    <x-admin.sidebar-item :url="'admin.dashboard'" :name="'Dashboard'"
                        :active="($title=='Dashboard' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-4 text-gray-900-800"
                            x-description="Heroicon name: outline/home" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6">
                            </path>
                        </svg>
                    </x-admin.sidebar-item>
                    @endif

                    <x-admin.sidebar-item :url="'admin.link'" :name="'MyLink'"
                        :active="($title=='MyLink' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-4 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M13.828 10.172a4 4 0 00-5.656 0l-4 4a4 4 0 105.656 5.656l1.102-1.101m-.758-4.899a4 4 0 005.656 0l4-4a4 4 0 00-5.656-5.656l-1.1 1.1" />
                        </svg>
                    </x-admin.sidebar-item>

                    <x-admin.sidebar-item :url="'admin.profile'" :name="'Profile'"
                        :active="($title=='Profile' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-4 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                    </x-admin.sidebar-item>

                    <x-admin.sidebar-header :name="'SICK'"></x-admin.sidebar-header>

                    <x-admin.sidebar-item :url="'admin.charity'" :name="'Charity'"
                        :active="($title=='Charity' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-4 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                        </svg>
                    </x-admin.sidebar-item>

                    <x-admin.sidebar-item :url="'admin.charityfinance'" :name="'Charity Finance'"
                        :active="($title=='Charity Finance' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-4 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z" />
                        </svg>
                    </x-admin.sidebar-item>

                    @if(in_array(Auth::user()->role, ['superuser']))
                    <x-admin.sidebar-header :name="'Ez Life'"></x-admin.sidebar-header>

                    <x-admin.sidebar-item :url="'admin.client'" :name="'Client'"
                        :active="($title=='Client' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-4 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                        </svg>
                    </x-admin.sidebar-item>

                    <x-admin.sidebar-item :url="'admin.project'" :name="'Project'"
                        :active="($title=='Project' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-4 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253" />
                        </svg>
                    </x-admin.sidebar-item>

                    <x-admin.sidebar-item :url="'admin.translator'" :name="'Translator'"
                        :active="($title=='Translator' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-4 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M3 21v-4m0 0V5a2 2 0 012-2h6.5l1 1H21l-3 6 3 6h-8.5l-1-1H5a2 2 0 00-2 2zm9-13.5V9" />
                        </svg>
                    </x-admin.sidebar-item>
                    @endif

                    @if(in_array(Auth::user()->role, ['superuser']))
                    <x-admin.sidebar-header :name="'GPP'"></x-admin.sidebar-header>

                    <x-admin.sidebar-item :url="'admin.user'" :name="'User'" :active="($title=='User' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-4 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                        </svg>
                    </x-admin.sidebar-item>

                    <x-admin.sidebar-item :url="'admin.mail'" :name="'Mail'" :active="($title=='Mail' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-4 text-gray-900-800"
                            x-description="Heroicon name: outline/document-duplicate" xmlns="http://www.w3.org/2000/svg"
                            fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M8 7v8a2 2 0 002 2h6M8 7V5a2 2 0 012-2h4.586a1 1 0 01.707.293l4.414 4.414a1 1 0 01.293.707V15a2 2 0 01-2 2h-2M8 7H6a2 2 0 00-2 2v10a2 2 0 002 2h8a2 2 0 002-2v-2" />
                        </svg>
                    </x-admin.sidebar-item>

                    <x-admin.sidebar-header :name="'Finance'"></x-admin.sidebar-header>
                    <x-admin.sidebar-item :url="'admin.finance'" :name="'Finance'"
                        :active="($title=='Finance' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-4 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z" />
                        </svg>
                    </x-admin.sidebar-item>
                    <x-admin.sidebar-item :url="'admin.financeaccount'" :name="'Finance Account'"
                        :active="($title=='Finance Account' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-4 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-width="2" stroke-linejoin="round" d="M2.25 8.25h19.5M2.25 9h19.5m-16.5 5.25h6m-6 2.25h3m-3.75 3h15a2.25 2.25 0 002.25-2.25V6.75A2.25 2.25 0 0019.5 4.5h-15a2.25 2.25 0 00-2.25 2.25v10.5A2.25 2.25 0 004.5 19.5z" />
                        </svg>                          
                    </x-admin.sidebar-item>
                    <x-admin.sidebar-item :url="'admin.financecategory'" :name="'Finance Category'"
                        :active="($title=='Finance Category' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-4 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-width="2" stroke-linejoin="round" d="M8.25 6.75h12M8.25 12h12m-12 5.25h12M3.75 6.75h.007v.008H3.75V6.75zm.375 0a.375.375 0 11-.75 0 .375.375 0 01.75 0zM3.75 12h.007v.008H3.75V12zm.375 0a.375.375 0 11-.75 0 .375.375 0 01.75 0zm-.375 5.25h.007v.008H3.75v-.008zm.375 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z" />
                        </svg>                          
                    </x-admin.sidebar-item>
                    @endif
                </nav>
            </div>
            <div class="flex flex-shrink-0 p-4 border-t border-indigo-800">
                <a href="{{ route('logout') }}" class="flex-shrink-0 block group">
                    <div class="flex items-center">
                        <div>
                            <x-admin.sidebar-user-photo></x-admin.sidebar-user-photo>
                        </div>
                        <div class="ml-3">
                            <p class="text-base font-medium text-gray-900">
                                {{ Auth::user()->name }}
                            </p>
                            <p class="text-sm font-medium text-indigo-200 group-hover:text-gray-900">
                                Logout
                            </p>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="flex-shrink-0 w-14" aria-hidden="true">
            <!-- Force sidebar to shrink to fit close icon -->
        </div>
    </div>


    <!-- Static sidebar for desktop -->
    <div class="hidden md:flex md:w-64 md:flex-col md:fixed md:inset-y-0">
        <!-- Sidebar component, swap this element with another sidebar if you like -->
        <div class="flex flex-col flex-1 min-h-0 bg-white border-r border-gray-200">
            <div class="flex flex-col flex-1 pt-5 pb-4 overflow-y-auto">
                <div class="flex items-center flex-shrink-0 px-4">
                    <a href="{{ route('home') }}">
                        <img class="w-auto h-8" src="{{ asset('logo.png') }}" alt="{{ env('APP_NAME') }}">
                    </a>
                </div>
                <nav class="flex-1 px-2 mt-5">

                    @if(in_array(Auth::user()->role, ['superuser']))
                    <x-admin.sidebar-item :url="'admin.dashboard'" :name="'Dashboard'"
                        :active="($title=='Dashboard' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-3 text-gray-900-800"
                            x-description="Heroicon name: outline/home" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6">
                            </path>
                        </svg>
                    </x-admin.sidebar-item>
                    @endif

                    <x-admin.sidebar-item :url="'admin.profile'" :name="'Profile'"
                        :active="($title=='Profile' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-3 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                    </x-admin.sidebar-item>

                    <x-admin.sidebar-item :url="'admin.link'" :name="'MyLink'"
                        :active="($title=='MyLink' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-3 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M13.828 10.172a4 4 0 00-5.656 0l-4 4a4 4 0 105.656 5.656l1.102-1.101m-.758-4.899a4 4 0 005.656 0l4-4a4 4 0 00-5.656-5.656l-1.1 1.1" />
                        </svg>
                    </x-admin.sidebar-item>

                    <x-admin.sidebar-header :name="'SICK'"></x-admin.sidebar-header>

                    <x-admin.sidebar-item :url="'admin.charity'" :name="'Charity'"
                        :active="($title=='Charity' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-3 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                        </svg>
                    </x-admin.sidebar-item>

                    <x-admin.sidebar-item :url="'admin.charityfinance'" :name="'Charity Finance'"
                        :active="($title=='Charity Finance' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-3 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z" />
                        </svg>
                    </x-admin.sidebar-item>

                    @if(in_array(Auth::user()->role, ['superuser']))
                    <x-admin.sidebar-header :name="'Ez Life'"></x-admin.sidebar-header>

                    <x-admin.sidebar-item :url="'admin.client'" :name="'Client'"
                        :active="($title=='Client' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-3 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                        </svg>
                    </x-admin.sidebar-item>

                    <x-admin.sidebar-item :url="'admin.project'" :name="'Project'"
                        :active="($title=='Project' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-3 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253" />
                        </svg>
                    </x-admin.sidebar-item>

                    <x-admin.sidebar-item :url="'admin.translator'" :name="'Translator'"
                        :active="($title=='Translator' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-3 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M3 21v-4m0 0V5a2 2 0 012-2h6.5l1 1H21l-3 6 3 6h-8.5l-1-1H5a2 2 0 00-2 2zm9-13.5V9" />
                        </svg>
                    </x-admin.sidebar-item>
                    @endif

                    @if(in_array(Auth::user()->role, ['superuser']))
                    <x-admin.sidebar-header :name="'GPP'"></x-admin.sidebar-header>

                    <x-admin.sidebar-item :url="'admin.user'" :name="'User'" :active="($title=='User' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-3 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                        </svg>
                    </x-admin.sidebar-item>

                    <x-admin.sidebar-item :url="'admin.mail'" :name="'Mail'" :active="($title=='Mail' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-3 text-gray-900-800"
                            x-description="Heroicon name: outline/document-duplicate" xmlns="http://www.w3.org/2000/svg"
                            fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M8 7v8a2 2 0 002 2h6M8 7V5a2 2 0 012-2h4.586a1 1 0 01.707.293l4.414 4.414a1 1 0 01.293.707V15a2 2 0 01-2 2h-2M8 7H6a2 2 0 00-2 2v10a2 2 0 002 2h8a2 2 0 002-2v-2" />
                        </svg>
                    </x-admin.sidebar-item>

                    <x-admin.sidebar-header :name="'Finance'"></x-admin.sidebar-header>
                    <x-admin.sidebar-item :url="'admin.finance'" :name="'Finance'"
                        :active="($title=='Finance' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-3 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z" />
                        </svg>
                    </x-admin.sidebar-item>
                    <x-admin.sidebar-item :url="'admin.financeaccount'" :name="'Finance Account'"
                        :active="($title=='Finance Account' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-3 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-width="2" stroke-linejoin="round" d="M2.25 8.25h19.5M2.25 9h19.5m-16.5 5.25h6m-6 2.25h3m-3.75 3h15a2.25 2.25 0 002.25-2.25V6.75A2.25 2.25 0 0019.5 4.5h-15a2.25 2.25 0 00-2.25 2.25v10.5A2.25 2.25 0 004.5 19.5z" />
                        </svg>                          
                    </x-admin.sidebar-item>
                    <x-admin.sidebar-item :url="'admin.financecategory'" :name="'Finance Category'"
                        :active="($title=='Finance Category' ? true : false)">
                        <svg class="flex-shrink-0 w-6 h-6 mr-3 text-gray-900-800"
                            x-description="Heroicon name: outline/users" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-width="2" stroke-linejoin="round" d="M8.25 6.75h12M8.25 12h12m-12 5.25h12M3.75 6.75h.007v.008H3.75V6.75zm.375 0a.375.375 0 11-.75 0 .375.375 0 01.75 0zM3.75 12h.007v.008H3.75V12zm.375 0a.375.375 0 11-.75 0 .375.375 0 01.75 0zm-.375 5.25h.007v.008H3.75v-.008zm.375 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z" />
                        </svg>                          
                    </x-admin.sidebar-item>
                    @endif
                </nav>
            </div>
            <div class="flex flex-shrink-0 p-4 border-t border-gray-200">
                <a href="{{ route('logout') }}" class="flex-shrink-0 block w-full group">
                    <div class="flex items-center">
                        <div>
                            <x-admin.sidebar-user-photo></x-admin.sidebar-user-photo>
                        </div>
                        <div class="ml-3">
                            <p class="text-sm font-medium text-gray-700 group-hover:text-gray-900">
                                {{ Auth::user()->name }}
                            </p>
                            <p class="text-xs font-medium text-gray-500 group-hover:text-gray-700">
                                Logout
                            </p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="flex flex-col flex-1 md:pl-64">
        <div class="sticky top-0 z-10 pt-1 pl-1 bg-gray-100 md:hidden sm:pl-3 sm:pt-3">
            <button type="button"
                class="-ml-0.5 -mt-0.5 h-12 w-12 inline-flex items-center justify-center rounded-md text-gray-500 hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500"
                @click="open = true">
                <span class="sr-only">Open sidebar</span>
                <svg class="w-6 h-6" x-description="Heroicon name: outline/menu" xmlns="http://www.w3.org/2000/svg"
                    fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16">
                    </path>
                </svg>
            </button>
        </div>
    </div>
</div>
