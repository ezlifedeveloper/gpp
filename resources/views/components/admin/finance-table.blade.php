@props(['data'])

<div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8 sm:rounded-lg">
    <x-admin.table-header>
        <div class="flex-1">
            <h3 class="text-lg font-medium ">
                Charity Finance Data
            </h3>
            @if(Helper::isAllowed('superuser'))
            <x-admin.button-create-component></x-admin.button-create-component>
            @endif
        </div>
    </x-admin.table-header>
    <div class="overflow-hidden border-b border-gray-200 shadow">
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-blue-100">
                <tr class="font-bold text-black">
                    <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                        Date
                    </th>
                    <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                        Charity
                    </th>
                    <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                        User
                    </th>
                    <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                        Detail
                    </th>
                    <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                        Value
                    </th>
                    @if(!Helper::isAllowed('charity partner'))
                    <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                        Balance
                    </th>
                    @endif
                    @if(Helper::isAllowed('superuser'))
                    <th scope="col" class="relative px-6 py-3"></th>
                    @endif
                </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
                @forelse($data as $d)
                <tr class="@if($d->trashed()) text-gray-200 @endif">
                    <td class="px-6 py-4 whitespace-nowrap">
                        {{ $d->date->format('d M Y') }}
                    </td>
                    <td class="px-6 py-4 break-words">
                        {{ $d->charity->name ?? "" }}
                    </td>
                    <td class="px-6 py-4">
                        @if(Helper::isAllowed('superuser') || $d->user->role == 'charity partner' || $d->user->id==Auth::id())
                        <div class="flex items-center">
                            <div class="flex-shrink-0 w-10 h-10">
                                @if($d->user)
                                <x-admin.timeline-participant-item :participant="$d->user">
                                </x-admin.timeline-participant-item>
                                @endif
                            </div>
                        </div>
                        @endif
                    </td>
                    <td class="px-6 py-4 break-words">
                        {{ $d->detail }}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        @if(Helper::isAllowed('charity partner')) {{ Helper::formatRupiah(-1*$d->value) }}
                        @else {{ Helper::formatRupiah($d->value) }}
                        @endif
                    </td>
                    @if(!Helper::isAllowed('charity partner'))
                    <td class="px-6 py-4 whitespace-nowrap">
                        {{ Helper::formatRupiah($d->balance) }}
                    </td>
                    @endif

                    @if(Helper::isAllowed('superuser'))
                    <td class="gap-4 px-6 py-4 font-medium text-right">
                        @if(!$d->trashed())
                        <x-admin.button-edit-component :id="$d->id"></x-admin.button-edit-component>
                        @else
                        <x-admin.button-restore-component :id="$d->id"></x-admin.button-restore-component>
                        @endif
                        <x-admin.button-delete-component :id="$d->id"></x-admin.button-delete-component>
                    </td>
                    @endif
                </tr>
                @empty
                <tr>
                    <td class="col-span-6 px-6 py-4 text-sm">
                        No data available
                    </td>
                    <td class="col-span-6 px-6 py-4 text-sm"></td>
                    <td class="col-span-6 px-6 py-4 text-sm"></td>
                    <td class="col-span-6 px-6 py-4 text-sm"></td>
                    <td class="col-span-6 px-6 py-4 text-sm"></td>
                    <td class="col-span-6 px-6 py-4 text-sm"></td>

                    @if(Helper::isAllowed('superuser'))
                    <td class="col-span-6 px-6 py-4 text-sm"></td>
                    @endif
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="mt-4 text-xs">
        @if($data->hasPages())
        {{ $data->links() }}
        @endif
    </div>
</div>
