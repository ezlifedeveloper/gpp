@props(['type', 'status'])
<div class="grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-3">
    <x-admin.add-mail-type-item></x-admin.add-mail-type-item>
    <x-admin.add-mail-status-item></x-admin.add-mail-status-item>

    <div class="sm:col-span-1">
        <label for="subject" class="block text-sm font-medium text-gray-700">
            Judul Surat
        </label>
        <div class="mt-1">
            <input type="text" name="subject" id="subject" wire:model='inputSubject'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="">
        </div>
    </div>

    <div class="sm:col-span-1">
        <label for="sender" class="block text-sm font-medium text-gray-700">
            Dari
        </label>
        <div class="mt-1">
            <input type="text" name="sender" id="sender" wire:model='inputSender'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="PT. BBB">
        </div>
    </div>

    <div class="sm:col-span-1">
        <label for="recipient" class="block text-sm font-medium text-gray-700">
            Kepada
        </label>
        <div class="mt-1">
            <input type="text" name="recipient" id="recipient" wire:model='inputRecipient'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="PT. BBB">
        </div>
    </div>

    <div>
        <label for="date" class="block text-sm font-medium text-gray-700">
            Tanggal Surat
        </label>
        <div class="mt-1">
            <input type="date" name="date" id="date" wire:model='inputDate' wire:change='changeFormState'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
        </div>
    </div>

    <div class="sm:col-span-1">
        <label for="no" class="block text-sm font-medium text-gray-700">
            Nomor Surat
        </label>
        <div class="mt-1">
            <input type="text" name="no" id="no" wire:model='inputNo'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="">
        </div>
    </div>

    @if($type == 'Invoice')
    <div>
        <label for="value" class="block text-sm font-medium text-gray-700">
            Value
        </label>
        <div class="mt-1">
            <input type="number" name="value" id="value" wire:model='inputValue'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="1000000">
        </div>
    </div>
    @endif

    <div class="sm:col-span-1">
        <label for="file" class="block text-sm font-medium text-gray-700">
            File Scan / Foto
        </label>
        <div class="mt-1">
            <input type="file" name="file" id="file" wire:model="file"
                class="block w-full mt-1 border-gray-300 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            @error('file') <div class="invalid-feedback">{{ $message }}</div> @enderror
        </div>
    </div>
</div>
