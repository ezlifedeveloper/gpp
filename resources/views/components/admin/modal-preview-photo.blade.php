@props(['url'])

<div x-data="{ open: @entangle('previewImage') }" @keydown.window.escape="open = false" x-show="open"
    @click.away="$wire.closePreviewPhoto()" class="fixed inset-0 z-10 overflow-y-hidden" aria-labelledby="modal-title"
    x-ref="dialog" aria-modal="true">
    <div class="flex justify-center max-h-screen p-4 m-8 text-center">
        <div x-show="open" x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0"
            x-transition:enter-end="opacity-100" x-transition:leave="ease-in duration-200"
            x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0"
            x-description="Background overlay, show/hide based on modal state."
            class="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" @click="open = false" aria-hidden="true">
        </div>

        <!-- This element is to trick the browser into centering the modal contents. -->
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

        <!--
        Modal panel, show/hide based on modal state.

        Entering: "ease-out duration-300"
          From: "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          To: "opacity-100 translate-y-0 sm:scale-100"
        Leaving: "ease-in duration-200"
          From: "opacity-100 translate-y-0 sm:scale-100"
          To: "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
      -->
        <div x-show="open" x-transition:enter="ease-out duration-300"
            x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100" x-transition:leave="ease-in duration-200"
            x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
            x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            x-description="Modal panel, show/hide based on modal state."
            class="inline-block max-h-screen m-8 overflow-hidden text-left align-middle transition-all transform bg-white rounded-lg shadow-xl">
            <div>
                <img class="" src={{ $url ?? asset('logo.png')}}>
            </div>
        </div>
    </div>
</div>
