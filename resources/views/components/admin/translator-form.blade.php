<div class="grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-3">
    <div class="sm:col-span-1">
        <label for="name" class="block text-sm font-medium text-gray-700">
            Name
        </label>
        <div class="mt-1">
            <input type="text" name="name" id="name" wire:model='inputName'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="Bahasa Indonesia">
        </div>
    </div>

    <div class="sm:col-span-1">
        <label for="code" class="block text-sm font-medium text-gray-700">
            Code
        </label>
        <div class="mt-1">
            <input type="text" name="code" id="code" wire:model='inputCode'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="id">
        </div>
    </div>

    <div class="sm:col-span-1">
        <label for="contact" class="block text-sm font-medium text-gray-700">
            Contact
        </label>
        <div class="mt-1">
            <input type="text" name="contact" id="contact" wire:model='inputContact'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="Imo, Pras">
        </div>
    </div>
</div>
