<div>
    <label id="listbox-label" class="block text-sm font-medium text-gray-700">
        Select Mail Type
    </label>
    <div class="relative mt-1">
        <select id="type" name="type" wire:model="inputType" wire:change='changeFormState'
            class="block w-full px-3 py-2 mt-1 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">

            <option value="">---- Select Type ----</option>
            <option>Letter</option>
            <option>Invoice</option>
            <option>Receipt</option>
        </select>
    </div>
</div>
