<div class="grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-6">
    <div class="sm:col-span-6">
        <label for="name" class="block text-sm font-medium text-gray-700">
            Charity Title
        </label>
        <div class="mt-1">
            <input type="text" name="name" id="name" wire:model='inputName'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="Membagikan sembako kepada siapa saja">
        </div>
    </div>

    <div class="sm:col-span-6">
        <label for="detail" class="block text-sm font-medium text-gray-700">
            Charity Detail
        </label>
        <div class="mt-1">
            <textarea rows="4" name="detail" id="detail" wire:model='inputDetail'
                class="block w-full px-3 py-2 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="Kegiatan ini akan membagikan ... kepada .... orang. Makanan dibeli di ... "></textarea>
        </div>
    </div>

    <div class="sm:col-span-6">
        <label for="target" class="block text-sm font-medium text-gray-700">
            Charity Target
        </label>
        <div class="mt-1">
            <input type="text" name="target" id="target" wire:model='inputTarget'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="Pemulung, Tukang Becak, Pasukan Kuning, dll">
        </div>
    </div>

    <div class="sm:col-span-6">
        <label for="date" class="block text-sm font-medium text-gray-700">
            Date Planned
        </label>
        <div class="mt-1">
            <input type="date" name="date" id="date" wire:model='inputDate'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
        </div>
    </div>

    <div class="sm:col-span-6">
        <label for="report" class="block text-sm font-medium text-gray-700">
            Charity Report
        </label>
        <div class="mt-1">
            <textarea rows="4" name="report" id="report" wire:model='inputReport'
                class="block w-full px-3 py-2 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="Kegiatan telah dilaksanakan dengan ... kepada ... di ... "></textarea>
        </div>
    </div>
</div>
