@props(['data', 'loop', 'pagination' => true])
<td class="px-6 py-4 text-center">{{ ($pagination ? $data->perPage()*($data->currentPage()-1) : 0)+$loop->iteration}}</td>
