@props(['data'])

<div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8 sm:rounded-lg">
    <x-admin.table-header>
        <div class="flex-1">
            <h3 class="text-lg font-medium ">
                Mail Data
            </h3>
            @if(Helper::isAllowed('superuser'))
            <x-admin.button-create-component></x-admin.button-create-component>
            @endif
        </div>
    </x-admin.table-header>
    <div class="overflow-hidden border-b border-gray-200 shadow">
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-blue-100">
                <tr class="font-bold text-black">
                    <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                        No
                    </th>
                    <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                        Date
                    </th>
                    <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                        Type
                    </th>
                    <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                        Subject
                    </th>
                    <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                        File
                    </th>
                    @if(Helper::isAllowed('superuser'))
                    <th scope="col" class="relative px-6 py-3"></th>
                    @endif
                </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
                @forelse($data as $d)
                <tr class="@if($d->trashed()) text-gray-200 @endif">
                    <td class="px-6 py-4 whitespace-nowrap">
                        {{ $d->no }}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        {{ $d->date->format('d M Y') }}
                    </td>
                    <td class="px-6 py-4 break-words">
                        {{ $d->status.' - '.$d->type }}
                    </td>
                    <td class="px-6 py-4 break-words">
                        {{ $d->subject ?? "" }}<br />
                        <small>{{ $d->sender.' to '.$d->recipient }}</small>
                    </td>
                    <td class="px-6 py-4 text-blue-500 break-words">
                        @if($d->file)<a href={{ $d->file }} target="_blank">File</a>@endif
                    </td>

                    @if(Helper::isAllowed('superuser'))
                    <td class="gap-4 px-6 py-4 font-medium text-right">
                        @if(!$d->trashed())
                        <x-admin.button-edit-component :id="$d->id"></x-admin.button-edit-component>
                        @else
                        <x-admin.button-restore-component :id="$d->id"></x-admin.button-restore-component>
                        @endif
                        <x-admin.button-delete-component :id="$d->id"></x-admin.button-delete-component>
                    </td>
                    @endif
                </tr>
                @empty
                <tr>
                    <td class="col-span-6 px-6 py-4 text-sm">
                        No data available
                    </td>
                    <td class="col-span-6 px-6 py-4 text-sm"></td>
                    <td class="col-span-6 px-6 py-4 text-sm"></td>
                    <td class="col-span-6 px-6 py-4 text-sm"></td>
                    <td class="col-span-6 px-6 py-4 text-sm"></td>

                    @if(Helper::isAllowed('superuser'))
                    <td class="col-span-6 px-6 py-4 text-sm"></td>
                    @endif
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="mt-4 text-xs">
        @if($data->hasPages())
        {{ $data->links() }}
        @endif
    </div>
</div>
