<div>
    <label id="listbox-label" class="block text-sm font-medium text-gray-700">
        Select Mail Status
    </label>
    <div class="relative mt-1">
        <select id="status" name="status" wire:model="inputStatus" wire:change='changeFormState'
            class="block w-full px-3 py-2 mt-1 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">

            <option value="">---- Select Status ----</option>
            <option>In</option>
            <option>Out</option>
        </select>
    </div>
</div>
