@props(['id', 'data', 'col'])

<ul role="list"
    class="grid grid-cols-2 mt-2 gap-x-4 gap-y-8 sm:grid-cols-3 sm:gap-x-6 lg:grid-cols-{{$col}} xl:gap-x-8">
    @foreach ($data as $d)
    <x-admin.timeline-photo-item :id="$d->id" :photo="$d->photo"></x-admin.timeline-photo-item>
    @endforeach

    @if(Auth::check() && (Helper::isAllowed('superuser') || Auth::id() == 23))
    <button type="button" wire:click='toggleAddPhoto({{$id}})'
        class="relative block w-full px-4 py-6 text-center border-2 border-gray-300 border-dashed rounded-lg xl:p-12 hover:border-gray-400 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
        <span class="block mt-2 text-sm font-medium text-gray-900">
            Add More Photos
        </span>
    </button>
    @endif
</ul>
