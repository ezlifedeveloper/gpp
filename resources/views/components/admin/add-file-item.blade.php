<div>
    <label id="listbox-label" class="block text-sm font-medium text-gray-700" @click="$refs.button.focus()">
        Select File(s)
    </label>
    <div class="relative mt-1">
        <input type="file" wire:model="photos" class="form-control" multiple>
        @error('photos.*') <span class="error">{{ $message }}</span> @enderror
    </div>
</div>
