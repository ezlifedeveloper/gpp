<div class="grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-3">
    <div class="sm:col-span-1">
        <label for="name" class="block text-sm font-medium text-gray-700">
            Name
        </label>
        <div class="mt-1">
            <input type="text" name="name" id="name" wire:model='inputName'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="PT. Bahahaha Bahahaha Bahahaha">
        </div>
    </div>

    <div class="sm:col-span-1">
        <label for="short" class="block text-sm font-medium text-gray-700">
            Short Name
        </label>
        <div class="mt-1">
            <input type="text" name="short" id="short" wire:model='inputShort'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="PT. BBB">
        </div>
    </div>

    <div class="sm:col-span-1">
        <label for="photo" class="block text-sm font-medium text-gray-700">
            Logo
        </label>
        <div class="mt-1">
            <input type="file" name="photo" id="photo" wire:model="photo"
                class="block w-full mt-1 border-gray-300 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            @error('photo') <div class="invalid-feedback">{{ $message }}</div> @enderror
        </div>
    </div>
</div>
