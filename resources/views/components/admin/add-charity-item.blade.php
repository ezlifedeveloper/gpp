@props(['charity'])

<div>
    <label id="listbox-label" class="block text-sm font-medium text-gray-700" @click="$refs.button.focus()">
        Select Charity
    </label>
    <div class="relative mt-1">
        <select id="charity" name="charity" wire:model="inputCharity"
            class="block w-full px-3 py-2 mt-1 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">

            <option value="">---- Select Charity ----</option>
            @foreach ($charity as $u)
            <option value={{$u->id}}>{{$u->name.' - '.$u->date->format('d M Y')}}</option>
            @endforeach
        </select>
    </div>
</div>
