@props(['id', 'participants', 'showText'])

@if($showText)
<a href="#" class="font-medium text-gray-900">Participant</a>
@endif

<div class="relative z-0 flex mt-2">
    <div class="flex -space-x-2 row">
        @forelse($participants as $p)
        <x-admin.timeline-participant-item :participant="$p->user"></x-admin.timeline-participant-item>
        @empty
        @endforelse
    </div>

    @if(Auth::check() && (Helper::isAllowed('superuser') || Auth::id() == 23))
    <div class="flex items-center justify-center w-10 h-10 ml-2 bg-gray-600 rounded-full">
        <button wire:click='toggleAddParticipant({{$id}})'>
            <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 mx-auto" fill="none" viewBox="0 0 24 24"
                stroke="white">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M18 9v3m0 0v3m0-3h3m-3 0h-3m-2-5a4 4 0 11-8 0 4 4 0 018 0zM3 20a6 6 0 0112 0v1H3v-1z" />
            </svg>
        </button>
    </div>
    @endif
</div>
