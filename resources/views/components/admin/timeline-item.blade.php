@props(['data', 'isLast'])

@php
$initial = Helper::getNameInitial($data->name);
if($initial == 'HS') $color = 'black';
else if($initial == 'BB') $color = 'yellow-500';
else if($initial == 'BS') $color = 'blue-500';
else if($initial == 'NS') $color = 'gray-500';
else if($initial == 'YD') $color = 'purple-500';
else $color = 'green-500';
@endphp

<li class="@if($data->trashed()) bg-gray-300 @endif">
    <div class="relative pb-8 ">
        @if(!$isLast) <span class="absolute top-5 left-5 -ml-px h-full w-0.5 bg-gray-200" aria-hidden="true"></span>
        @endif
        <div class="relative flex items-start space-x-3 ">
            <div class="relative">
                <span class="inline-flex items-center justify-center h-10 w-10 rounded-full bg-{{$color}} ">
                    <span class="text-xl font-medium leading-none text-white">{{ $initial }}</span>
                </span>
            </div>
            <div class="flex-1 min-w-0">
                <div>
                    <div class="text-sm">
                        <a href="#" class="font-medium text-gray-900">{{ $data->name }}</a>
                        @if(Helper::isAllowed('superuser'))
                        <button wire:click='toggleEditCharity({{$data->id}})'
                            class="inline-flex items-center px-1 py-1.5 border border-transparent text-xs font-medium rounded-md
                            text-indigo-700 bg-indigo-100 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Edit Data
                        </button>
                        <button wire:click='deleteCharity({{$data->id}})'
                            onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()"
                            class="inline-flex items-center px-1 py-1.5 border border-transparent text-xs font-medium rounded-md
                            text-white bg-red-500 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-200">
                            Delete Data
                        </button>
                        @if($data->trashed())
                        <button wire:click='restoreCharity({{$data->id}})'
                            onclick="confirm('Are you sure to restore?') || event.stopImmediatePropagation()"
                            class="inline-flex items-center px-1 py-1.5 border border-transparent text-xs font-medium rounded-md
                            text-white bg-green-500 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-200">
                            Restore Data
                        </button>
                        @endif
                        @endif
                    </div>
                    <p class="mt-0.5 text-sm text-gray-500">
                        {{ $data->date->format('d M Y').' ('.$data->date->diffForHumans().')' }}
                    </p>
                </div>
                <div class="mt-2 text-sm text-gray-700">
                    <p>
                        {{ $data->detail }}
                    </p>
                </div>
                {{ $slot }}
            </div>
        </div>
    </div>
</li>


{{--
<li>
    <div class="relative pb-8">
        <span class="absolute top-5 left-5 -ml-px h-full w-0.5 bg-gray-200" aria-hidden="true"></span>
        <div class="relative flex items-start space-x-3">
            <div>
                <div class="relative px-1">
                    <div class="flex items-center justify-center w-8 h-8 bg-gray-100 rounded-full ring-8 ring-white">
                        <!-- Heroicon name: solid/user-circle -->
                        <svg class="w-5 h-5 text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                            fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd"
                                d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z"
                                clip-rule="evenodd" />
                        </svg>
                    </div>
                </div>
            </div>
            <div class="min-w-0 flex-1 py-1.5">
                <div class="text-sm text-gray-500">
                    <a href="#" class="font-medium text-gray-900">Hilary Mahy</a>
                    assigned
                    <a href="#" class="font-medium text-gray-900">Kristin Watson</a>
                    <span class="whitespace-nowrap">2d ago</span>
                </div>
            </div>
        </div>
    </div>
</li>
<li>
    <div class="relative pb-8">
        <span class="absolute top-5 left-5 -ml-px h-full w-0.5 bg-gray-200" aria-hidden="true"></span>
        <div class="relative flex items-start space-x-3">
            <div>
                <div class="relative px-1">
                    <div class="flex items-center justify-center w-8 h-8 bg-gray-100 rounded-full ring-8 ring-white">
                        <!-- Heroicon name: solid/tag -->
                        <svg class="w-5 h-5 text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                            fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd"
                                d="M17.707 9.293a1 1 0 010 1.414l-7 7a1 1 0 01-1.414 0l-7-7A.997.997 0 012 10V5a3 3 0 013-3h5c.256 0 .512.098.707.293l7 7zM5 6a1 1 0 100-2 1 1 0 000 2z"
                                clip-rule="evenodd" />
                        </svg>
                    </div>
                </div>
            </div>
            <div class="flex-1 min-w-0 py-0">
                <div class="text-sm leading-8 text-gray-500">
                    <span class="mr-0.5">
                        <a href="#" class="font-medium text-gray-900">Hilary Mahy</a>
                        added tags
                    </span>
                    <span class="mr-0.5">
                        <a href="#"
                            class="relative inline-flex items-center rounded-full border border-gray-300 px-3 py-0.5 text-sm">
                            <span class="absolute flex items-center justify-center flex-shrink-0">
                                <span class="h-1.5 w-1.5 rounded-full bg-rose-500" aria-hidden="true"></span>
                            </span>
                            <span class="ml-3.5 font-medium text-gray-900">Bug</span>
                        </a>
                        <a href="#"
                            class="relative inline-flex items-center rounded-full border border-gray-300 px-3 py-0.5 text-sm">
                            <span class="absolute flex items-center justify-center flex-shrink-0">
                                <span class="h-1.5 w-1.5 rounded-full bg-indigo-500" aria-hidden="true"></span>
                            </span>
                            <span class="ml-3.5 font-medium text-gray-900">Accessibility</span>
                        </a>
                    </span>
                    <span class="whitespace-nowrap">6h ago</span>
                </div>
            </div>
        </div>
    </div>
</li>

<li>
    <div class="relative pb-8">
        <span class="absolute top-5 left-5 -ml-px h-full w-0.5 bg-gray-200" aria-hidden="true"></span>
        <div class="relative flex items-start space-x-3">
            <div class="relative">
                <span class="flex items-center justify-center w-10 h-10 bg-green-500 rounded-full ring-8 ring-white">
                    <!-- Heroicon name: solid/check -->
                    <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                        fill="currentColor" aria-hidden="true">
                        <path fill-rule="evenodd"
                            d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                            clip-rule="evenodd" />
                    </svg>
                </span>
            </div>
            <div class="flex-1 min-w-0">
                <div>
                    <div class="text-sm">
                        <a href="#" class="font-medium text-gray-900">Eduardo Benz</a>
                    </div>
                    <p class="mt-0.5 text-sm text-gray-500">
                        Commented 6d ago
                    </p>
                </div>
                <div class="mt-2 text-sm text-gray-700">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tincidunt nunc ipsum
                        tempor purus vitae id. Morbi in vestibulum nec varius. Et diam cursus quis sed
                        purus nam.
                    </p>
                </div>
            </div>
        </div>
    </div>
</li>

<li>
    <div class="relative pb-8">
        <span class="absolute top-5 left-5 -ml-px h-full w-0.5 bg-gray-200" aria-hidden="true"></span>
        <div class="relative flex items-start space-x-3">
            <div>
                <div class="relative px-1">
                    <div class="flex items-center justify-center w-8 h-8 bg-gray-100 rounded-full ring-8 ring-white">
                        <!-- Heroicon name: solid/user-circle -->
                        <svg class="w-5 h-5 text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                            fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd"
                                d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z"
                                clip-rule="evenodd" />
                        </svg>
                    </div>
                </div>
            </div>
            <div class="min-w-0 flex-1 py-1.5">
                <div class="text-sm text-gray-500">
                    <a href="#" class="font-medium text-gray-900">Hilary Mahy</a>
                    assigned
                    <a href="#" class="font-medium text-gray-900">Kristin Watson</a>
                    <span class="whitespace-nowrap">2d ago</span>
                </div>
            </div>
        </div>
    </div>
</li>

<li>
    <div class="relative pb-8">
        <span class="absolute top-5 left-5 -ml-px h-full w-0.5 bg-gray-200" aria-hidden="true"></span>
        <div class="relative flex items-start space-x-3">
            <div>
                <div class="relative px-1">
                    <div class="flex items-center justify-center w-8 h-8 bg-gray-100 rounded-full ring-8 ring-white">
                        <!-- Heroicon name: solid/tag -->
                        <svg class="w-5 h-5 text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                            fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd"
                                d="M17.707 9.293a1 1 0 010 1.414l-7 7a1 1 0 01-1.414 0l-7-7A.997.997 0 012 10V5a3 3 0 013-3h5c.256 0 .512.098.707.293l7 7zM5 6a1 1 0 100-2 1 1 0 000 2z"
                                clip-rule="evenodd" />
                        </svg>
                    </div>
                </div>
            </div>
            <div class="flex-1 min-w-0 py-0">
                <div class="text-sm leading-8 text-gray-500">
                    <span class="mr-0.5">
                        <a href="#" class="font-medium text-gray-900">Hilary Mahy</a>
                        added tags
                    </span>
                    <span class="mr-0.5">
                        <a href="#"
                            class="relative inline-flex items-center rounded-full border border-gray-300 px-3 py-0.5 text-sm">
                            <span class="absolute flex items-center justify-center flex-shrink-0">
                                <span class="h-1.5 w-1.5 rounded-full bg-rose-500" aria-hidden="true"></span>
                            </span>
                            <span class="ml-3.5 font-medium text-gray-900">Bug</span>
                        </a>
                        <a href="#"
                            class="relative inline-flex items-center rounded-full border border-gray-300 px-3 py-0.5 text-sm">
                            <span class="absolute flex items-center justify-center flex-shrink-0">
                                <span class="h-1.5 w-1.5 rounded-full bg-indigo-500" aria-hidden="true"></span>
                            </span>
                            <span class="ml-3.5 font-medium text-gray-900">Accessibility</span>
                        </a>
                    </span>
                    <span class="whitespace-nowrap">6h ago</span>
                </div>
            </div>
        </div>
    </div>
</li> --}}
