@props(['user', 'charity'])

<div class="grid grid-cols-1 mt-6 gap-y-6 gap-x-4 md:grid-cols-2 xl:grid-cols-4">
    <x-admin.add-charity-item :charity="$charity"></x-admin.add-charity-item>
    <x-admin.add-participant-item :user="$user"></x-admin.add-participant-item>

    <div>
        <label for="value" class="block text-sm font-medium text-gray-700">
            Value
        </label>
        <div class="mt-1">
            <input type="number" name="value" id="value" wire:model='inputValue'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="1000000">
        </div>
    </div>

    <div>
        <label for="date" class="block text-sm font-medium text-gray-700">
            Date
        </label>
        <div class="mt-1">
            <input type="date" name="date" id="date" wire:model='inputDate'
                class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
        </div>
    </div>

    <div class="md:col-span-2 xl:col-span-4">
        <label for="detail" class="block text-sm font-medium text-gray-700">
            Detail
        </label>
        <div class="mt-1">
            <textarea rows="3" name="detail" id="detail" wire:model='inputDetail'
                class="block w-full px-3 py-2 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                placeholder="Pembelian .... di ... seharga ... untuk ..."></textarea>
        </div>
    </div>
</div>
