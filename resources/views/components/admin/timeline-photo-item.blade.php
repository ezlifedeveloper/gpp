@props(['id', 'photo'])

<li class="relative">
    <div wire:click.prevent="previewPhoto('{{ $photo }}')"
        class="overflow-hidden bg-gray-100 rounded-lg hover:scale-150 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500">
        <img src="{{ $photo }}" class="object-cover pointer-events-none group-hover:opacity-75">

        @if(Auth::check() && Helper::isAllowed('superuser'))
        <div class="object-cover w-full py-1 text-center bg-red-700">
            <a wire:click='deleteImage({{$id}})'
                onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()"
                class="text-white">Delete</a>
        </div>
        @endif
    </div>
</li>
