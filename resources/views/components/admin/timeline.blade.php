@props(['data', 'user', 'openedImage'])

<ul role="list" class="-mb-8">
    @foreach($data as $d)
    <x-admin.timeline-item :data="$d" :isLast="$loop->last">
        <div class="mt-6 text-sm">
            <x-admin.timeline-participant :id="$d->id" :participants="$d->participants" :showText="true">
            </x-admin.timeline-participant>
        </div>

        <div class="mt-8 text-sm">
            <a href="#" class="font-medium text-gray-900">Documentation</a>

            <x-admin.timeline-photo :id="$d->id" :data="$d->photos" :col="6"></x-admin.timeline-photo>
        </div>
    </x-admin.timeline-item>
    @endforeach

    <x-admin.modal-preview-photo :url="$openedImage"></x-admin.modal-preview-photo>
</ul>
