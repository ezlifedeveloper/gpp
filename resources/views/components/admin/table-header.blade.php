<div class="flex flex-row h-full p-4 mt-5 bg-white shadow">
    {{ $slot }}

    <x-admin.table-search-bar></x-admin.table-search-bar>
</div>
