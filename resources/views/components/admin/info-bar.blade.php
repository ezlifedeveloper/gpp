@props(['title', 'desc', 'color'])

<li class="flex col-span-1 rounded-md shadow-sm">
    <div
        class="flex items-center justify-center flex-shrink-0 w-16 text-sm font-medium text-white bg-{{ $color }} rounded-l-md">
        {{ $slot }}
    </div>
    <div
        class="flex items-center justify-between flex-1 truncate bg-white border-t border-b border-r border-gray-200 rounded-r-md">
        <div class="flex-1 px-4 py-2 text-sm truncate">
            <a href="#" class="font-medium text-gray-900 hover:text-gray-600">{{ $title }}</a>
            <p class="text-gray-500">{{ $desc }}</p>
        </div>
    </div>
</li>
