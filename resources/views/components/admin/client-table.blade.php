@props(['data'])

<div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8 sm:rounded-lg">
    <x-admin.table-header>
        <div class="flex-1">
            <h3 class="text-lg font-medium ">
                Client Data
            </h3>
            <x-admin.button-create-component></x-admin.button-create-component>
        </div>
    </x-admin.table-header>
    <div class="overflow-hidden border-b border-gray-200 shadow">
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-blue-100">
                <tr class="font-bold text-black">
                    <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                        Name
                    </th>
                    <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                        Logo
                    </th>
                    <th scope="col" class="relative px-6 py-3"></th>
                </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
                @forelse($data as $d)
                <tr class="@if($d->trashed()) text-gray-200 @endif">
                    <td class="px-6 py-4 whitespace-nowrap">
                        {{ $d->name }}
                    </td>
                    <td class="px-6 py-4 break-words">
                        <img class="h-16" src={{ $d->photo }}>
                    </td>

                    <td class="gap-4 px-6 py-4 font-medium text-right">
                        @if(!$d->trashed())
                        <x-admin.button-edit-component :id="$d->id"></x-admin.button-edit-component>
                        @else
                        <x-admin.button-restore-component :id="$d->id"></x-admin.button-restore-component>
                        @endif
                        <x-admin.button-delete-component :id="$d->id"></x-admin.button-delete-component>
                    </td>
                </tr>
                @empty
                <tr>
                    <td class="col-span-6 px-6 py-4 text-sm">
                        No data available
                    </td>
                    <td class="col-span-6 px-6 py-4 text-sm"></td>
                    <td class="col-span-6 px-6 py-4 text-sm"></td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="mt-4 text-xs">
        @if($data->hasPages())
        {{ $data->links() }}
        @endif
    </div>
</div>
