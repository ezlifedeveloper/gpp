@props(['url', 'name', 'active'])

<a href="{{ route($url) }}" @if($active)
    class="bg-gray-100 text-gray-900 group flex items-center px-2 py-2 text-sm font-medium rounded-md"
    x-state:on="Current" x-state:off="Default"
    x-state-description="Current: &quot;bg-gray-100 text-gray-900&quot;, Default: &quot;text-gray-600 hover:bg-gray-50 hover:bg-opacity-75&quot;"
    @else class="text-gray-600 hover:bg-gray-50 hover:bg-opacity-75 group flex items-center px-2 py-2 text-sm font-medium
    rounded-md" x-state-description="undefined: &quot;bg-gray-100 text-gray-900&quot;, undefined: &quot;text-gray-600
    hover:bg-gray-50 hover:bg-opacity-75&quot;" @endif>
    {!! $slot !!}

    {{ $name }}
</a>
