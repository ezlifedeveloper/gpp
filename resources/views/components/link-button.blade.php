@props(['textcolor' => 'white', 'bgcolor' => 'blue', 'text' => 'Link Button', 'href' => '#', 'id' => null, 'admin' => false])

<style>
    a#myLinkId{{$id}} {
        color: @php echo $textcolor; @endphp;
        background-color: @php echo $bgcolor.'80'; @endphp;
        border-radius: 5px;
        padding: 10px;
        text-decoration: none;
        transition: all 0.2s ease-in-out;
    }
    :hover#myLinkId{{$id}} {
        background-color: @php echo $bgcolor; @endphp;
    }
</style>

<a id="myLinkId{{$id}}" href="{{ $href }}" target="_blank" class="flex flex-1 w-6/12 px-4 py-3 max-w-3xl mt-5 rounded-full gap-3 border-2 shadow-lg font-bold"
    > 
    <!-- Button tag -->
    {{-- <i class="fa fa-fan text-3xl"></i>
    <div class="w-1 bg-blue-500"></div> --}}
    <span class="flex-1 text-xl font-semibold text-center my-auto">{{$text}}</span>
</a>
@if($admin)
<div class="flex mt-3 gap-1">
    <button wire:click="editLink({{$id}})"
        class="px-2 py-1 rounded-sm bg-yellow-500 text-white border-2 border-white-500">
        EDIT
    </button>
    <button wire:click="deleteLink({{$id}})" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()"
        class="px-2 py-1 rounded-sm bg-red-500 text-white border-2 border-white-500">
        DELETE
    </button>
</div>
@endif