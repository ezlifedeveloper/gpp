@props(['view'])

<div class="relative">
    <!-- Item active: "text-gray-900", Item inactive: "text-gray-500" -->
    <button type="button" @click="open = !open "
        class="inline-flex items-center text-base font-medium text-gray-500 bg-white rounded-md group hover:text-gray-900"
        aria-expanded="false">
        <img class="h-4 mr-2" src="{{ asset('flag/'.LaravelLocalization::getCurrentLocale().'.png') }}">
        <span>{{LaravelLocalization::getCurrentLocaleNative()}}</span>

        <svg class="w-5 h-5 ml-2 text-gray-400 group-hover:text-gray-500" xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
            <path fill-rule="evenodd"
                d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                clip-rule="evenodd" />
        </svg>
    </button>

    @php
    $class = $view == 'web' ? 'absolute z-10 px-2 mt-3 -ml-4 transform w-content sm:px-0 lg:ml-0 lg:left-1/2
    lg:-translate-x-1/2' : 'absolute inset-x-0 top-0 z-30 p-2 transition origin-top-right transform md:hidden';
    @endphp
    <div x-show="open" x-transition:enter="duration-200 ease-out" x-transition:enter-start="opacity-0 translate-y-1"
        x-transition:enter-end="opacity-100 translate-y-0" x-transition:leave="duration-150 ease-in"
        x-transition:leave-start="opacity-100 translate-y-0" x-transition:leave-end="opacity-0 translate-y-1"
        class="{{$class}}" style="display: none;">
        <div class="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5">
            <div class="relative grid gap-6 px-5 py-6 bg-white sm:gap-8 sm:p-8">
                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                <a rel="alternate" hreflang="{{ $localeCode }}"
                    href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}"
                    class="flex items-center p-3 rounded-lg hover:bg-gray-50">
                    <!-- Heroicon name: outline/chart-bar -->
                    <img class="h-6 -ml-2" src="{{ asset('flag/'.$localeCode.'.png') }}">
                    @if($view=='web')
                    <div class="ml-2 mr-8">
                        <p class="text-base font-medium text-gray-900 whitespace-nowrap">
                            {{ $properties['native'] }}
                        </p>
                    </div>
                    @endif
                </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
