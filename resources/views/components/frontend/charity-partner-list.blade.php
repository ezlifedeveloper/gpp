@props(['partner'])
<section>
    <div class="max-w-3xl px-4 py-8 mx-auto my-8 sm:px-6">
        <div class="flex items-center justify-center w-full gap-y-10 gap-x-6 xl:gap-x-8">
            @php $time = 300; @endphp
            @foreach ($partner as $d)
            <div class="relative group" data-aos="fade-left" data-aos-delay="{{ $time+=150 }}">
                <a href="{{ route('link', $d->username) }}">
                    <div class="bg-white rounded-lg shadow-xl md:order-1 w-60">
                        <div class="h-8 bg-white col-span-3" style="@if($d->cover) background-image: url({{$d->cover}}) @else background-color: black @endif"></div>
                        <div class="flex gap-2 p-4 items-center">
                            <div class="flex-none w-12 h-12">
                            @if($d->photo)
                            <img src="{{ $d->photo ?? 'https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80' }}"
                                alt="{{ $d->name }}" class="absolute z-10 object-cover rounded-full animate-float w-12 h-12" data-aos="fade-up"
                                data-aos-delay="{{$time+=150}}">
                            @else
                            <span class="absolute z-10 inline-flex items-center justify-center w-12 h-12 bg-blue-500 rounded-full animate-float"
                                style="{{ $d->style }}" data-aos="fade-up" data-aos-delay="{{$time+=150}}">
                                <span class="text-xl font-medium text-white">{{ Helper::getNameInitial($d->name) }}</span>
                            </span>
                            @endif
                            </div>
                            <div class="flex-1">
                            <h3 class="text-xl font-bold text-black">{{ $d->name }}</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>
