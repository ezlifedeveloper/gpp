@props(['title', 'description', 'price', 'unit', 'items', 'note'])
<div class="relative flex flex-col p-8 bg-white border border-gray-200 shadow-sm rounded-2xl">
    <div class="flex-1">
        <h3 class="text-lg font-semibold text-gray-900">{{ $title }}</h3>
        @if($note==__('dictionary.express'))
        <p
            class="absolute top-0 py-1.5 px-4 bg-green-500 rounded-full text-xs font-semibold uppercase tracking-wide text-white transform -translate-y-1/2">
            {{ __("dictionary.express") }}</p>
        @else
        <p
            class="absolute top-0 py-1.5 px-4 bg-indigo-500 rounded-full text-xs font-semibold uppercase tracking-wide text-white transform -translate-y-1/2">
            {{ __("dictionary.regular") }}</p>
        @endif
        <p class="mt-6 text-xs">{{ __('dictionary.start_from') }}</p>
        <p class="flex items-baseline text-gray-900">
            <span class="text-2xl font-extrabold tracking-tight">{{ __('dictionary.currency').$price }}</span>
            <span class="ml-1 text-lg font-semibold">/{{ $unit }}</span>
        </p>
        <p class="mt-6 text-gray-500">{{ $description }}</p>

        <!-- Feature list -->
        <ul role="list" class="mt-6 space-y-6">
            @foreach ($items as $item)
            <li class="flex">
                <svg class="flex-shrink-0 w-6 h-6 text-indigo-500" x-description="Heroicon name: outline/check"
                    xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor"
                    aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20 12H4" />
                </svg>
                <span class="ml-3 text-gray-500">{{$item}}</span>
            </li>
            @endforeach
        </ul>
    </div>

    <a href="#calculator" wire:click="setValue('{{$title}}', '{{$note}}');" class="block w-full px-6 py-3 mt-8 font-medium text-center border border-transparent rounded-md
        @if($note==__('dictionary.express')) text-white bg-green-500 hover:bg-green-700 @else text-white bg-indigo-500
        hover:bg-indigo-700 @endif ">
        {{ __('dictionary.calculate_price') }}
    </a>
</div>
