@props(['image', 'title', 'description', 'number', 'slug'])
<div class="flex flex-col overflow-hidden rounded-lg shadow-lg h-full bg-gray-50" data-aos="fade-up" data-aos-delay="{{300+(($number-1)*200)}}">
    <a href="{{ 'https://ezlife.id/portfolio/'.$slug }}">
    <div class="flex-1">
        <img class="object-cover w-full" src="{{ str_replace('8000', '8001', $image) }}" width="100" height="100" alt="{{ $title }}">
    </div>
    <div class="flex flex-col justify-between flex-1 p-2 h-full">
        <div class="mt-2">
            <p class="text-md font-semibold text-gray-900">
                {{ $title }}
            </p>
        </div>
    </div>
    </a>
</div>
