@props(['title', 'description'])
<div class="pt-6">
    <div class="flow-root px-6 pb-8 bg-white rounded-lg shadow-lg" data-aos="fade-up">
        <div class="-mt-6">
            <div>
                <span
                    class="inline-flex items-center justify-center p-3 rounded-md shadow-lg bg-gradient-to-r from-purple-500 to-blue-600">
                    <svg class="w-6 h-6 text-white" x-description="Heroicon name: outline/cloud-upload"
                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor"
                        aria-hidden="true">
                        {{ $slot }}
                    </svg>
                </span>
            </div>
            <h3 class="mt-8 text-lg font-medium tracking-tight text-gray-900">{{ $title }}</h3>
            <p class="mt-5 text-base text-gray-500">
                {{ $description }}
            </p>
        </div>
    </div>
</div>
