@props(['image', 'title', 'description', 'number', 'slug', 'client'])
<div class="flex flex-row md:col-span-3 lg:col-span-6 overflow-hidden w-full rounded-lg shadow-lg bg-gray-50" data-aos="fade-up">
    @if($number%2)
    <div class="flex-2">
        <img class="object-cover h-full" src="{{ str_replace('8000', '8001', $image) }}" width="200" height="200" alt="{{$title}}">
    </div>
    @endif
    <div class="flex flex-1 p-6">
        <a href="{{ 'https://ezlife.id/portfolio/'.$slug }}" class="block mt-2">
            <p class="text-xl font-semibold text-gray-900">
                {{ $title }}
            </p>
            @if($client)
            <p class="text-base font-medium text-gray-900">
                {{ $client }}
            </p>
            @endif
            <p class="mt-3 text-base text-gray-700">
                {!! substr($description, 0, 250).'...' !!}
            </p>
        </a>
    </div>
    @if(!($number%2))
    <div class="flex-2">
        <img class="object-cover h-full" src="{{ str_replace('8000', '8001', $image) }}" width="200" height="200" alt="{{$title}}">
    </div>
    @endif
</div>
