@props(['data'])

<script type="text/javascript">
    $(document).ready(function() {
        $("#charity-light-slider").lightSlider({
            item : 4,
            slideMove:2,
            easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
            speed:600,
            responsive : [{
                    breakpoint:1360,
                    settings: {
                        item:4,
                        slideMove:1,
                        slideMargin:6,
                    }
                },
                {
                    breakpoint:1120,
                    settings: {
                        item:3,
                        slideMove:1,
                        slideMargin:6,
                    }
                },
                {
                    breakpoint:800,
                    settings: {
                        item:2,
                        slideMove:1,
                        slideMargin:6,
                    }
                },
                {
                    breakpoint:640,
                    settings: {
                        item:1,
                        slideMove:1
                }
            }]
        });
        });
</script>

<div class="bg-white">
    <div class="max-w-2xl px-4 py-16 mx-auto sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8">
        <h2 class="mb-4 text-3xl font-extrabold tracking-tight text-center" data-aos="fade-left" data-aos-delay="450">
            {{ __('Charity') }}
        </h2>
        <h4 class="mb-8 text-xl tracking-tight text-center text-gray-900" data-aos="fade-right" data-aos-delay="450">
            {{ __('dictionary.charity_description') }}
        </h4>

        <ul id="charity-light-slider" data-aos="fade-up" data-aos-delay="450">
            @foreach ($data as $d)
            <li>
                <div class="relative group">
                    <div
                        class="w-full overflow-hidden bg-gray-200 rounded-md min-h-80 aspect-w-1 aspect-h-1 group-hover:opacity-75 lg:h-80 lg:aspect-none">
                        <img src="{{ $d->cover->photo }}" alt="{{ $d->name }}"
                            class="object-cover object-center w-full h-full lg:w-full lg:h-full">
                    </div>
                    <div class="flex justify-between mt-4">
                        <div>
                            <h3 class="text-sm text-gray-700">
                                <a href="{{ url('charity/'.$d->slug) }}">
                                    <span aria-hidden="true" class="absolute inset-0"></span>
                                    <b>{{ $d->name }}</b>
                                </a>
                            </h3>
                            <p class="mt-1 text-sm text-gray-500">{{ $d->date}}</p>
                        </div>
                        {{-- <p class="text-sm font-medium text-gray-900">$35</p> --}}
                    </div>
                </div>
            </li>
            @endforeach
        </ul>

        <div class="max-w-xl mx-auto mt-10 mb-8 text-xl tracking-tight text-center text-gray-900" data-aos="fade-up"
            data-aos-delay="450">
            <x-frontend.button-big :title="'See More Info'" :link="route('charity')" :color="'gray'" :newtab="false">
            </x-frontend.button-big>
        </div>
    </div>
</div>
