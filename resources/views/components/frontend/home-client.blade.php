@props(['data'])

<div class="bg-white">
    <div class="px-4 py-16 mx-auto max-w-7xl sm:px-6 lg:px-8">
        <p class="text-sm font-semibold tracking-wide text-center text-gray-500 uppercase">
            {{ __('Our Clients') }}
        </p>
        <div class="grid grid-cols-2 gap-10 mt-6 md:grid-cols-4 lg:grid-cols-4">
            @php $time = 300; @endphp
            @foreach ($data as $d)
            <div x-data="{ tooltip: false }" data-aos="fade-up" data-aos-delay="{{ $time+=150 }}">
                <div class="flex justify-center col-span-1 mt-4 lg:mt-8 md:col-span-2 lg:col-span-1">
                    <img x-on:mouseover="tooltip = true" x-on:mouseleave="tooltip = false" class="h-24"
                        src=" {{ $d->photo }}" alt="{{ $d->name }}">
                </div>

                <div class="relative" x-cloak x-show.transition.origin.top="tooltip">
                    <div
                        class="absolute top-0 z-10 w-32 p-2 -mt-1 text-sm leading-tight text-white transform -translate-x-1/2 -translate-y-full bg-blue-500 rounded-lg shadow-lg">
                        {{ $d->name }}
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
