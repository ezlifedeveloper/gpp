@props(['data'])

<!-- component -->

<div>
    <div class="py-8 mx-4 md:mx-auto md:max-w-7xl sm:py-24 sm:px-6 lg:px-8">

        @foreach ($data as $d)
        @if($loop->index%3==0 && ($data->total()-3>$loop->index))
        <div class="h-full border border-gray-700 md:absolute border-2-2 border-opacity-20" style="left: 50%">
        </div>
        @endif

        <div @if($loop->index%2) data-aos="fade-left" @else data-aos="fade-right" @endif data-aos-delay="450"
            class="md:flex items-center justify-between md:w-full md:mb-8 @if($loop->index%2) md:right-timeline @else
            md:flex-row-reverse md:left-timeline @endif">
            <div class="md:order-1 md:w-5/12"></div>
            <div
                class="items-center mx-16 my-4 text-center bg-purple-800 rounded-full shadow-xl sm:mx-32 max-w-content md:p-2 md:mx-2 md:z-10 md:flex md:order-1">
                <h1 class="text-lg font-semibold text-white md:mx-auto">{{ $d->date->format('d M Y') }}</h1>
            </div>
            <div class="px-6 py-4 bg-white rounded-lg shadow-xl md:order-1 md:w-5/12">
                <h3 class="mb-3 text-xl font-bold text-purple-800">{{ $d->name }}</h3>
                <p class="mb-4 text-sm leading-snug tracking-wide text-black text-opacity-100">{{ $d->detail }}</p>
                <x-admin.timeline-participant :id="$d->id" :participants="$d->participants" :showText="false">
                </x-admin.timeline-participant>

                <div class="mt-4 text-right ">
                    <a class="px-4 py-2 text-sm text-white rounded-full shadow-xl" style="background: #4314ff"
                        href="{{ url('charity/'.$d->slug) }}">{{ __('dictionary.see_detail') }}</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
