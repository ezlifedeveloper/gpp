@props(['translator'])
<section>
    <div class="max-w-6xl px-4 mx-auto sm:px-6">
        <div class="py-12 md:py-20 border-slate-200">

            <!-- Section content -->
            <div class="flex flex-col max-w-xl mx-auto space-y-8 space-y-reverse md:max-w-none md:flex-row md:items-start md:space-x-8 lg:space-x-16 xl:space-x-18 md:space-y-0"
                x-data="{ tab: '1' }">

                <!-- Tabs items (images) -->
                <div class="order-1 md:rtl md:w-5/12 lg:w-1/2 md:order-none" data-aos="fade-down">
                    <div class="relative flex flex-col pt-8">
                        @foreach ($translator as $t)
                        <div class="w-full" x-show="tab === '{{$loop->index+1}}'"
                            x-transition:enter="transition ease-in-out duration-700 transform order-first"
                            x-transition:enter-start="opacity-0 -translate-y-16"
                            x-transition:enter-end="opacity-100 translate-y-0"
                            x-transition:leave="transition ease-in-out duration-300 transform absolute"
                            x-transition:leave-start="opacity-100 translate-y-0"
                            x-transition:leave-end="opacity-0 translate-y-16">
                            <img class="w-full mx-auto rounded shadow-lg" src="{{ $t->flag }}" alt="{{ $t->name }}" />
                        </div>
                        @endforeach
                    </div>
                </div>

                <!-- Content -->
                <div class="md:w-7/12 lg:w-1/2" data-aos="fade-up">
                    <div class="mb-8 text-center md:text-left">
                        <h3 class="mb-3 text-xl font-bold h3 text-slate-800">{{
                            __('dictionary.translation_available')
                            }}</h3>
                    </div>
                    <!-- Tabs buttons -->
                    <div class="mb-8 md:mb-0">
                        @foreach ($translator as $t)
                        <button data-aos="fade-left"
                            :class="tab !== '{{$loop->index+1}}' ? 'border-transparent opacity-50 hover:opacity-75' : 'border-2 border-blue-500 opacity-100'"
                            class="flex items-start w-full px-5 py-3 mb-3 text-left transition duration-300 ease-in-out bg-white border-2 rounded shadow-md"
                            @click="tab = '{{$loop->index+1}}'">
                            <svg class="w-4 h-4 mt-1 mr-4 text-blue-600 fill-current shrink-0" viewBox="0 0 16 16"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M15.686 5.71 10.291.3c-.4-.4-.999-.4-1.399 0a.97.97 0 0 0 0 1.403l.6.6L2.698 6.01l-1-1.002c-.4-.4-.999-.4-1.398 0a.97.97 0 0 0 0 1.403l1.498 1.502 2.398 2.404L.6 14.023 2 15.425l3.696-3.706 3.997 4.007c.5.5 1.199.2 1.398 0a.97.97 0 0 0 0-1.402l-.999-1.002 3.697-6.711.6.6c.599.602 1.199.201 1.398 0 .3-.4.3-1.1-.1-1.502Zm-7.193 6.11L4.196 7.511l6.695-3.706 1.298 1.302-3.696 6.711Z" />
                            </svg>
                            <div>
                                <div class="mb-1 font-medium text-slate-800">{{ $t->name }}</div>
                            </div>
                        </button>
                        @endforeach
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>
