@props(['finance'])
<div style="background: #f6f6f6">
    <div class="pt-4 pb-8 mx-4 md:mx-auto md:max-w-7xl sm:py-16 sm:px-6 lg:px-8">
        <h2 class="mb-4 text-3xl font-extrabold tracking-tight text-center" data-aos="fade-down" data-aos-delay="450">
            {{ trans_choice('dictionary.last_report', 5, ['count' => 5]) }}
        </h2>

        <div class="overflow-hidden border-b border-gray-200 shadow">
            <table class="min-w-full divide-y divide-gray-200">
                <thead class="bg-blue-100" data-aos="fade-up" data-aos-delay="600">
                    <tr class="font-bold text-black">
                        <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                            Date
                        </th>
                        <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                            Detail
                        </th>
                        <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                            Value
                        </th>
                    </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                    @php $time = 600; @endphp
                    @forelse($finance as $d)
                    <tr class="@if($d->trashed()) text-gray-200 @endif" data-aos="fade-up"
                        data-aos-delay="{{$time+=150}}">
                        <td class="px-6 py-4">
                            {{ $d->date->format('d M Y') }}
                        </td>
                        <td class="px-6 py-4 break-words">
                            {{ $d->detail }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            {{ Helper::formatRupiah($d->value) }}
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td class="col-span-6 px-6 py-4 text-sm">
                            No data available
                        </td>
                        <td class="col-span-6 px-6 py-4 text-sm"></td>
                        <td class="col-span-6 px-6 py-4 text-sm"></td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
