@props(['project', 'charity', 'translation'])
<section class="relative">
    <!-- Background gradient (light version only) -->
    <div class="absolute bottom-0 left-0 right-0 pointer-events-none h-128 bg-gradient-to-t from-gray-100 to-white -z-10 dark:hidden"
        aria-hidden="true"></div>
    <!-- End background gradient (light version only) -->
    <div class="relative px-4 mx-auto max-w-7xl sm:px-6">
        <div class="pb-12 md:pb-20">
            <div class="grid grid-cols-1 gap-4 text-center lg:gap-6 md:grid-cols-3" data-aos-id-stats>
                <!-- 1st item -->
                <div class="px-1 py-8 bg-white shadow-2xl dark:bg-gray-800" data-aos="fade-down"
                    data-aos-anchor="[data-aos-id-stats]">
                    <div class="mb-1 text-3xl font-extrabold tracking-tighter font-red-hat-display">{{ $project }}</div>
                    <div class="text-gray-600 dark:text-gray-400">{{ __('Project') }}</div>
                </div>
                <!-- 2nd item -->
                <div class="px-1 py-8 bg-white shadow-2xl dark:bg-gray-800" data-aos="fade-down"
                    data-aos-anchor="[data-aos-id-stats]" data-aos-delay="100">
                    <div class="mb-1 text-3xl font-extrabold tracking-tighter font-red-hat-display">{{ $charity }}</div>
                    <div class="text-gray-600 dark:text-gray-400">{{ __("Charity") }}</div>
                </div>
                <!-- 3rd item -->
                <div class="px-1 py-8 bg-white shadow-2xl dark:bg-gray-800" data-aos="fade-down"
                    data-aos-anchor="[data-aos-id-stats]" data-aos-delay="200">
                    <div class="mb-1 text-3xl font-extrabold tracking-tighter font-red-hat-display">{{ $translation }}
                    </div>
                    <div class="text-gray-600 dark:text-gray-400">{{ __("Translator") }}</div>
                </div>
            </div>
        </div>
    </div>
</section>
