@props(['image', 'name', 'position', 'size'])
{{-- <div class="flex flex-col mb-8 sm:mb-16 mx-2 items-center {{$size}}">
    <div class="mb-4">
        <img src="{{ $image }}" alt="{{ $name }}" width="220" height="220">
    </div>
    <h5 class="mt-0 text-2xl text-blue-600">
        {{ $name }}
    </h5>
    <div class="text-xs team-item-role fw-500">
        {{ $position }}
    </div>
</div> --}}

<div class="mb-16 text-center" data-aos="fade-up" data-aos-delay="450">
    <div class="inline-flex mb-4">
        <img class="rounded-full" src="{{ $image }}" width="150" height="150" alt="{{ $name }}" />
    </div>
    <h4 class="mb-2 h4 font-playfair-display text-slate-800">{{ $name }}</h4>
    <div class="font-medium text-blue-600">{{ $position }}</div>
</div>
