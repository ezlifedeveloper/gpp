<section>
    <div class="max-w-6xl px-4 mx-auto sm:px-6">
        <div class="pt-10 pb-10 -mt-8 md:pb-16">

            <!-- Hero content -->
            <div class="items-center md:grid md:grid-cols-12 md:gap-12 lg:gap-10">
                <!-- Content -->
                <div class="mb-8 text-center md:col-span-6 lg:col-span-6 md:mb-0 md:text-left">
                    <h1 class="mb-4 text-4xl font-extrabold h1 lg:text-6xl font-red-hat-display" data-aos="fade-down">
                        <span class="block xl:inline">{{ __("dictionary.this_page") }}</span>
                        <span class="block text-indigo-600 xl:inline">{{ __("dictionary.under_development") }}</span>
                    </h1>
                    <p class="text-xl text-gray-600 dark:text-gray-400" data-aos="fade-down" data-aos-delay="150">
                        <i>{{ __("dictionary.thank_you_patience") }}</i>
                    </p>
                </div>

                <!-- Mobile mockup -->
                <div class="text-center md:col-span-6 lg:col-span-6 md:text-right" x-data="{ modalExpanded: false }"
                    data-aos="fade-up" data-aos-delay="450">
                    <div class="relative inline-flex items-center justify-center">
                        <!-- Image inside mockup size: 290x624px (or 580x1248px for Retina devices) -->
                        {{-- <img class="absolute" src="{{asset('mockup-image-01.jpeg')}}" width="290" height="600"
                            style="max-width: 84.33%;" alt="Features illustration" /> --}}
                        <!-- iPhone mockup -->
                        <img class="relative h-auto mx-auto pointer-events-none overflow-none max-w-screen md:mr-0 xl:max-w-none"
                            src="{{asset('logo.png')}}" width="600" alt="iPhone mockup" aria-hidden="true" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
