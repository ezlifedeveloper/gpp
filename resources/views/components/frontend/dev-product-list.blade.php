@props(['image', 'title', 'description', 'number', 'showAll' => true])
<div class="flex flex-col overflow-hidden rounded-lg shadow-lg h-full" data-aos="fade-up" data-aos-delay="{{300+(($number-1)*400)}}">
    <div class="flex-shrink-0">
        <img class="object-cover w-full" src="{{ $image }}" width="342" height="342" alt="{{$description}}">
    </div>
    <div class="flex flex-col justify-between flex-1 p-6 bg-white h-full">
        <div class="mt-2">
            <p class="text-xl font-semibold text-gray-900">
                {{ $title }}
            </p>
            @if($showAll)
            <p class="mt-3 text-base text-gray-500">
                {{ $description }}
            </p>
            @endif
        </div>
    </div>
</div>
