@props(['data', 'color'])
<!-- Hero Section -->
<div class="relative overflow-hidden">
    <div class="pb-2 mx-auto max-w-7xl" style="background: {{'#'.$color}}">
        <div class="relative pt-8 z-1 sm:pt-16 md:pt-20 lg:max-w lg:w-full lg:pt-28 xl:pt-32">
            <div class="px-4 pt-10 mb-5 max-w-7xl sm:pt-12 sm:px-6 md:pt-16 lg:pt-20 lg:px-8 xl:pt-28 lg:w-1/2">
                <div class="text-white text-bottom lg:text-left">
                    <h1 class="text-4xl font-extrabold tracking-tight sm:text-5xl md:text-6xl">
                        <span class="block xl:inline">{{ $data->name }}</span>
                    </h1>
                    <p class="mt-2 mb-3 text-base sm:mt-5 sm:text-lg sm:max-w-xl sm:mx-auto md:mt-3 md:text-xl lg:mx-0">
                        {{ $data->date->format('d F Y')}}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
