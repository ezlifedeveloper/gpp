@props(['data', 'finance'])
<!-- Translator section -->
<div class="bg-white">
    <div class="px-4 pt-4 pb-16 mx-auto sm:px-6 lg:max-w-7xl lg:px-8">
        <h2 class="mb-4 text-3xl font-extrabold tracking-tight">
            {{ __('Detail') }}
        </h2>
        <h4 class="max-w-4xl mb-8 text-xl tracking-tight text-gray-900">
            {{ $data->detail }}
        </h4>
        
        @if($data->report)
        <h2 class="mb-4 text-3xl font-extrabold tracking-tight">
            {{ __('Report') }}
        </h2>
        <h4 class="max-w-4xl mb-8 text-xl tracking-tight text-gray-900">
            {!! nl2br($data->report) !!}
        </h4>
        @endif

        <h2 class="mb-4 text-3xl font-extrabold tracking-tight">
            {{ __('dictionary.expenses') }}
        </h2>
        <h4 class="max-w-4xl mb-8 tracking-tight text-gray-900 text-md">
            <div class="overflow-hidden border-b border-gray-200 shadow">
                <table class="min-w-full divide-y divide-gray-200 table-auto">
                    <thead class="bg-blue-100">
                        <tr class="font-bold text-black">
                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                                Date
                            </th>
                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                                Detail
                            </th>
                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                                Value
                            </th>
                        </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                        @forelse($finance as $d)
                        <tr class="@if($d->trashed()) text-gray-200 @endif">
                            <td scope="col" class="px-2 break-words md:py-4">
                                {{ $d->date->format('d M Y') }}
                            </td>
                            <td scope="col" class="px-2 break-words md:py-4">
                                {{ $d->detail }}
                            </td>
                            <td scope="col" class="px-2 md:py-4 whitespace-nowrap">
                                {{ Helper::formatRupiah(abs($d->value)) }}
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td class="col-span-6 px-6 py-4 text-sm">
                                No data available
                            </td>
                            <td class="col-span-6 px-6 py-4 text-sm"></td>
                            <td class="col-span-6 px-6 py-4 text-sm"></td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </h4>

        <h2 class="mb-4 text-3xl font-extrabold tracking-tight">
            {{ __('dictionary.gallery') }}
        </h2>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#charity-light-slider").lightSlider({
                    item : 4,
                    slideMove:2,
                    easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
                    speed:600,
                    responsive : [{
                            breakpoint:1360,
                            settings: {
                                item:4,
                                slideMove:1,
                                slideMargin:6,
                            }
                        },
                        {
                            breakpoint:1120,
                            settings: {
                                item:3,
                                slideMove:1,
                                slideMargin:6,
                            }
                        },
                        {
                            breakpoint:800,
                            settings: {
                                item:2,
                                slideMove:1,
                                slideMargin:6,
                            }
                        },
                        {
                            breakpoint:640,
                            settings: {
                                item:1,
                                slideMove:1
                        }
                    }]
                });
            });
        </script>
        <ul id="charity-light-slider">
            @foreach ($data->photos as $d)
            <li>
                <div class="relative group">
                    <div
                        class="w-full overflow-hidden bg-gray-200 rounded-md min-h-80 aspect-w-1 aspect-h-1 group-hover:opacity-75 lg:h-80 lg:aspect-none">
                        <img src="{{ $d->photo }}" alt="{{ $data->name }}"
                            class="object-cover object-center w-full h-full lg:w-full lg:h-full">
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
</div>
