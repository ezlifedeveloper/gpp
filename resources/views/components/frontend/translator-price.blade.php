<div style="background: #f6f6f6">
    <div class="px-4 py-24 mx-auto max-w-7xl sm:px-6 lg:px-8">
        <h2 data-aos="fade-down"
            class="text-xl font-extrabold text-center text-gray-900 sm:text-2xl sm:leading-none sm:tracking-tight lg:text-3xl">
            {{ __("dictionary.pricing_plan") }}
        </h2>
        <h4 data-aos="fade-down"
            class="text-sm text-center text-gray-900 sm:text-md sm:leading-none sm:tracking-tight lg:text-lg">
            {{ __("dictionary.not_include_tax") }}
        </h4>

        <!-- Tiers -->
        <div class="mt-12 space-y-12 lg:space-y-0 lg:grid lg:grid-cols-4 lg:gap-x-8">
            <x-frontend.item-pricing :title="__('dictionary.translation_pricing_application_title')"
                :price="__('dictionary.translation_pricing_application_price')"
                :unit="__('dictionary.translation_pricing_application_unit')"
                :description="__('dictionary.translation_pricing_application_description')"
                :items="__('dictionary.translation_pricing_application_item')" :note="__('dictionary.regular')">
            </x-frontend.item-pricing>

            <x-frontend.item-pricing :title="__('dictionary.translation_pricing_document_normal_title')"
                :price="__('dictionary.translation_pricing_document_normal_price')"
                :unit="__('dictionary.translation_pricing_document_normal_unit')"
                :description="__('dictionary.translation_pricing_document_normal_description')"
                :items="__('dictionary.translation_pricing_document_normal_item')" :note="__('dictionary.regular')">
            </x-frontend.item-pricing>

            <x-frontend.item-pricing :title="__('dictionary.translation_pricing_document_express_title')"
                :price="__('dictionary.translation_pricing_document_express_price')"
                :unit="__('dictionary.translation_pricing_document_express_unit')"
                :description="__('dictionary.translation_pricing_document_express_description')"
                :items="__('dictionary.translation_pricing_document_express_item')" :note="__('dictionary.express')">
            </x-frontend.item-pricing>

            <x-frontend.item-pricing :title="__('dictionary.translation_pricing_proof_read_title')"
                :price="__('dictionary.translation_pricing_proof_read_price')"
                :unit="__('dictionary.translation_pricing_proof_read_unit')"
                :description="__('dictionary.translation_pricing_proof_read_description')"
                :items="__('dictionary.translation_pricing_proof_read_item')" :note="__('dictionary.regular')">
            </x-frontend.item-pricing>
        </div>
    </div>
</div>
