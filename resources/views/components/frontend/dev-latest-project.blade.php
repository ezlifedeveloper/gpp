@props(['portfolio'])
<section class="relative" style="background: white">
    <!-- Dark background -->
    <div class="absolute inset-0 bg-slate-900 pointer-events-none -z-10 [clip-path:polygon(0_0,_5760px_0,_5760px_calc(100%_-_352px),_0_100%)] h-96 md:h-auto md:mb-64"
        aria-hidden="true"></div>

    <div class="relative max-w-6xl px-4 mx-auto sm:px-6">
        <div class="py-12 md:py-20">

            <!-- Section header -->
            <div class="max-w-3xl pb-12 mx-auto text-center md:pb-20">
                <h2 class="text-3xl h2 font-playfair-display text-slate-100"
                    style="font-family: 'Playfair Display', serif;">{{
                    __('dictionary.dev_latest_project_description')}}</h2>
            </div>

            <!-- Section content -->
            <div class="grid items-start max-w-sm gap-12 mx-auto md:max-w-none md:grid-cols-3 lg:grid-cols-6 md:gap-x-5 md:gap-y-10">
                @foreach ($portfolio as $item)
                @if($loop->index < 4)
                <x-frontend.dev-project-list :image='$item->cover->file' :number='$loop->index' :title='$item->name'
                    :client='$item->client' :description='$item->detail' :slug='$item->slug'></x-frontend.dev-project-list>
                @else
                <x-frontend.dev-project-card-min :image='$item->cover->file' :number='$loop->index' :title='$item->name'
                    :description='$item->detail' :slug='$item->slug'></x-frontend.dev-project-card-min>
                @endif
                @endforeach
            </div>
        </div>
    </div>
</section>
