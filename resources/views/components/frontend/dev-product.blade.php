<section class="relative" style="background: #f6f6f6">
    <!-- Dark background -->
    <div class="absolute inset-0 bg-slate-900 pointer-events-none -z-10 [clip-path:polygon(0_0,_5760px_0,_5760px_calc(100%_-_352px),_0_100%)] h-96 md:h-auto md:mb-64"
        aria-hidden="true"></div>

    <div class="relative max-w-8xl px-4 mx-auto sm:px-6">
        <div class="py-12 md:py-20">

            <!-- Section header -->
            <div class="max-w-3xl pb-12 mx-auto text-center md:pb-20">
                <h2 class="text-3xl h2 font-playfair-display text-slate-100"
                    style="font-family: 'Playfair Display', serif;">{{
                    __('dictionary.dev_build_description')}}</h2>
            </div>

            <!-- Section content -->
            <div class="grid items-start max-w-sm gap-12 mx-auto md:max-w-none md:grid-cols-3 lg:grid-cols-5 md:gap-x-5 md:gap-y-10">
                <x-frontend.dev-product-list :image='asset("Katalog_Logo.jpg")' :number='"1"' :title='"Logo"'
                    :description='__("dictionary.dev_logo_description")'></x-frontend.dev-product-list>
                <x-frontend.dev-product-list :image='asset("Katalog_Web.jpg")' :number='"2"' :title='"Website"'
                    :description='__("dictionary.dev_web_description")'></x-frontend.dev-product-list>
                <x-frontend.dev-product-list :image='asset("Katalog_Mobile.jpg")' :number='"3"' :title='"Mobile App"'
                    :description='__("dictionary.dev_mobile_description")'></x-frontend.dev-product-list>
                <x-frontend.dev-product-list :image='asset("Katalog_AppsInt.jpg")' :number='"4"' :title='"Apps Integration"'
                    :description='__("dictionary.dev_apps_int_description")'></x-frontend.dev-product-list>
                <x-frontend.dev-product-list :image='asset("Katalog_ServerInfra.jpg")' :number='"5"' :title='"Server & Infra"'
                    :description='__("dictionary.dev_server_infra_description")'></x-frontend.dev-product-list>
            </div>
        </div>
    </div>
</section>
