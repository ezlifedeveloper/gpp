@props(['name', 'link'])
<a class="flex items-center justify-center text-white transition duration-150 ease-in-out bg-blue-500 rounded-full dark:text-blue-500 dark:bg-gray-800 hover:underline hover:bg-teal-600"
    href="{{ $link }}" aria-label="Twitter">
    <svg class="w-8 h-8 fill-current" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
        {{ $slot }}
    </svg>
</a>
