<section>
    <div class="max-w-6xl px-4 mx-auto sm:px-6">
        <div class="pt-10 pb-10 -mt-8">
            <!-- Hero content -->
            <div class="items-center">
                <!-- Content -->
                <div class="mt-8 text-center">
                    <h1 class="text-4xl font-extrabold h1 lg:text-6xl font-red-hat-display" data-aos="fade-down">
                        <span class="block xl:inline">{{ __("dictionary.we_are") }}</span>
                        <span class="block text-indigo-600 xl:inline">Grasia Prima Perfekta</span>
                    </h1>
                </div>

                <!-- Mobile mockup -->
                <div class="text-center" data-aos="fade-right">
                    <div class="relative inline-flex items-center justify-center -mt-4">
                        <!-- Image inside mockup size: 290x624px (or 580x1248px for Retina devices) -->
                        {{-- <img class="absolute" src="{{asset('mockup-image-01.jpeg')}}" width="290" height="600"
                            style="max-width: 84.33%;" alt="Features illustration" /> --}}
                        <!-- iPhone mockup -->
                        <img class="relative h-auto mx-auto pointer-events-none overflow-none max-w-screen md:mr-0 xl:max-w-none"
                            src="{{asset('logo.png')}}" width="600" alt="iPhone mockup" aria-hidden="true" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
