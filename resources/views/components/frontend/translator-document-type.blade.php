<div class="relative py-16 sm:py-24 lg:py-32" style="background: #f6f6f6">
    <div class="max-w-md px-4 mx-auto text-center sm:max-w-3xl sm:px-6 lg:px-8 lg:max-w-7xl">
        <h2 class="text-base font-semibold tracking-wider text-blue-600 uppercase" data-aos="fade-left">
            {{__('dictionary.translation_provide')}}</h2>
        <p class="mt-2 text-3xl font-extrabold tracking-tight text-gray-900 sm:text-4xl" data-aos="fade-right">
            {{__('dictionary.translation_provide_2')}}
        </p>
        {{-- <p class="mx-auto mt-5 text-xl text-gray-500 max-w-prose">
            Phasellus lorem quam molestie id quisque diam aenean nulla in. Accumsan in quis quis nunc, ullamcorper
            malesuada. Eleifend condimentum id viverra nulla.
        </p> --}}
        <div class="mt-12">
            <div class="grid grid-cols-1 gap-8 sm:grid-cols-2 lg:grid-cols-4">
                <x-frontend.item-image-type :title="__('dictionary.translation_website_title')"
                    :description="__('dictionary.translation_website_description')">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9" />
                </x-frontend.item-image-type>

                <x-frontend.item-image-type :title="__('dictionary.translation_mobile_title')"
                    :description="__('dictionary.translation_mobile_description')">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M12 18h.01M8 21h8a2 2 0 002-2V5a2 2 0 00-2-2H8a2 2 0 00-2 2v14a2 2 0 002 2z" />
                </x-frontend.item-image-type>

                <x-frontend.item-image-type :title="__('dictionary.translation_document_title')"
                    :description="__('dictionary.translation_document_description')">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                </x-frontend.item-image-type>

                <x-frontend.item-image-type :title="__('dictionary.translation_proof_read_title')"
                    :description="__('dictionary.translation_proof_read_description')">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                </x-frontend.item-image-type>
            </div>
        </div>
    </div>
</div>
