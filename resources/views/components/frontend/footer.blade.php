<footer class="relative pt-1 mx-auto bg-gray-100 footer">
    <div class="container px-6 mx-auto max-w-7xl">

        <div class="sm:flex sm:mt-8">
            <div class="flex flex-col justify-between mt-8 mb-8 sm:mt-0 sm:w-full sm:px-8 md:flex-row">
                <div class="flex flex-col">
                    <span class="mb-2 font-bold text-gray-700 uppercase">Grasia Prima Perfekta</span>
                    <span class="my-2">Phone: <a class="text-blue-500"
                            href="https://api.whatsapp.com/send?phone=6281221120660&text=Hello%2C%20GPP%21%20I%20found%20your%20contact%20from%20your%20website."
                            target="_blank">+62 812 2112 0660</a></span>
                    <span class="my-2">Email: <a class="text-blue-500"
                            href="mailto:grasiaprimaperfekta@gmail.com">grasiaprimaperfekta@gmail.com</a></span>
                </div>
            </div>

            <!-- Social links -->
            <ul class="flex mb-4 md:order-2 md:ml-4 md:mb-0">
                {{-- <li>
                    <x-frontend.social-icon :name="'twitter'" :link="'google.com'">
                        <path
                            d="M24 11.5c-.6.3-1.2.4-1.9.5.7-.4 1.2-1 1.4-1.8-.6.4-1.3.6-2.1.8-.6-.6-1.5-1-2.4-1-1.7 0-3.2 1.5-3.2 3.3 0 .3 0 .5.1.7-2.7-.1-5.2-1.4-6.8-3.4-.3.5-.4 1-.4 1.7 0 1.1.6 2.1 1.5 2.7-.5 0-1-.2-1.5-.4 0 1.6 1.1 2.9 2.6 3.2-.3.1-.6.1-.9.1-.2 0-.4 0-.6-.1.4 1.3 1.6 2.3 3.1 2.3-1.1.9-2.5 1.4-4.1 1.4H8c1.5.9 3.2 1.5 5 1.5 6 0 9.3-5 9.3-9.3v-.4c.7-.5 1.3-1.1 1.7-1.8z" />
                    </x-frontend.social-icon>
                </li>
                <li class="ml-4">
                    <x-frontend.social-icon :name="'github.com'" :link="'github.com'">
                        <path
                            d="M16 8.2c-4.4 0-8 3.6-8 8 0 3.5 2.3 6.5 5.5 7.6.4.1.5-.2.5-.4V22c-2.2.5-2.7-1-2.7-1-.4-.9-.9-1.2-.9-1.2-.7-.5.1-.5.1-.5.8.1 1.2.8 1.2.8.7 1.3 1.9.9 2.3.7.1-.5.3-.9.5-1.1-1.8-.2-3.6-.9-3.6-4 0-.9.3-1.6.8-2.1-.1-.2-.4-1 .1-2.1 0 0 .7-.2 2.2.8.6-.2 1.3-.3 2-.3s1.4.1 2 .3c1.5-1 2.2-.8 2.2-.8.4 1.1.2 1.9.1 2.1.5.6.8 1.3.8 2.1 0 3.1-1.9 3.7-3.7 3.9.3.4.6.9.6 1.6v2.2c0 .2.1.5.6.4 3.2-1.1 5.5-4.1 5.5-7.6-.1-4.4-3.7-8-8.1-8z" />
                    </x-frontend.social-icon>
                </li> --}}
                <li class="ml-4">
                    <x-frontend.social-icon :name="'Facebook'" :link="'https://www.facebook.com/grasiapp.id'">
                        <path
                            d="M14.023 24L14 17h-3v-3h3v-2c0-2.7 1.672-4 4.08-4 1.153 0 2.144.086 2.433.124v2.821h-1.67c-1.31 0-1.563.623-1.563 1.536V14H21l-1 3h-2.72v7h-3.257z" />
                    </x-frontend.social-icon>
                </li>
                <li class="ml-4">
                    <x-frontend.social-icon :name="'Instagram'"
                        :link="'https://www.instagram.com/grasiapp.id/'">
                        <circle cx="20.145" cy="11.892" r="1" />
                        <path
                            d="M16 20c-2.206 0-4-1.794-4-4s1.794-4 4-4 4 1.794 4 4-1.794 4-4 4zm0-6c-1.103 0-2 .897-2 2s.897 2 2 2 2-.897 2-2-.897-2-2-2z" />
                        <path
                            d="M20 24h-8c-2.056 0-4-1.944-4-4v-8c0-2.056 1.944-4 4-4h8c2.056 0 4 1.944 4 4v8c0 2.056-1.944 4-4 4zm-8-14c-.935 0-2 1.065-2 2v8c0 .953 1.047 2 2 2h8c.935 0 2-1.065 2-2v-8c0-.935-1.065-2-2-2h-8z" />
                    </x-frontend.social-icon>
                </li>
                <li class="ml-4">
                    <x-frontend.social-icon :name="'LinkedIn'"
                        :link="'https://www.linkedin.com/company/grasiaprimaperfekta/'">
                        <path
                            d="M23.3 8H8.7c-.4 0-.7.3-.7.7v14.7c0 .3.3.6.7.6h14.7c.4 0 .7-.3.7-.7V8.7c-.1-.4-.4-.7-.8-.7zM12.7 21.6h-2.3V14h2.4v7.6h-.1zM11.6 13c-.8 0-1.4-.7-1.4-1.4 0-.8.6-1.4 1.4-1.4.8 0 1.4.6 1.4 1.4-.1.7-.7 1.4-1.4 1.4zm10 8.6h-2.4v-3.7c0-.9 0-2-1.2-2s-1.4 1-1.4 2v3.8h-2.4V14h2.3v1c.3-.6 1.1-1.2 2.2-1.2 2.4 0 2.8 1.6 2.8 3.6v4.2h.1z" />
                    </x-frontend.social-icon>
                </li>
            </ul>
        </div>
    </div>

    <div class="container px-6 mx-auto">
        <div class="flex flex-col items-center mt-16 border-t-2 border-gray-300">
            <div class="py-6 text-center sm:w-2/3">
                <p class="mb-2 text-sm font-bold text-blue-700">
                    Grasia Prima Perfekta © 2022 - Built with <a href="https://tallstack.dev/" target="_blank"> TALL
                        Stack</a>
                </p>
            </div>
        </div>
    </div>
</footer>
