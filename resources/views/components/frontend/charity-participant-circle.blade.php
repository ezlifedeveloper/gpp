@props(['data', 'size'])
<section style="background: #f6f6f6">
    <div class="max-w-3xl px-4 py-12 mx-auto sm:px-6">
        <div class="relative" x-data="{ tooltip: false }">
            <!-- Background image -->
            <svg class="mx-auto" viewBox="0 0 678 346" xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <radialGradient cx="50%" cy="50%" fx="50%" fy="50%" r="39.386%" id="circle_b">
                        <stop stop-color="#3ABAB4" offset="0%" />
                        <stop stop-color="#3ABAB4" stop-opacity="0" offset="100%" />
                    </radialGradient>
                    <linearGradient x1="50%" y1="50%" x2="50%" y2="89.386%" id="circle_a">
                        <stop stop-color="#2E2E33" offset="0%" />
                        <stop stop-color="#2E2E33" stop-opacity="0" offset="100%" />
                    </linearGradient>
                </defs>
                <g fill="none" fill-rule="evenodd">
                    <circle class="opacity-10 dark:opacity-100" fill="url(#circle_a)" opacity=".32" cx="339" cy="173"
                        r="173" />
                    <circle fill="url(#circle_b)" opacity=".32" cx="339" cy="173" r="140" />
                </g>
            </svg>

            <!-- People pics -->
            @php $time = 250; @endphp
            @foreach ($data as $d)
            <a href="{{ route('link', $d->username) }}">
                @if($d->image)
                <img src="{{ $d->image ?? 'https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80' }}"
                    alt="{{ $d->name }}" style="{{ $d->style }} width:{{$d->size}}px; height:{{$d->size}}px"
                    class="absolute z-10 object-cover rounded-full animate-float" data-aos="fade-up"
                    data-aos-delay="{{$time+=150}}" x-on:mouseover="tooltip = true" x-on:mouseleave="tooltip = false">
                @else
                <span x-on:mouseover="tooltip=true" x-on:mouseleave="tooltip = false"
                    class="absolute z-10 inline-flex items-center justify-center w-10 h-10 bg-blue-500 rounded-full animate-float"
                    style="{{ $d->style }}" data-aos="fade-up" data-aos-delay="{{$time+=150}}">
                    <span class="text-xl font-medium text-white">{{
                        Helper::getNameInitial($d->name)
                        }}</span>
                </span>
                @endif
            </a>
            @endforeach
        </div>
    </div>
</section>
