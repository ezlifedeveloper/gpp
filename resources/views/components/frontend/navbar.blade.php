@props(['title'])
<header>
    <div x-data="{ open: false, show: false }" @click.away="show=false, open=false" class="relative bg-white">
        <div
            class="flex items-center justify-between px-4 py-6 mx-auto max-w-7xl sm:px-6 md:justify-start md:space-x-10 lg:px-8">
            <div class="flex justify-start lg:w-0 lg:flex-1">
                <a href="{{ route('home') }}">
                    <span class="sr-only">{{ env('APP_NAME') }}</span>
                    <img class="w-auto h-8 sm:h-10" src="{{ asset('logo.png') }}" alt="{{ env('APP_NAME') }}">
                </a>
            </div>
            <div class="-my-2 -mr-2 md:hidden">
                <div class="inline-flex items-center justify-center">
                    <x-frontend.language-select :view="'mobile'"></x-frontend.language-select>
                    <button @click="show = true" type="button"
                        class="p-2 text-gray-400 bg-white rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500"
                        aria-expanded="false">
                        <span class="sr-only">Open menu</span>
                        <!-- Heroicon name: outline/menu -->
                        <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M4 6h16M4 12h16M4 18h16" />
                        </svg>
                    </button>
                </div>
            </div>
            <nav class="hidden space-x-10 md:flex">
                <a href="{{ route('softwaredev') }}"
                    class="@if($title && $title=='IT Solutions') border-indigo-500 @else border-transparent @endif border-b-2 text-base font-medium text-gray-500 hover:text-gray-900">
                    {{ __('IT Solutions') }}
                </a>

                <a href="{{ route('translator') }}"
                    class="@if($title && $title=='Translator') border-indigo-500 @else border-transparent @endif border-b-2 text-base font-medium text-gray-500 hover:text-gray-900">
                    {{ __('Translator') }}
                </a>

                <a href="{{ route('charity') }}"
                    class="@if($title && $title=='Charity') border-indigo-500 @else border-transparent @endif border-b-2 text-base font-medium text-gray-500 hover:text-gray-900">
                    {{ __('Charity') }}
                </a>

                <a href="{{ route('about') }}"
                    class="@if($title && $title=='About Us') border-indigo-500 @else border-transparent @endif border-b-2 text-base font-medium text-gray-500 hover:text-gray-900">
                    {{ __('About Us') }}
                </a>
            </nav>
            <div class="items-center justify-end hidden md:flex md:flex-1 lg:w-0">
                <x-frontend.language-select :view="'web'"></x-frontend.language-select>
                <a href="{{ route('login') }}"
                    class="inline-flex items-center justify-center px-4 py-2 ml-8 text-base font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm whitespace-nowrap hover:bg-indigo-700">
                    @if(Auth::check()) {{ __('Profile') }} @else {{ __('Sign In') }} @endif
                </a>
            </div>
        </div>

        <div x-show="show" x-transition:enter="duration-200 ease-out" x-transition:enter-start="opacity-0 scale-95"
            x-transition:enter-end="opacity-100 scale-100" x-transition:leave="duration-100 ease-in"
            x-transition:leave-start="opacity-100 scale-100" x-transition:leave-end="opacity-0 scale-95"
            x-description="Mobile menu, show/hide based on mobile menu state."
            class="absolute inset-x-0 top-0 z-30 p-2 transition origin-top-right transform md:hidden" x-ref="panel"
            @click.away="show = false" style="display: none;">
            <div class="bg-white divide-y-2 rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 divide-gray-50">
                <div class="px-5 pt-5 pb-6">
                    <div class="flex items-center justify-between">
                        <div>
                            <a href="{{ route('home') }}">
                                <img class="w-auto h-8" src="{{ asset('logo.png') }}" alt="{{ env('APP_NAME') }}">
                            </a>
                        </div>
                        <div class="-mr-2">
                            <button @click="show = false " type="button"
                                class="inline-flex items-center justify-center p-2 text-gray-400 bg-white rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                                <span class="sr-only">Close menu</span>
                                <!-- Heroicon name: outline/x -->
                                <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                    stroke="currentColor" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div class="mt-6">
                        <nav class="grid grid-cols-1 gap-7">
                            <a href="{{ route('softwaredev') }}"
                                class="@if($title && $title=='IT Solutions') bg-indigo-50 border-indigo-500 @else border-transparent @endif border-l-4 flex items-center p-3 -m-3 rounded-lg hover:bg-gray-50">
                                <div
                                    class="flex items-center justify-center flex-shrink-0 w-10 h-10 text-white bg-indigo-600 rounded-md">
                                    <!-- Heroicon name: outline/inbox -->
                                    <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M20 13V6a2 2 0 00-2-2H6a2 2 0 00-2 2v7m16 0v5a2 2 0 01-2 2H6a2 2 0 01-2-2v-5m16 0h-2.586a1 1 0 00-.707.293l-2.414 2.414a1 1 0 01-.707.293h-3.172a1 1 0 01-.707-.293l-2.414-2.414A1 1 0 006.586 13H4" />
                                    </svg>
                                </div>
                                <div class="ml-4 text-base font-medium text-gray-900">
                                    {{ __('IT Solutions') }}
                                </div>
                            </a>

                            <a href="{{ route('translator') }}"
                                class="@if($title && $title=='Translator') bg-indigo-50 border-indigo-500 @else border-transparent @endif border-l-4 flex items-center p-3 -m-3 rounded-lg hover:bg-gray-50">
                                <div
                                    class="flex items-center justify-center flex-shrink-0 w-10 h-10 text-white bg-indigo-600 rounded-md">
                                    <!-- Heroicon name: outline/annotation -->
                                    <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z" />
                                    </svg>
                                </div>
                                <div class="ml-4 text-base font-medium text-gray-900">
                                    {{ __('Translator') }}
                                </div>
                            </a>

                            <a href="{{ route('charity') }}"
                                class="@if($title && $title=='Charity') bg-indigo-50 border-indigo-500 @else border-transparent @endif border-l-4 flex items-center p-3 -m-3 rounded-lg hover:bg-gray-50">
                                <div
                                    class="flex items-center justify-center flex-shrink-0 w-10 h-10 text-white bg-indigo-600 rounded-md">
                                    <!-- Heroicon name: outline/chat-alt-2 -->
                                    <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z" />
                                    </svg>
                                </div>
                                <div class="ml-4 text-base font-medium text-gray-900">
                                    {{ __('Charity') }}
                                </div>
                            </a>

                            <a href="{{ route('about') }}"
                                class="@if($title && $title=='About Us') bg-indigo-50 border-indigo-500 @else border-transparent @endif border-l-4 flex items-center p-3 -m-3 rounded-lg hover:bg-gray-50">
                                <div
                                    class="flex items-center justify-center flex-shrink-0 w-10 h-10 text-white bg-indigo-600 rounded-md">
                                    <!-- Heroicon name: outline/question-mark-circle -->
                                    <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M8.228 9c.549-1.165 2.03-2 3.772-2 2.21 0 4 1.343 4 3 0 1.4-1.278 2.575-3.006 2.907-.542.104-.994.54-.994 1.093m0 3h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                    </svg>
                                </div>
                                <div class="ml-4 text-base font-medium text-gray-900">
                                    {{ __('About Us') }}
                                </div>
                            </a>
                        </nav>
                    </div>
                </div>
                <div class="px-5 py-6">
                    <div class="mt-6">
                        <a href="{{ route('login') }}"
                            class="flex items-center justify-center w-full px-4 py-2 text-base font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700">
                            @if(Auth::check()) {{ __('Profile') }} @else {{ __('Sign In') }} @endif
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
