@props(['data'])
<!-- Translator section -->
<div class="bg-gray-50">
    <div class="max-w-2xl px-4 py-16 mx-auto sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8">
        <h2 class="mb-4 text-3xl font-extrabold tracking-tight text-center" data-aos="fade-right" data-aos-delay="450">
            {{ __('Translator') }}
        </h2>
        <h4 class="max-w-5xl mx-auto mb-8 text-xl tracking-tight text-center text-gray-900" data-aos="fade-left"
            data-aos-delay="450">
            {{ __('dictionary.translation_description') }}
        </h4>

        <div
            class="grid content-center justify-center w-full grid-cols-2 mt-6 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 gap-y-10 gap-x-6 xl:gap-x-8">

            @php $time = 300; @endphp
            @foreach ($data as $d)
            <div class="relative group" data-aos="fade-left" data-aos-delay="{{ $time+=150 }}">
                <div
                    class="w-full h-24 overflow-hidden rounded-md aspect-w-1 aspect-h-1 group-hover:opacity-75 lg:aspect-none">
                    <img src="{{ asset('flag/'.$d->code.'.png') }}" alt="{{ $d->name }}"
                        class="object-contain object-center w-full h-full">
                </div>
                <div class="content-center justify-between w-full mt-4">
                    <h3 class="text-sm text-center text-gray-700">
                        <span aria-hidden="true" class="absolute inset-0"></span>
                        {{ $d->name }}
                    </h3>
                </div>
            </div>
            @endforeach
        </div>

        <div class="max-w-xl mx-auto mt-10 mb-8 text-xl tracking-tight text-center text-gray-900" data-aos="fade-up"
            data-aos-delay="450">
            <x-frontend.button-big :title="'See More Info'" :link="route('translator')" :color="'gray'" :newtab="false">
            </x-frontend.button-big>
        </div>
    </div>
</div>
