@props(['link', 'title', 'color', 'newtab'])
<div class="rounded-md shadow">
    <a href="{{$link}}" @if($newtab) target="_blank" @endif
        class="flex items-center justify-center w-full px-8 py-3 text-base font-medium text-white bg-{{$color}}-500 border border-transparent rounded-md hover:bg-{{$color}}-600 md:py-4 md:text-lg md:px-10">
        {{ $slot }}
        {{ __($title) }}
    </a>
</div>
