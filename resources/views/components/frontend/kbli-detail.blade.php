<section>
    <div class="max-w-6xl px-4 mx-auto sm:px-6">
        <div class="py-12 md:py-20 border-slate-200">

            <!-- Section content -->
            <div class="flex flex-col max-w-xl mx-auto space-y-8 space-y-reverse md:max-w-none md:flex-row md:items-start md:space-x-8 lg:space-x-16 xl:space-x-18 md:space-y-0"
                x-data="{ tab: '1' }">

                <!-- Tabs items (images) -->
                <div class="order-1 md:rtl md:w-5/12 lg:w-1/2 md:order-none" data-aos="fade-down">
                    <div class="relative flex flex-col">
                        <!-- Item 1 -->
                        <div class="w-full" x-show="tab === '1'"
                            x-transition:enter="transition ease-in-out duration-700 transform order-first"
                            x-transition:enter-start="opacity-0 -translate-y-16"
                            x-transition:enter-end="opacity-100 translate-y-0"
                            x-transition:leave="transition ease-in-out duration-300 transform absolute"
                            x-transition:leave-start="opacity-100 translate-y-0"
                            x-transition:leave-end="opacity-0 translate-y-16">
                            <img class="mx-auto rounded md:max-w-none" src="{{ asset('logo_oss.png') }}" width="540"
                                height="620" alt="Features home 2 01" />
                        </div>
                        <!-- Item 2 -->
                        <div class="w-full" x-show="tab === '2'"
                            x-transition:enter="transition ease-in-out duration-700 transform order-first"
                            x-transition:enter-start="opacity-0 -translate-y-16"
                            x-transition:enter-end="opacity-100 translate-y-0"
                            x-transition:leave="transition ease-in-out duration-300 transform absolute"
                            x-transition:leave-start="opacity-100 translate-y-0"
                            x-transition:leave-end="opacity-0 translate-y-16">
                            <img class="mx-auto rounded md:max-w-none" src="{{ asset('logo_oss.png') }}" width="540"
                                height="620" alt="Features home 2 02" />
                        </div>
                        <!-- Item 3 -->
                        <div class="w-full" x-show="tab === '3'"
                            x-transition:enter="transition ease-in-out duration-700 transform order-first"
                            x-transition:enter-start="opacity-0 -translate-y-16"
                            x-transition:enter-end="opacity-100 translate-y-0"
                            x-transition:leave="transition ease-in-out duration-300 transform absolute"
                            x-transition:leave-start="opacity-100 translate-y-0"
                            x-transition:leave-end="opacity-0 translate-y-16">
                            <img class="mx-auto rounded md:max-w-none" src="{{ asset('logo_oss.png') }}" width="540"
                                height="620" alt="Features home 2 03" />
                        </div>
                    </div>
                </div>

                <!-- Content -->
                <div class="md:w-7/12 lg:w-1/2" data-aos="fade-up">
                    <div class="mb-8 text-center md:text-left">
                        <h3 class="mb-3 text-xl font-bold h3 text-slate-800">{{
                            __('dictionary.dev_registered_oss')
                            }}</h3>
                    </div>
                    <!-- Tabs buttons -->
                    <div class="mb-8 md:mb-0">
                        <button data-aos="fade-left"
                            :class="tab !== '1' ? 'border-transparent opacity-50 hover:opacity-75' : 'border-2 border-blue-500 opacity-100'"
                            class="flex items-start w-full px-5 py-3 mb-3 text-left transition duration-300 ease-in-out bg-white border-2 rounded shadow-md"
                            @click="tab = '1'">
                            <svg class="w-4 h-4 mt-1 mr-4 text-blue-600 fill-current shrink-0" viewBox="0 0 16 16"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M15.686 5.71 10.291.3c-.4-.4-.999-.4-1.399 0a.97.97 0 0 0 0 1.403l.6.6L2.698 6.01l-1-1.002c-.4-.4-.999-.4-1.398 0a.97.97 0 0 0 0 1.403l1.498 1.502 2.398 2.404L.6 14.023 2 15.425l3.696-3.706 3.997 4.007c.5.5 1.199.2 1.398 0a.97.97 0 0 0 0-1.402l-.999-1.002 3.697-6.711.6.6c.599.602 1.199.201 1.398 0 .3-.4.3-1.1-.1-1.502Zm-7.193 6.11L4.196 7.511l6.695-3.706 1.298 1.302-3.696 6.711Z" />
                            </svg>
                            <div>
                                <div class="mb-1 font-medium text-slate-800">47413</div>
                                <div class="text-slate-500">{{ __('dictionary.kbli_47413') }}</div>
                            </div>
                        </button>
                        <button data-aos="fade-left"
                            :class="tab !== '1' ? 'border-transparent opacity-50 hover:opacity-75' : 'border-2 border-blue-500 opacity-100'"
                            class="flex items-start w-full px-5 py-3 mb-3 text-left transition duration-300 ease-in-out bg-white border-2 rounded shadow-md"
                            @click="tab = '1'">
                            <svg class="w-4 h-4 mt-1 mr-4 text-blue-600 fill-current shrink-0" viewBox="0 0 16 16"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M15.686 5.71 10.291.3c-.4-.4-.999-.4-1.399 0a.97.97 0 0 0 0 1.403l.6.6L2.698 6.01l-1-1.002c-.4-.4-.999-.4-1.398 0a.97.97 0 0 0 0 1.403l1.498 1.502 2.398 2.404L.6 14.023 2 15.425l3.696-3.706 3.997 4.007c.5.5 1.199.2 1.398 0a.97.97 0 0 0 0-1.402l-.999-1.002 3.697-6.711.6.6c.599.602 1.199.201 1.398 0 .3-.4.3-1.1-.1-1.502Zm-7.193 6.11L4.196 7.511l6.695-3.706 1.298 1.302-3.696 6.711Z" />
                            </svg>
                            <div>
                                <div class="mb-1 font-medium text-slate-800">58200 </div>
                                <div class="text-slate-500">{{ __('dictionary.kbli_58200') }}</div>
                            </div>
                        </button>
                        <button data-aos="fade-left"
                            :class="tab !== '2' ? 'border-transparent opacity-50 hover:opacity-75' : 'border-2 border-blue-500 opacity-100'"
                            class="flex items-start w-full px-5 py-3 mb-3 text-left transition duration-300 ease-in-out bg-white border-2 rounded shadow-md"
                            @click="tab = '2'">
                            <svg class="w-4 h-4 mt-1 mr-4 text-blue-600 fill-current shrink-0" viewBox="0 0 16 16"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M15.686 5.71 10.291.3c-.4-.4-.999-.4-1.399 0a.97.97 0 0 0 0 1.403l.6.6L2.698 6.01l-1-1.002c-.4-.4-.999-.4-1.398 0a.97.97 0 0 0 0 1.403l1.498 1.502 2.398 2.404L.6 14.023 2 15.425l3.696-3.706 3.997 4.007c.5.5 1.199.2 1.398 0a.97.97 0 0 0 0-1.402l-.999-1.002 3.697-6.711.6.6c.599.602 1.199.201 1.398 0 .3-.4.3-1.1-.1-1.502Zm-7.193 6.11L4.196 7.511l6.695-3.706 1.298 1.302-3.696 6.711Z" />
                            </svg>
                            <div>
                                <div class="mb-1 font-medium text-slate-800">62029</div>
                                <div class="text-slate-500">{{ __('dictionary.kbli_62029') }}</div>
                            </div>
                        </button>
                        <button data-aos="fade-left"
                            :class="tab !== '2' ? 'border-transparent opacity-50 hover:opacity-75' : 'border-2 border-blue-500 opacity-100'"
                            class="flex items-start w-full px-5 py-3 mb-3 text-left transition duration-300 ease-in-out bg-white border-2 rounded shadow-md"
                            @click="tab = '2'">
                            <svg class="w-4 h-4 mt-1 mr-4 text-blue-600 fill-current shrink-0" viewBox="0 0 16 16"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M15.686 5.71 10.291.3c-.4-.4-.999-.4-1.399 0a.97.97 0 0 0 0 1.403l.6.6L2.698 6.01l-1-1.002c-.4-.4-.999-.4-1.398 0a.97.97 0 0 0 0 1.403l1.498 1.502 2.398 2.404L.6 14.023 2 15.425l3.696-3.706 3.997 4.007c.5.5 1.199.2 1.398 0a.97.97 0 0 0 0-1.402l-.999-1.002 3.697-6.711.6.6c.599.602 1.199.201 1.398 0 .3-.4.3-1.1-.1-1.502Zm-7.193 6.11L4.196 7.511l6.695-3.706 1.298 1.302-3.696 6.711Z" />
                            </svg>
                            <div>
                                <div class="mb-1 font-medium text-slate-800">62090</div>
                                <div class="text-slate-500">{{ __('dictionary.kbli_62090') }}</div>
                            </div>
                        </button>
                        <button data-aos="fade-left"
                            :class="tab !== '2' ? 'border-transparent opacity-50 hover:opacity-75' : 'border-2 border-blue-500 opacity-100'"
                            class="flex items-start w-full px-5 py-3 mb-3 text-left transition duration-300 ease-in-out bg-white border-2 rounded shadow-md"
                            @click="tab = '2'">
                            <svg class="w-4 h-4 mt-1 mr-4 text-blue-600 fill-current shrink-0" viewBox="0 0 16 16"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M15.686 5.71 10.291.3c-.4-.4-.999-.4-1.399 0a.97.97 0 0 0 0 1.403l.6.6L2.698 6.01l-1-1.002c-.4-.4-.999-.4-1.398 0a.97.97 0 0 0 0 1.403l1.498 1.502 2.398 2.404L.6 14.023 2 15.425l3.696-3.706 3.997 4.007c.5.5 1.199.2 1.398 0a.97.97 0 0 0 0-1.402l-.999-1.002 3.697-6.711.6.6c.599.602 1.199.201 1.398 0 .3-.4.3-1.1-.1-1.502Zm-7.193 6.11L4.196 7.511l6.695-3.706 1.298 1.302-3.696 6.711Z" />
                            </svg>
                            <div>
                                <div class="mb-1 font-medium text-slate-800">62019</div>
                                <div class="text-slate-500">{{ __('dictionary.kbli_62019') }}</div>
                            </div>
                        </button>
                        <button data-aos="fade-left"
                            :class="tab !== '3' ? 'border-transparent opacity-50 hover:opacity-75' : 'border-2 border-blue-500 opacity-100'"
                            class="flex items-start w-full px-5 py-3 mb-3 text-left transition duration-300 ease-in-out bg-white border-2 rounded shadow-md"
                            @click="tab = '3'">
                            <svg class="w-4 h-4 mt-1 mr-4 text-blue-600 fill-current shrink-0" viewBox="0 0 16 16"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M15.686 5.71 10.291.3c-.4-.4-.999-.4-1.399 0a.97.97 0 0 0 0 1.403l.6.6L2.698 6.01l-1-1.002c-.4-.4-.999-.4-1.398 0a.97.97 0 0 0 0 1.403l1.498 1.502 2.398 2.404L.6 14.023 2 15.425l3.696-3.706 3.997 4.007c.5.5 1.199.2 1.398 0a.97.97 0 0 0 0-1.402l-.999-1.002 3.697-6.711.6.6c.599.602 1.199.201 1.398 0 .3-.4.3-1.1-.1-1.502Zm-7.193 6.11L4.196 7.511l6.695-3.706 1.298 1.302-3.696 6.711Z" />
                            </svg>
                            <div>
                                <div class="mb-1 font-medium text-slate-800">74901</div>
                                <div class="text-slate-500"> {{ __('dictionary.kbli_74901') }}</div>
                            </div>
                        </button>
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>
