@props(['translator', 'qty', 'price', 'days', 'messageUrl', 'inputOrderType'])
<section id="calculator">
    <div class="px-4 py-24 mx-auto max-w-7xl sm:px-6 lg:px-8">
        <div class="relative bg-white shadow-xl">
            <div class="grid grid-cols-1 lg:grid-cols-4">
                <!-- Contact information -->
                <div
                    class="relative px-6 py-10 overflow-hidden text-white bg-gradient-to-b from-blue-500 to-blue-600 sm:px-10 xl:p-12">
                    <!-- Decorative angle backgrounds -->
                    <div class="absolute inset-0 pointer-events-none sm:hidden" aria-hidden="true">
                        <svg class="absolute inset-0 w-full h-full" width="343" height="388" viewBox="0 0 343 388"
                            fill="none" preserveAspectRatio="xMidYMid slice" xmlns="http://www.w3.org/2000/svg">
                            <path d="M-99 461.107L608.107-246l707.103 707.107-707.103 707.103L-99 461.107z"
                                fill="url(#linear1)" fill-opacity=".1"></path>
                            <defs>
                                <linearGradient id="linear1" x1="254.553" y1="107.554" x2="961.66" y2="814.66"
                                    gradientUnits="userSpaceOnUse">
                                    <stop stop-color="#fff"></stop>
                                    <stop offset="1" stop-color="#fff" stop-opacity="0"></stop>
                                </linearGradient>
                            </defs>
                        </svg>
                    </div>
                    <div class="absolute top-0 bottom-0 right-0 hidden w-1/2 pointer-events-none sm:block lg:hidden"
                        aria-hidden="true">
                        <svg class="absolute inset-0 w-full h-full" width="359" height="339" viewBox="0 0 359 339"
                            fill="none" preserveAspectRatio="xMidYMid slice" xmlns="http://www.w3.org/2000/svg">
                            <path d="M-161 382.107L546.107-325l707.103 707.107-707.103 707.103L-161 382.107z"
                                fill="url(#linear2)" fill-opacity=".1"></path>
                            <defs>
                                <linearGradient id="linear2" x1="192.553" y1="28.553" x2="899.66" y2="735.66"
                                    gradientUnits="userSpaceOnUse">
                                    <stop stop-color="#fff"></stop>
                                    <stop offset="1" stop-color="#fff" stop-opacity="0"></stop>
                                </linearGradient>
                            </defs>
                        </svg>
                    </div>
                    <div class="absolute top-0 bottom-0 right-0 hidden w-1/2 pointer-events-none lg:block"
                        aria-hidden="true">
                        <svg class="absolute inset-0 w-full h-full" width="160" height="678" viewBox="0 0 160 678"
                            fill="none" preserveAspectRatio="xMidYMid slice" xmlns="http://www.w3.org/2000/svg">
                            <path d="M-161 679.107L546.107-28l707.103 707.107-707.103 707.103L-161 679.107z"
                                fill="url(#linear3)" fill-opacity=".1"></path>
                            <defs>
                                <linearGradient id="linear3" x1="192.553" y1="325.553" x2="899.66" y2="1032.66"
                                    gradientUnits="userSpaceOnUse">
                                    <stop stop-color="#fff"></stop>
                                    <stop offset="1" stop-color="#fff" stop-opacity="0"></stop>
                                </linearGradient>
                            </defs>
                        </svg>
                    </div>
                    <h3 class="text-xl text-center text-white">
                        {{ __('dictionary.pricing_calculator') }}
                    </h3>
                    <h3 class="text-xl text-center text-white">
                        {{ __('dictionary.estimation') }}
                    </h3>

                    <h3 class="mt-8 text-3xl font-extrabold text-center text-white">{{ $price }}</h3>
                    <h3 class="mt-2 mb-4 text-xl text-center text-white">
                        {{ $days }}
                    </h3>
                    <div class="rounded-md shadow">
                        <a href="{{$messageUrl}}" target="_blank"
                            class="flex items-center justify-center w-full px-4 py-3 text-base font-medium text-green-500 bg-white border border-transparent rounded-md hover:bg-gray-200 md:py-4 md:text-lg md:px-6">

                            <svg class="mr-2" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="36" height="36"
                                viewBox="0 0 48 48" style=" fill:#000000;">
                                <path fill="#fff"
                                    d="M4.9,43.3l2.7-9.8C5.9,30.6,5,27.3,5,24C5,13.5,13.5,5,24,5c5.1,0,9.8,2,13.4,5.6	C41,14.2,43,18.9,43,24c0,10.5-8.5,19-19,19c0,0,0,0,0,0h0c-3.2,0-6.3-0.8-9.1-2.3L4.9,43.3z">
                                </path>
                                <path fill="#fff"
                                    d="M4.9,43.8c-0.1,0-0.3-0.1-0.4-0.1c-0.1-0.1-0.2-0.3-0.1-0.5L7,33.5c-1.6-2.9-2.5-6.2-2.5-9.6	C4.5,13.2,13.3,4.5,24,4.5c5.2,0,10.1,2,13.8,5.7c3.7,3.7,5.7,8.6,5.7,13.8c0,10.7-8.7,19.5-19.5,19.5c-3.2,0-6.3-0.8-9.1-2.3	L5,43.8C5,43.8,4.9,43.8,4.9,43.8z">
                                </path>
                                <path fill="#cfd8dc"
                                    d="M24,5c5.1,0,9.8,2,13.4,5.6C41,14.2,43,18.9,43,24c0,10.5-8.5,19-19,19h0c-3.2,0-6.3-0.8-9.1-2.3	L4.9,43.3l2.7-9.8C5.9,30.6,5,27.3,5,24C5,13.5,13.5,5,24,5 M24,43L24,43L24,43 M24,43L24,43L24,43 M24,4L24,4C13,4,4,13,4,24	c0,3.4,0.8,6.7,2.5,9.6L3.9,43c-0.1,0.3,0,0.7,0.3,1c0.2,0.2,0.4,0.3,0.7,0.3c0.1,0,0.2,0,0.3,0l9.7-2.5c2.8,1.5,6,2.2,9.2,2.2	c11,0,20-9,20-20c0-5.3-2.1-10.4-5.8-14.1C34.4,6.1,29.4,4,24,4L24,4z">
                                </path>
                                <path fill="#40c351"
                                    d="M35.2,12.8c-3-3-6.9-4.6-11.2-4.6C15.3,8.2,8.2,15.3,8.2,24c0,3,0.8,5.9,2.4,8.4L11,33l-1.6,5.8	l6-1.6l0.6,0.3c2.4,1.4,5.2,2.2,8,2.2h0c8.7,0,15.8-7.1,15.8-15.8C39.8,19.8,38.2,15.8,35.2,12.8z">
                                </path>
                                <path fill="#fff" fill-rule="evenodd"
                                    d="M19.3,16c-0.4-0.8-0.7-0.8-1.1-0.8c-0.3,0-0.6,0-0.9,0	s-0.8,0.1-1.3,0.6c-0.4,0.5-1.7,1.6-1.7,4s1.7,4.6,1.9,4.9s3.3,5.3,8.1,7.2c4,1.6,4.8,1.3,5.7,1.2c0.9-0.1,2.8-1.1,3.2-2.3	c0.4-1.1,0.4-2.1,0.3-2.3c-0.1-0.2-0.4-0.3-0.9-0.6s-2.8-1.4-3.2-1.5c-0.4-0.2-0.8-0.2-1.1,0.2c-0.3,0.5-1.2,1.5-1.5,1.9	c-0.3,0.3-0.6,0.4-1,0.1c-0.5-0.2-2-0.7-3.8-2.4c-1.4-1.3-2.4-2.8-2.6-3.3c-0.3-0.5,0-0.7,0.2-1c0.2-0.2,0.5-0.6,0.7-0.8	c0.2-0.3,0.3-0.5,0.5-0.8c0.2-0.3,0.1-0.6,0-0.8C20.6,19.3,19.7,17,19.3,16z"
                                    clip-rule="evenodd"></path>
                            </svg>
                            {{ __('dictionary.contact_us') }}
                        </a>
                    </div>
                </div>

                <!-- Contact form -->
                <div class="px-6 pt-5 pb-10 sm:px-10 lg:col-span-3 xl:px-12">
                    <div class="grid grid-cols-1 mt-6 gap-y-6 sm:grid-cols-2 md:grid-cols-3 sm:gap-x-8">
                        <div class="col-span-1 md:col-span-2">
                            <label for="order-type" class="block text-sm font-medium text-warm-gray-900">
                                {{ __('dictionary.order_type') }}</label>
                            <div class="mt-1">
                                <select wire:change="calculatePrice()" id="order-type" name="order-type"
                                    wire:model="inputOrderType"
                                    class="block w-full px-4 py-3 rounded-md shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300">

                                    <option value="">---- {{ __('dictionary.select').' '. __('dictionary.order_type')}}
                                        ----</option>
                                    <option value="{{ __('dictionary.translation_pricing_document_normal_title') }}">{{
                                        __('dictionary.translation_pricing_document_normal_title') }}</option>
                                    <option value="{{ __('dictionary.translation_pricing_application_title') }}">{{
                                        __('dictionary.translation_pricing_application_title') }}</option>
                                    <option value="{{ __('dictionary.translation_pricing_proof_read_title') }}">{{
                                        __('dictionary.translation_pricing_proof_read_title') }}</option>
                                </select>
                            </div>
                        </div>
                        <div>
                            <div class="flex justify-between">
                                <label for="phone" class="block text-sm font-medium text-warm-gray-900">
                                    {{ __('dictionary.order_package') }}</label>
                            </div>
                            <div class="mt-1">
                                <select wire:change="calculatePrice()" id="order-package" name="order-package"
                                    wire:model="inputOrderPackage"
                                    class="block w-full px-4 py-3 rounded-md shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300">

                                    <option value="">
                                        ---- {{ __('dictionary.select').' '.__('dictionary.order_package')}} ----
                                    </option>
                                    <option value="{{ __('dictionary.regular') }}">
                                        {{__('dictionary.regular') }}
                                    </option>
                                    <option value="{{ __('dictionary.express') }}">
                                        {{__('dictionary.express') }}
                                    </option>
                                </select>
                            </div>
                        </div>

                        @if($inputOrderType == __('dictionary.translation_pricing_proof_read_title'))
                        <div class="col-span-1 md:col-span-2">
                            <label for="language" class="block text-sm font-medium text-warm-gray-900">
                                {{ __('dictionary.language') }}</label>
                            <div class="mt-1">
                                <select wire:change="calculatePrice()" id="language" name="language"
                                    wire:model="inputLanguage"
                                    class="block w-full px-4 py-3 rounded-md shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300">

                                    <option value="">
                                        ---- {{ __('dictionary.select').' '.__('dictionary.language')}} ----
                                    </option>
                                    @foreach ($translator as $t)
                                    @if(in_array($t->code, ['en', 'id']))
                                    <option value="{{ $t->code }}">
                                        {{ $t->name }}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @else
                        <div>
                            <label for="language-from" class="block text-sm font-medium text-warm-gray-900">
                                {{ __('dictionary.translate_from') }}</label>
                            <div class="mt-1">
                                <select wire:change="calculatePrice()" id="language-from" name="language-from"
                                    wire:model="inputLanguageFrom"
                                    class="block w-full px-4 py-3 rounded-md shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300">

                                    <option value="">
                                        ---- {{ __('dictionary.select').' '.__('dictionary.language')}} ----
                                    </option>
                                    @foreach ($translator as $t)
                                    <option value="{{ $t->code }}">
                                        {{ $t->name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div>
                            <label for="email" class="block text-sm font-medium text-warm-gray-900">
                                {{ __('dictionary.translate_to') }}</label>
                            <div class="mt-1">
                                <select wire:change="calculatePrice()" id="language-to" name="language-to"
                                    wire:model="inputLanguageTo"
                                    class="block w-full px-4 py-3 rounded-md shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300">

                                    <option value="">
                                        ---- {{ __('dictionary.select').' '.__('dictionary.language')}} ----
                                    </option>
                                    @foreach ($translator as $t)
                                    <option value="{{ $t->code }}">
                                        {{ $t->name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endif
                        <div>
                            <label for="qty" class="block text-sm font-medium text-warm-gray-900">
                                {{ __('dictionary.number_of').' '.$qty }}
                            </label>
                            <div class="mt-1">
                                <input wire:change="calculatePrice()" type="number" name="qty" id="qty"
                                    wire:model="inputQty"
                                    class="block w-full px-4 py-3 rounded-md shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
