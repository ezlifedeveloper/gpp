<div>
    <article class="pb-6 bg-white rounded-lg shadow lg:col-span-2">
        <!-- Profile header -->
        <div>
            <div>
                <img class="object-cover w-full h-32 lg:h-48"
                    src="{{ $cover ? (!is_string($cover) ? $cover->temporaryUrl() : $cover) : 'https://images.unsplash.com/photo-1444628838545-ac4016a5418a?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80'}}"
                    alt="">
            </div>
            <div class="max-w-3xl px-4 sm:px-6 lg:px-8">
                <div class="-mt-12 sm:-mt-16 sm:flex sm:items-end sm:space-x-5">
                    <div class="flex">
                        @if($photo)
                        <img class="w-24 h-24 rounded-full ring-4 ring-white sm:h-32 sm:w-32"
                            src="{{ !is_string($photo) ? $photo->temporaryUrl() : $photo }}" alt="">
                        @else
                        <span
                            class="inline-flex items-center justify-center w-24 h-24 bg-blue-500 rounded-full sm:h-32 sm:w-32 ">
                            <span class="text-3xl font-medium leading-none text-white">{{
                                Helper::getNameInitial($inputName)
                                }}</span>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="xl:grid xl:grid-cols-2">
            <!-- Description list -->
            <div class="px-4 mt-6 sm:px-6 lg:px-8">
                @if($editProfile)
                <form method="post" wire:submit.prevent="updateProfile">@endif
                    <div class="flex justify-between flex-initial mb-6">
                        <h1 class="text-2xl font-bold text-gray-900 truncate">
                            {{ $inputName }}
                        </h1>

                        <div>
                            @if(!$editProfile)
                            <button wire:click.prevent="toggleEditProfile"
                                class="px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 ">
                                Edit
                            </button>
                            @else
                            <button wire:click.prevent="toggleEditProfile"
                                class="px-4 py-2 text-sm font-medium text-white bg-red-500 border border-white rounded-md shadow-sm hover:bg-red-700 ">
                                Cancel
                            </button>
                            <button wire:click.prevent="updateProfile"
                                class="px-4 py-2 text-sm font-medium text-white bg-green-500 border border-white rounded-md shadow-sm hover:bg-green-700 ">
                                Save
                            </button>
                            @endif
                        </div>
                    </div>

                    <dl class="grid grid-cols-1 gap-x-4 gap-y-8 sm:grid-cols-2">
                        <div class="sm:col-span-1">
                            <dt class="text-sm font-medium text-gray-500">
                                Name
                            </dt>
                            @if(!$editProfile)
                            <dd class="mt-1 text-sm text-gray-900">
                                {{ Auth::user()->name }}
                            </dd>
                            @else
                            <input type="text" name="name" id="name" wire:model="inputName"
                                class="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            @error('inputName') <div class="invalid-feedback">{{ $message }}</div> @enderror
                            @endif
                        </div>

                        <div class="sm:col-span-1">
                            <dt class="text-sm font-medium text-gray-500">
                                Username
                            </dt>
                            @if(!$editProfile)
                            <dd class="mt-1 text-sm text-gray-900">
                                {{ Auth::user()->username }}
                            </dd>
                            @else
                            <input type="text" name="username" id="username" wire:model="inputUsername"
                                class="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            @error('inputUsername') <div class="invalid-feedback">{{ $message }}</div> @enderror
                            @endif
                        </div>

                        <div class="sm:col-span-2">
                            <dt class="text-sm font-medium text-gray-500">
                                About
                            </dt>
                            <dd class="mt-1 space-y-5 text-sm text-gray-900">
                                @if(!$editProfile)
                                @if(!Auth::user()->desc)
                                <p>Tincidunt quam neque in cursus viverra orci, dapibus nec tristique. Nullam ut sit
                                    dolor
                                    consectetur urna, dui cras nec sed. Cursus risus congue arcu aenean posuere aliquam.
                                </p>
                                <p>Et vivamus lorem pulvinar nascetur non. Pulvinar a sed platea rhoncus ac mauris amet.
                                    Urna,
                                    sem pretium sit pretium urna, senectus vitae. Scelerisque fermentum, cursus felis
                                    dui
                                    suspendisse velit pharetra. Augue et duis cursus maecenas eget quam lectus. Accumsan
                                    vitae
                                    nascetur pharetra rhoncus praesent dictum risus suspendisse.
                                </p>
                                @else
                                {{-- <p class="mt-1 text-sm text-gray-900"> --}}
                                    {!! nl2br($inputDesc) !!}
                                {{-- </p> --}}
                                @endif
                                @else
                                <textarea name="desc" id="desc" wire:model="inputDesc" rows="5"
                                    class="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"></textarea>
                                @error('inputDesc') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                @endif
                            </dd>
                        </div>
                        
                        @if($editProfile)
                        <div class="sm:col-span-1">
                            <dt class="text-sm font-medium text-gray-500">
                                Profile Picture
                            </dt>
                            <input type="file" name="photo" id="photo" wire:model="photo" accept="image/*" capture="environment"
                                class="block w-full mt-1 border-gray-300 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            @error('photo') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>

                        <div class="sm:col-span-1">
                            <dt class="text-sm font-medium text-gray-500">
                                Profile Header
                            </dt>
                            <input type="file" name="cover" id="cover" wire:model="cover"
                                class="block w-full mt-1 border-gray-300 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            @error('cover') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                        @endif
                    </dl>
                    @if($editProfile)
                </form> @endif
            </div>

            @if(!$editProfile)
            <!-- Description list -->
            <div class="px-4 mt-6 sm:px-6 lg:px-8">
                <ul role="list" class="grid grid-cols-1 gap-5 sm:gap-6 sm:grid-cols-2 lg:grid-cols-2">
                    <x-admin.info-bar :title="'Joined Since'" :desc="$joined" :color="'blue-600'">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                        </svg>
                    </x-admin.info-bar>
                    <x-admin.info-bar :title="Helper::isAllowed('charity partner') ? 'Executed' : 'Donated'" :desc="Helper::formatRupiah($donated)" :color="'green-600'">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z" />
                        </svg>
                    </x-admin.info-bar>
                    <x-admin.info-bar :title="'Event Joined'" :desc="$participated.' events'" :color="'gray-400'">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path d="M9 17a2 2 0 11-4 0 2 2 0 014 0zM19 17a2 2 0 11-4 0 2 2 0 014 0z" />
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M13 16V6a1 1 0 00-1-1H4a1 1 0 00-1 1v10a1 1 0 001 1h1m8-1a1 1 0 01-1 1H9m4-1V8a1 1 0 011-1h2.586a1 1 0 01.707.293l3.414 3.414a1 1 0 01.293.707V16a1 1 0 01-1 1h-1m-6-1a1 1 0 001 1h1M5 17a2 2 0 104 0m-4 0a2 2 0 114 0m6 0a2 2 0 104 0m-4 0a2 2 0 114 0" />
                        </svg>
                    </x-admin.info-bar>
                </ul>
            </div>
            @endif
        </div>
    </article>
</div>
