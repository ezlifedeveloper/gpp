<div class="mt-5">
    <dl class="grid h-full grid-cols-2 gap-5 xs:grid-cols-1 sm:grid-cols-2 lg:grid-cols-5">
        <x-admin.info-bar-2 :color="'gray-500'" :title="'Balance'" :text="Helper::formatRupiah($recap['balance'])">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z" />
        </x-admin.info-bar-2>
        <x-admin.info-bar-2 :color="'green-500'" :title="'Income'" :text="Helper::formatRupiah($recap['income'])">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z" />
        </x-admin.info-bar-2>
        <x-admin.info-bar-2 :color="'red-500'" :title="'Expense'" :text="Helper::formatRupiah($recap['expense'])">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z" />
        </x-admin.info-bar-2>
        <x-admin.info-bar-2 :color="'yellow-500'" :title="'Debt'" :text="Helper::formatRupiah($recap['debt'])">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z" />
        </x-admin.info-bar-2>
        <x-admin.info-bar-2 :color="'purple-500'" :title="'Receivable'" :text="Helper::formatRupiah($recap['receivable'])">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z" />
        </x-admin.info-bar-2>
    </dl>

    @if($openForm)
    <div class="h-full p-4 mt-5 bg-white rounded-lg shadow">
        <div class="mb-4">
            <h3 class="text-lg font-medium text-gray-900">
                @if($editData) Update @else Add @endif Transaction Data
            </h3>
        </div>

        <div class="grid grid-cols-1 mt-6 gap-y-6 gap-x-4 md:grid-cols-2 lg:grid-cols-4">
            <div>
                <label for="date" class="block text-sm font-medium text-gray-700">
                    Date
                </label>
                <div class="mt-1">
                    <input type="date" name="date" id="date" wire:model='inputDate'
                        class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                </div>
            </div>
            <div>
                <label id="listbox-label" class="block text-sm font-medium text-gray-700">
                    Category
                </label>
                <div class="relative mt-1">
                    <select wire:model="inputCategory"
                        class="block w-full px-3 py-2 mt-1 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
        
                        <option value="null">---- Select Category ----</option>
                        @foreach($category as $c)
                        <option value="{{$c->id}}">{{$c->flow.($c->name ? ' - '.$c->name : '')}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div>
                <label id="listbox-label" class="block text-sm font-medium text-gray-700">
                    Account
                </label>
                <div class="relative mt-1">
                    <select wire:model="inputAccount"
                        class="block w-full px-3 py-2 mt-1 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
        
                        <option value="null">---- Select Account ----</option>
                        @foreach($account as $c)
                        <option value="{{$c->id}}">{{ $c->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div>
                <label for="value" class="block text-sm font-medium text-gray-700">
                    Amount
                </label>
                <div class="mt-1">
                    <input type="number" wire:model='inputAmount'
                        class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                        placeholder="1000000">
                </div>
            </div>
            <div class="col-span-1 md:col-span-2 ">
                <label for="name" class="block text-sm font-medium text-gray-700">
                    Description
                </label>
                <div class="mt-1">
                    <input type="text" wire:model='inputNote'
                        class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                        placeholder="Pembayaran proyek XXX / payroll YYY">
                </div>
            </div> 
        </div>
        
        <x-admin.form-bottom-button :cancel="'toggleEditData'" :submit="'storeData'">
        </x-admin.form-bottom-button>
    </div>
    @endif

    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8 sm:rounded-lg">
            <x-admin.table-header>
                <div class="flex-1">
                    <h3 class="text-lg font-medium ">
                        Finance Data
                    </h3>
                    @if(Helper::isAllowed('superuser'))
                    <x-admin.button-create-component></x-admin.button-create-component>
                    @endif
                </div>
            </x-admin.table-header>
            <div class="overflow-hidden border-b border-gray-200 shadow">
                <table class="min-w-full divide-y divide-gray-200">
                    <thead class="bg-blue-100">
                        <tr class="font-bold text-black">
                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-center uppercase">
                                No
                            </th>
                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                                Date
                            </th>
                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                                Note
                            </th>
                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                                Category
                            </th>
                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                                Account
                            </th>
                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                                Amount
                            </th>
                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                                Creator
                            </th>
                            <th scope="col" class="relative px-6 py-3"></th>
                        </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                        @forelse($data as $d)
                        <tr>
                            <x-admin.numbering :data="$data" :loop="$loop" :pagination="true"></x-admin.numbering>
                            <td class="px-6 py-4 whitespace-nowrap">
                                {{ $d->date }}
                            </td>
                            <td class="px-6 py-4">
                                {{ $d->note }}
                            </td>
                            <td class="px-6 py-4 
                            @if($d->flow == 'Income') text-green-500
                            @elseif($d->flow == 'Expense') text-red-500
                            @elseif($d->flow == 'Borrow') text-yellow-500
                            @endif">
                                {{ $d->flow.($d->category ? ' - '.$d->category : '') }}
                            </td>
                            <td class="px-6 py-4">
                                {{ $d->account_name }}
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-right">
                                {{ Helper::formatRupiah($d->amount) }}
                            </td>
                            <td class="px-6 py-4">
                                {{ $d->creator->name }}
                            </td>

                            <td class="gap-4 px-6 py-4 font-medium text-right">
                                <x-admin.button-edit-component :id="$d->id"></x-admin.button-edit-component>
                                <x-admin.button-delete-component :id="$d->id"></x-admin.button-delete-component>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td class="col-span-6 px-6 py-4 text-sm">
                                No data available
                            </td>
                            <td class="col-span-6 px-6 py-4 text-sm"></td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="mt-4 text-xs">
                @if($data->hasPages())
                {{ $data->links() }}
                @endif
            </div>
        </div>
    </div>
</div>
