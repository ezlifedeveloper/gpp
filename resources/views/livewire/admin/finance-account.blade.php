<div class="mt-5">
    <dl class="grid h-full grid-cols-2 gap-5 xs:grid-cols-1 xl:grid-cols-4">
        <x-admin.info-bar-2 :color="'indigo-500'" :title="'Account'" :text="$data->total()">
            <path stroke-linecap="round" stroke-width="2" stroke-linejoin="round" d="M2.25 8.25h19.5M2.25 9h19.5m-16.5 5.25h6m-6 2.25h3m-3.75 3h15a2.25 2.25 0 002.25-2.25V6.75A2.25 2.25 0 0019.5 4.5h-15a2.25 2.25 0 00-2.25 2.25v10.5A2.25 2.25 0 004.5 19.5z" />
        </x-admin.info-bar-2>
    </dl>

    @if($openForm)
    <div class="h-full p-4 mt-5 bg-white rounded-lg shadow">
        <div class="mb-4">
            <h3 class="text-lg font-medium text-gray-900">
                @if($editData) Update @else Add @endif Account Data
            </h3>
        </div>

        <div class="mt-6 gap-y-6 gap-x-4">
            <div>
                <label for="name" class="block text-sm font-medium text-gray-700">
                    Account Name
                </label>
                <div class="mt-1">
                    <input type="text" name="name" id="name" wire:model='inputName'
                        class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                        placeholder="Rek. BRI / cash / other / ...">
                </div>
            </div>        
        </div>
        
        <x-admin.form-bottom-button :cancel="'toggleEditData'" :submit="'storeData'">
        </x-admin.form-bottom-button>
    </div>
    @endif

    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8 sm:rounded-lg">
            <x-admin.table-header>
                <div class="flex-1">
                    <h3 class="text-lg font-medium ">
                        Finance Account Data
                    </h3>
                    @if(Helper::isAllowed('superuser'))
                    <x-admin.button-create-component></x-admin.button-create-component>
                    @endif
                </div>
            </x-admin.table-header>
            <div class="overflow-hidden border-b border-gray-200 shadow">
                <table class="min-w-full divide-y divide-gray-200">
                    <thead class="bg-blue-100">
                        <tr class="font-bold text-black">
                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-center uppercase">
                                No
                            </th>
                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                                Name
                            </th>
                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                                Creator
                            </th>
                            <th scope="col" class="relative px-6 py-3"></th>
                        </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                        @forelse($data as $d)
                        <tr>
                            <x-admin.numbering :data="$data" :loop="$loop" :pagination="true"></x-admin.numbering>
                            <td class="px-6 py-4">
                                {{ $d->name }}
                            </td>
                            <td class="px-6 py-4">
                                {{ $d->creator->name }}
                            </td>

                            <td class="gap-4 px-6 py-4 font-medium text-right">
                                <x-admin.button-edit-component :id="$d->id"></x-admin.button-edit-component>
                                <x-admin.button-delete-component :id="$d->id"></x-admin.button-delete-component>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td class="col-span-6 px-6 py-4 text-sm">
                                No data available
                            </td>
                            <td class="col-span-6 px-6 py-4 text-sm"></td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="mt-4 text-xs">
                @if($data->hasPages())
                {{ $data->links() }}
                @endif
            </div>
        </div>
    </div>
</div>
