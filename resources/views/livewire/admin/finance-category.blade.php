<div class="mt-5">
    <dl class="grid h-full grid-cols-2 gap-5 xs:grid-cols-1 xl:grid-cols-4">
        <x-admin.info-bar-2 :color="'indigo-500'" :title="'Category'" :text="$data->total()">
            <path stroke-linecap="round" stroke-width="2" stroke-linejoin="round" d="M8.25 6.75h12M8.25 12h12m-12 5.25h12M3.75 6.75h.007v.008H3.75V6.75zm.375 0a.375.375 0 11-.75 0 .375.375 0 01.75 0zM3.75 12h.007v.008H3.75V12zm.375 0a.375.375 0 11-.75 0 .375.375 0 01.75 0zm-.375 5.25h.007v.008H3.75v-.008zm.375 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z" />
        </x-admin.info-bar-2>
    </dl>

    @if($openForm)
    <div class="h-full p-4 mt-5 bg-white rounded-lg shadow">
        <div class="mb-4">
            <h3 class="text-lg font-medium text-gray-900">
                @if($editData) Update @else Add @endif Category Data
            </h3>
        </div>

        <div class="grid grid-cols-1 mt-6 gap-y-6 gap-x-4 md:grid-cols-2">
            <div>
                <label for="name" class="block text-sm font-medium text-gray-700">
                    Type
                </label>
                <div class="mt-1">
                    <input type="text" name="name" id="name" wire:model='inputType'
                        class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                        placeholder="Income / Expense / ...">
                </div>
            </div> 
            <div>
                <label for="name" class="block text-sm font-medium text-gray-700">
                    Category Name
                </label>
                <div class="mt-1">
                    <input type="text" name="name" id="name" wire:model='inputName'
                        class="block w-full border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                        placeholder="Transport / Event / ...">
                </div>
            </div> 
        </div>
        
        <x-admin.form-bottom-button :cancel="'toggleEditData'" :submit="'storeData'">
        </x-admin.form-bottom-button>
    </div>
    @endif

    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8 sm:rounded-lg">
            <x-admin.table-header>
                <div class="flex-1">
                    <h3 class="text-lg font-medium ">
                        Finance Category Data
                    </h3>
                    @if(Helper::isAllowed('superuser'))
                    <x-admin.button-create-component></x-admin.button-create-component>
                    @endif
                </div>
            </x-admin.table-header>
            <div class="overflow-hidden border-b border-gray-200 shadow">
                <table class="min-w-full divide-y divide-gray-200">
                    <thead class="bg-blue-100">
                        <tr class="font-bold text-black">
                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-center uppercase">
                                No
                            </th>
                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                                Flow
                            </th>
                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                                Name
                            </th>
                            <th scope="col" class="px-6 py-3 text-xs font-medium tracking-wider text-left uppercase">
                                Creator
                            </th>
                            <th scope="col" class="relative px-6 py-3"></th>
                        </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                        @forelse($data as $d)
                        <tr>
                            <x-admin.numbering :data="$data" :loop="$loop" :pagination="true"></x-admin.numbering>
                            <td class="px-6 py-4">
                                {{ $d->flow }}
                            </td>
                            <td class="px-6 py-4">
                                {{ $d->name }}
                            </td>
                            <td class="px-6 py-4">
                                {{ $d->creator->name }}
                            </td>

                            <td class="gap-4 px-6 py-4 font-medium text-right">
                                <x-admin.button-edit-component :id="$d->id"></x-admin.button-edit-component>
                                <x-admin.button-delete-component :id="$d->id"></x-admin.button-delete-component>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td class="col-span-6 px-6 py-4 text-sm">
                                No data available
                            </td>
                            <td class="col-span-6 px-6 py-4 text-sm"></td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="mt-4 text-xs">
                @if($data->hasPages())
                {{ $data->links() }}
                @endif
            </div>
        </div>
    </div>
</div>
