<div>
    <div class="gap-5 mt-5 lg:flex xs:grid xs:grid-cols-1 xs:gap-5">
        <div class="h-full p-4 mb-4 bg-white rounded-lg shadow lg:w-2/3 xs:span-col-1">
            <div class="mb-4">
                <h3 class="text-lg font-medium text-gray-900">
                    Timeline
                </h3>
                <p class="mt-1 text-sm text-gray-500 @if(count($data)==0) mb-8 @endif">
                    @if(count($data)>0) All charity event shown in timeline below
                    @else You haven’t created an event yet
                    @endif
                </p>
            </div>
            <x-admin.timeline :data="$data" :user="$user" :openedImage="$openedImage"></x-admin.timeline>
            <script type="text/javascript">
                window.onscroll = function (ev) {
                    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                        window.livewire.emit('load-more');
                    }
                };

            </script>
        </div>

        @if(Helper::isAllowed('superuser') || Auth::id() == 23)
        <div class="h-full lg:w-1/3 xs:span-col-1">
            @if($addPhoto)
            <div class="h-full p-4 mb-8 bg-white rounded-lg shadow">
                <div class="mb-4">
                    <h3 class="text-lg font-medium text-gray-900">
                        Add Photo
                    </h3>
                </div>

                <x-admin.add-file-item></x-admin.add-file-item>
                <x-admin.form-bottom-button :cancel="'toggleAddPhoto'" :submit="'uploadPhoto'">
                </x-admin.form-bottom-button>
            </div>
            @endif

            @if($addParticipant)
            <div class="h-full p-4 mb-8 bg-white rounded-lg shadow">
                <div class="mb-4">
                    <h3 class="text-lg font-medium text-gray-900">
                        Add Participant
                    </h3>
                </div>

                <x-admin.add-participant-item :user="$user"></x-admin.add-participant-item>
                <x-admin.form-bottom-button :cancel="'toggleAddParticipant'" :submit="'addParticipant'">
                </x-admin.form-bottom-button>
            </div>
            @endif

            <div class="h-full p-4 bg-white rounded-lg shadow">
                <div class="mb-4">
                    <h3 class="text-lg font-medium text-gray-900">
                        @if(!$editCharity) Create @else Update @endif Charity
                    </h3>
                    <p class="mt-1 text-sm text-gray-500">
                        Fill the form below to @if(!$editCharity) create new @else update @endif charity event
                    </p>
                </div>

                <form method="POST">
                    @csrf
                    <x-admin.charity-form></x-admin.charity-form>

                    <x-admin.form-bottom-button :cancel="'toggleEditCharity'" :submit="'storeCharity'">
                    </x-admin.form-bottom-button>
                </form>
            </div>
        </div>
        @endif
    </div>
</div>
