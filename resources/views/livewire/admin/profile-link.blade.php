<div class="mt-5 min-h-screen" style="background-image: url({{Auth::user()->cover ?? 'https://images.unsplash.com/photo-1444628838545-ac4016a5418a?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop'}}); background-size: cover">
    @if($openModal)
    <div class="fixed z-10 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true">
        <div class="flex items-center justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100"
                x-transition:leave="ease-in duration-200" x-transition:leave-start="opacity-100" x-transition:enter-end="opacity-0"
                class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>
      
            <!-- This element is to trick the browser into centering the modal contents. -->
            <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
            <div x-transition:enter="ease-out duration-300" x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95" x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
                x-transition:leave="ease-in duration-200" x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100" x-transition:enter-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                class="inline-block align-bottom bg-white rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-5xl sm:w-full sm:p-6">
                <div class="mb-4">
                    <h3 class="text-lg font-medium text-gray-900">
                        @if($dataId) Update @else Add @endif Link
                    </h3>
                </div>
                <div>
                    <div class="justify-content gap-5">
                        <x-form.text :defer="false" :title="'Judul'" :model="'inputTitle'" :required="true" :placeholder="'Judul Link'"></x-form.text>
                        <x-form.text :defer="false" :title="'URL'" :model="'inputUrl'" :required="true" :placeholder="'URL Link'"></x-form.text>
                        <x-form.color-picker :title="'Warna Text'" :model="'inputTextColor'" :required="true" :placeholder="'#FF0000'"></x-form.color-picker>
                        <x-form.color-picker :title="'Warna Background'" :model="'inputBackgroundColor'" :required="true" :placeholder="'#FF0000'"></x-form.color-picker>
                    </div>
                    <x-link-button :text="$inputTitle" :href="$inputUrl" :textcolor="$inputTextColor" :bgcolor="$inputBackgroundColor" />
                </div>
                <div class="pt-5">
                    <div class="flex justify-end">
                        <button wire:click.prevent="toggleModal(false)"
                            class="px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Cancel
                        </button>
                        <button wire:click.prevent="save"
                            class="inline-flex justify-center px-4 py-2 ml-3 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    
    <div class="flex flex-col pt-10 text-white text-center items-center">
        <img class="rounded-full w-36 h-36" style="background-color: white" src="{{Auth::user()->photo}}">
        <h1 class="text-3xl font-bold mt-3">{{Auth::user()->name}}</h1>

        <a href="{{ url('link/'.Auth::user()->username) }}" target="_blank"
            class="flex px-4 py-3 max-w-3xl mt-2 rounded-full bg-white gap-3 text-blue-500 border-2 border-white-500">
            <!-- Button tag -->
            <span class="flex-1 text-md font-semibold text-center my-auto">LIHAT LIVE</span>
        </a>

        <p class="max-w-3xl text-xl mt-10 mb-5">{!! nl2br(Auth::user()->desc) !!}</p>
        
        @if(isset(Auth::user()->links))
        @foreach(Auth::user()->links as $link)
        <x-link-button :text="$link->title" :href="$link->url" :textcolor="$link->textcolor" :bgcolor="$link->bgcolor" :admin="true" :id="$link->id" />
        @endforeach
        @endif

        <button wire:click="toggleModal(true)"
            class="flex w-6/12 px-4 py-3 max-w-3xl mt-10 rounded-full bg-blue-500 gap-3 text-white-500 border-2 border-white-500 hover:bg-blue-700">
            <!-- Button tag -->
            <span class="flex-1 text-xl font-semibold text-center my-auto">ADD NEW LINK</span>
        </button>
    </div>
</div>
