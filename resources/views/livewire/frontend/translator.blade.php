<div>
    <x-frontend.translator-hero></x-frontend.translator-hero>
    <x-frontend.translator-document-type></x-frontend.translator-document-type>
    <x-frontend.translator-list :translator="$translator"></x-frontend.translator-list>
    <x-frontend.translator-price></x-frontend.translator-price>
    <x-frontend.translator-price-calculator :translator="$translator" :qty="$qty" :price="$price" :days="$days"
        :messageUrl="$messageUrl" :inputOrderType="$inputOrderType">
    </x-frontend.translator-price-calculator>
</div>
