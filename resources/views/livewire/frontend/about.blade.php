<div>
    <x-frontend.about-hero></x-frontend.about-hero>
    <x-frontend.about-stats :project="$project" :charity="$charity" :translation="$translation">
    </x-frontend.about-stats>

    <section style="background: #f6f6f6">
        <div class="px-4 py-20 mx-auto text-center sm:px-6">
            <div class="flex justify-center px-4">
                <div class="max-w-3xl mx-auto">
                    <div class="text-lg text-slate-500">
                        <h2 class="mb-4 text-5xl font-bold h3 text-slate-800" data-aos="fade-up" data-aos-delay="450">
                            Our Story</h2>
                        <p class="mb-8" data-aos="fade-left" data-aos-delay="600">
                            {{ __('dictionary.about_seo_description') }}</p>
                        <p class="mb-8" data-aos="fade-right" data-aos-delay="750">
                            {{ __('dictionary.home_seo_description') }}
                        </p>
                        <p class="mb-8" data-aos="fade-up" data-aos-delay="900">
                            - The GPP Team -
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <x-frontend.kbli-detail></x-frontend.kbli-detail>
    <section style="background: #f6f6f6">
        <div class="max-w-7xl px-4 py-16 mx-auto mt-16 text-center sm:px-6">
            <div class="max-w-3xl mx-auto mb-8 text-center">
                <h2 class="text-4xl h2 font-playfair-display text-slate-800"
                    style="font-family: 'Playfair Display', serif;" data-aos="fade-up" data-aos-delay="450">Our Team
                </h2>
            </div>
            <div class="grid grid-cols-1 mt-8 sm:mt-16 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-6">
                <x-frontend.item-team :name="'Ronald Brian'" :size="'col-span-1'" :position="'CEO'"
                    :image="asset('team/1.png')"></x-frontend.item-team>
                <x-frontend.item-team :name="'Andy Yohanes H'" :size="'col-span-1'" :position="'Mobile App Developer'"
                    :image="asset('team/2.png')"></x-frontend.item-team>
                <x-frontend.item-team :name="'Prima Widiani'" :size="'col-span-1'" :position="'Translator'"
                    :image="asset('team/3.png')"></x-frontend.item-team>
                <x-frontend.item-team :name="'Prasetyo Nugrohadi'" :size="'col-span-1 md:col-span-2 lg:col-span-1'"
                    :position="'Software Engineer'" :image="asset('team/4.png')">
                </x-frontend.item-team>
                <x-frontend.item-team :name="'Agus Priyono'" :size="'col-span-1'" :position="'Creative Director'"
                    :image="asset('team/5.png')">
                </x-frontend.item-team>
                <x-frontend.item-team :name="'Herri Purba'" :size="'col-span-1'" :position="'Web Programmer'"
                    :image="asset('team/6.png')">
                </x-frontend.item-team>
            </div>
        </div>
    </section>
</div>
