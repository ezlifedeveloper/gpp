<div>
    <x-frontend.home-hero></x-frontend.home-hero>
    <x-frontend.home-dev :data="$portfolio"></x-frontend.home-dev>
    <x-frontend.home-client :data="$client"></x-frontend.home-client>
    <x-frontend.home-translator :data="$translator"></x-frontend.home-translator>
    <x-frontend.home-charity :data="$charity"></x-frontend.home-charity>
</div>
