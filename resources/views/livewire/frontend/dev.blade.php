<div>
    <x-frontend.dev-hero></x-frontend.dev-hero>
    <x-frontend.dev-product></x-frontend.dev-product>
    <x-frontend.home-client :data="$client"></x-frontend.home-client>
    <x-frontend.dev-latest-project :portfolio="$portfolio"></x-frontend.dev-latest-project>
</div>
