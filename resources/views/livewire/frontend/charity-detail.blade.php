<div>
    <x-frontend.charity-detail-hero :data="$data" :color="$selectedColor"></x-frontend.charity-detail-hero>
    <x-frontend.charity-detail-content :data="$data" :finance="$finance"></x-frontend.charity-detail-content>
</div>
