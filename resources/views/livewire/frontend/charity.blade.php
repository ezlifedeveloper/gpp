<div>
    <x-frontend.charity-hero></x-frontend.charity-hero>
    <x-frontend.charity-stats :total="Helper::formatRupiahShortened($funded)" :event="$charity->total()"
        :participant="count($participant)">
    </x-frontend.charity-stats>
    <x-frontend.charity-finance :finance="$finance"></x-frontend.charity-finance>
    <x-frontend.charity-partner-list :partner="$partner"></x-frontend.charity-partner-list>
    <x-frontend.charity-participant-circle :data="$participant" :size="$imageSize">
    </x-frontend.charity-participant-circle>
    <x-frontend.charity-timeline :data="$charity"></x-frontend.charity-timeline>

    <div class="w-full text-center">
        <div wire:loading.flex wire:target="load-more">
            <svg class="w-5 h-5 mr-3 -ml-1 text-white animate-spin" xmlns="http://www.w3.org/2000/svg" fill="none"
                viewBox="0 0 24 24">
                <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
                <path class="opacity-75" fill="currentColor"
                    d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z">
                </path>
            </svg>
            Loading More Data...
        </div>
    </div>
    <script type="text/javascript">
        window.onscroll = function (ev) {
            if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight-150) {
                window.livewire.emit('load-more');
            }
        };
    </script>
</div>
