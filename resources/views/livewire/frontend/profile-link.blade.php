<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-full bg-gray-50 snippet-html js-focus-visible"
    data-js-focus-visible style="scroll-behavior: smooth;">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{$user->name. ' - GPP Link'}}</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ url(asset('favicon.ico')) }}">

    <!-- SEO Tag -->
    <meta name="description" content="{{ $user->desc }}">
    <meta name="author" content="Grasia Prima Perfekta">
    <meta property="og:description" content="{{ $user->desc }}" />
    <meta property="og:url" content="{{ url('link/'.$user->username) }}" />
    <meta property="og:image" content="{{ $user->photo ?? asset('logo_bg.jpg') }}" />
    <meta property="og:title" content="{{ $user->name }}" />
    <meta property="og:type" content="{{ 'profile' }}" />
    <meta property="og:locale:alternate" content="in_ID" />

    {{-- Facebook Domain Verificator --}}
    <meta name="facebook-domain-verification" content="9vhfbfqypx230s36vrubfmujnh5zcp" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-M1FB3JCHH2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-M1FB3JCHH2');
    </script>

    <style>
        html { 
            background: url({{$user->cover ?? 'https://images.unsplash.com/photo-1444628838545-ac4016a5418a?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop'}}) no-repeat center center fixed; 
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
</head>

<body class="h-screen">
    <div class="flex flex-col px-8 md:px-0 py-10 text-white text-center items-center" >
        <img class="rounded-full w-36 h-36" style="background-color: white" src="{{$user->photo}}">
        <h1 class="text-3xl font-bold mt-3">{{$user->name}}</h1>

        <p class="max-w-3xl text-xl mt-10 mb-5">{!! nl2br($user->desc) !!}</p>
        
        @if(isset($user->links))
        @foreach($user->links as $link)
        <x-link-button :text="$link->title" :href="$link->url" :textcolor="$link->textcolor" :bgcolor="$link->bgcolor" :id="$link->id" />
        @endforeach
        @endif
    </div>
</body>