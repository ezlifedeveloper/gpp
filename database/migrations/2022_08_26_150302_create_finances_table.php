<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finances', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->text('note');

            $table->foreignId('category_id')->constraint('finance_categories');
            $table->string('flow'); // income / expense / borrow / lending
            $table->string('category')->nullable();
            
            $table->double('amount')->default(0);

            $table->foreignId('account_id')->constraint('finance_accounts');
            $table->string('account_name')->nullable();

            $table->foreignId('created_by')->constraint('users');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finances');
    }
}
