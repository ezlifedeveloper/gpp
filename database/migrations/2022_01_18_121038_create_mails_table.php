<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mails', function (Blueprint $table) {
            $table->id();

            $table->string('no');
            $table->string('sender');
            $table->string('recipient');
            $table->string('subject');
            $table->string('type'); // invoice / letter / receipt
            $table->string('status'); // in / out
            $table->date('date');
            $table->bigInteger('value')->default(0);
            $table->string('file')->nullable();

            $table->foreignId('created_by')->constraint('users');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mails');
    }
}
