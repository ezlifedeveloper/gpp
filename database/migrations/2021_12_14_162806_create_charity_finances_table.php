<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharityFinancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charity_finances', function (Blueprint $table) {
            $table->id();

            $table->foreignId('charity_id')->constraint('charities')->nullable();
            $table->foreignId('user_id')->constraint('users')->nullable();

            $table->string('detail');
            $table->bigInteger('value');
            $table->bigInteger('balance');
            $table->date('date');

            $table->foreignId('created_by')->constraint('users');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charity_finances');
    }
}
