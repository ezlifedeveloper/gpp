<?php

namespace Database\Seeders;

use App\Models\FinanceAccount;
use App\Models\FinanceCategory;
use Illuminate\Database\Seeder;

class FinanceCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FinanceAccount::create([
            'name' => 'Rek. BRI',
            'created_by' => 1,
        ]);
        FinanceAccount::create([
            'name' => 'Cash',
            'created_by' => 1,
        ]);
        FinanceAccount::create([
            'name' => 'Other',
            'created_by' => 1,
        ]);
        FinanceCategory::create([
            'flow' => 'Income',
            'name' => 'Project',
            'created_by' => 1,
        ]);
        FinanceCategory::create([
            'flow' => 'Income',
            'name' => 'Pinjam Bendera',
            'created_by' => 1,
        ]);
        FinanceCategory::create([
            'flow' => 'Expense',
            'name' => 'Payroll',
            'created_by' => 1,
        ]);
        FinanceCategory::create([
            'flow' => 'Expense',
            'name' => 'Client',
            'created_by' => 1,
        ]);
        FinanceCategory::create([
            'flow' => 'Expense',
            'name' => 'Internal',
            'created_by' => 1,
        ]);
        FinanceCategory::create([
            'flow' => 'Expense',
            'name' => 'Development',
            'created_by' => 1,
        ]);
        FinanceCategory::create([
            'flow' => 'Expense',
            'name' => 'Resource',
            'created_by' => 1,
        ]);
        FinanceCategory::create([
            'flow' => 'Lend',
            'created_by' => 1,
        ]);
        FinanceCategory::create([
            'flow' => 'Borrow',
            'created_by' => 1,
        ]);
    }
}
