<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        User::create([
            'name' => 'Prasetyo Nugrohadi',
            'username' => 'prasetyon',
            'role' => 'superuser',
            'password' => bcrypt('123'),
        ]);
        User::create([
            'name' => 'Andy Yohanes H',
            'username' => 'anyoh',
            'role' => 'superuser',
            'password' => bcrypt('123'),
        ]);
        User::create([
            'name' => 'Sarasticha Pamargi',
            'username' => 'sarasticha',
            'role' => 'charity',
            'password' => bcrypt('123'),
        ]);
        User::create([
            'name' => 'Parta Adi Putra',
            'username' => 'partaadip',
            'role' => 'charity',
            'password' => bcrypt('123'),
        ]);
        User::create([
            'name' => 'Murya Amien NP',
            'username' => 'muryaamien',
            'role' => 'charity',
            'password' => bcrypt('123'),
        ]);
        User::create([
            'name' => 'Leonardo Manurung',
            'username' => 'leonardo',
            'role' => 'charity',
            'password' => bcrypt('123'),
        ]);
        User::create([
            'name' => 'Ezra Agustina A',
            'username' => 'ezraaritonang',
            'role' => 'charity',
            'password' => bcrypt('123'),
        ]);
        User::create([
            'name' => 'I Gede Surya Dinata',
            'username' => 'desur',
            'role' => 'charity',
            'password' => bcrypt('123'),
        ]);
        User::create([
            'name' => 'Salim Fauzanul Ihsani',
            'username' => 'salimfi',
            'role' => 'charity',
            'password' => bcrypt('123'),
        ]);
        User::create([
            'name' => 'Genna Patriani',
            'username' => 'gennapa',
            'role' => 'charity',
            'password' => bcrypt('123'),
        ]);
        User::create([
            'name' => 'Rizqky Yuswiddy Juanda',
            'username' => 'rizqky',
            'role' => 'charity',
            'password' => bcrypt('123'),
        ]);
        User::create([
            'name' => 'Meidyca Febriandilla',
            'username' => 'meidycaf',
            'role' => 'charity',
            'password' => bcrypt('123'),
        ]);
    }
}
