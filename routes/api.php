<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('webhook',function (Request $request) {
    Log::info('Incoming get webhook: '.$request);
    $id = $request->hub_challenge;
    Log::info('Incoming webhook hub.challenge: '.$id);
    return $id;
});

Route::post('webhook',function (Request $request) {
    Log::info('Incoming post webhook: '.$request);
    return response()->json($request, 200);
});