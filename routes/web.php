<?php

use App\Http\Controllers\Auth\EmailVerificationController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\ProfileLinkController;
use App\Http\Livewire\Admin\Charity;
use App\Http\Livewire\Admin\CharityFinance;
use App\Http\Livewire\Admin\Client;
use App\Http\Livewire\Admin\Dashboard;
use App\Http\Livewire\Admin\Finance;
use App\Http\Livewire\Admin\FinanceAccount;
use App\Http\Livewire\Admin\FinanceCategory;
use App\Http\Livewire\Admin\Mail;
use App\Http\Livewire\Admin\Profile;
use App\Http\Livewire\Admin\ProfileLink;
use App\Http\Livewire\Admin\Project;
use App\Http\Livewire\Admin\Translator;
use App\Http\Livewire\Admin\User;
use App\Http\Livewire\Auth\Login;
use App\Http\Livewire\Auth\Passwords\Confirm;
use App\Http\Livewire\Auth\Passwords\Email;
use App\Http\Livewire\Auth\Passwords\Reset;
use App\Http\Livewire\Auth\Register;
use App\Http\Livewire\Auth\Verify;
use App\Http\Livewire\Frontend\About;
use App\Http\Livewire\Frontend\Charity as FrontendCharity;
use App\Http\Livewire\Frontend\CharityDetail;
use App\Http\Livewire\Frontend\Dev;
use App\Http\Livewire\Frontend\Home;
use App\Http\Livewire\Frontend\ProfileLink as FrontendProfileLink;
use App\Http\Livewire\Frontend\Translator as FrontendTranslator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix(LaravelLocalization::setLocale())
    ->middleware(['localeSessionRedirect', 'localizationRedirect', 'localeViewPath'])
    ->group(function () {

        Route::get('/', Home::class)
            ->name('home');

        Route::get('login', Login::class)
            ->name('login');

        Route::get('softwaredev', Dev::class)
            ->name('softwaredev');

        Route::get('translator', FrontendTranslator::class)
            ->name('translator');

        Route::get('charity', FrontendCharity::class)
            ->name('charity');

        Route::get('about', About::class)
            ->name('about');

        Route::get('charity/{slug}', ['as' => 'charity/{slug}', 'uses' => CharityDetail::class]);
        // Route::get('register', Register::class)
        //     ->name('register');
    });

Route::get('link/{slug}', [ProfileLinkController::class, 'index'])->name('link');

Route::middleware('auth')->group(function () {
    Route::get('email/verify/{id}/{hash}', EmailVerificationController::class)
        ->middleware('signed')
        ->name('verification.verify');

    Route::get('logout', LogoutController::class)
        ->name('logout');
});

Route::middleware('auth')->prefix('office')->name('admin.')->group(function () {
    Route::middleware('loggedin:superuser')->group(function () {

        Route::get('dashboard', Dashboard::class)->name('dashboard');
        Route::get('project', Project::class)->name('project');
        Route::get('user', User::class)->name('user');
    });

    Route::get('link', ProfileLink::class)->name('link');
    Route::get('client', Client::class)->name('client');
    Route::get('profile', Profile::class)->name('profile');
    Route::get('charity', Charity::class)->name('charity');
    Route::get('finance', Finance::class)->name('finance');
    Route::get('financecategory', FinanceCategory::class)->name('financecategory');
    Route::get('financeaccount', FinanceAccount::class)->name('financeaccount');
    Route::get('mail', Mail::class)->name('mail');
    Route::get('charityfinance', CharityFinance::class)->name('charityfinance');
    Route::get('translator', Translator::class)->name('translator');
});

Route::any('privacy-policy', function () {
    return view('privacy-policy');
});
Route::any('privacy-policy/msn', function () {
    return view('msn-privacy-policy');
});

Route::get('password/reset', Email::class)
    ->name('password.request');

Route::get('password/reset/{token}', Reset::class)
    ->name('password.reset');

Route::middleware('auth')->group(function () {
    Route::get('email/verify', Verify::class)
        ->middleware('throttle:6,1')
        ->name('verification.notice');

    Route::get('password/confirm', Confirm::class)
        ->name('password.confirm');
});
